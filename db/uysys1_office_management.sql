-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 08, 2019 at 04:13 AM
-- Server version: 5.6.40-84.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uysys1_office_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_actions`
--

CREATE TABLE `acl_actions` (
  `id` int(11) NOT NULL,
  `acl_controller_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `plugins` varchar(50) NOT NULL,
  `controllers` varchar(50) NOT NULL,
  `actions` varchar(50) NOT NULL,
  `params` varchar(64) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_actions`
--

INSERT INTO `acl_actions` (`id`, `acl_controller_id`, `title`, `plugins`, `controllers`, `actions`, `params`, `admin`, `status`) VALUES
(1, 1, 'clients', 'false', 'clients', 'index', '', 1, 'active'),
(2, 1, 'domains', 'false', 'domains', 'index', '', 1, 'active'),
(3, 1, 'projects', 'false', 'projects', 'index', '', 1, 'active'),
(4, 1, 'employees', 'false', 'employees', 'index', '', 1, 'active'),
(5, 1, 'task_managements', 'false', 'task_managements', 'index', '', 1, 'active'),
(6, 1, 'inventories', 'false', 'inventories', 'index', '', 1, 'active'),
(7, 1, 'team_managements', 'false', 'team_managements', 'index', '', 1, 'active'),
(8, 1, 'project_members', 'false', 'project_members', 'index', '', 1, 'active'),
(9, 1, 'office_certificates', 'false', 'office_certificates', 'index', '', 1, 'active'),
(10, 1, 'documents', 'false', 'documents', 'index', '', 1, 'active'),
(11, 1, 'products', 'false', 'products', 'index', '', 1, 'active'),
(12, 1, 'service_charges', 'false', 'service_charges', 'index', '', 1, 'active'),
(13, 1, 'our_website_references', 'false', 'our_website_references', 'index', '', 1, 'active'),
(14, 1, 'client_visits', 'false', 'client_visits', 'index', '', 1, 'active'),
(15, 1, 'meetings', 'false', 'meetings', 'index', '', 1, 'active'),
(16, 1, 'dashboards', 'false', 'dashboards', 'dashboard', '', 1, 'active'),
(17, 2, 'list_all_users', 'false', 'users', 'index', '', 1, 'active'),
(18, 2, 'new_user', 'false', 'users', 'add', '', 1, 'active'),
(19, 2, 'roles', 'false', 'roles', 'index', '', 1, 'active'),
(20, 2, 'generate_acl', 'false', 'roles', 'acl', '', 1, 'active'),
(21, 3, 'departments', 'false', 'departments', 'index', '', 1, 'active'),
(22, 3, 'hostings', 'false', 'hostings', 'index', '', 1, 'active'),
(23, 3, 'project_categories', 'false', 'project_categories', 'index', '', 1, 'active'),
(24, 3, 'document_types', 'false', 'document_types', 'index', '', 1, 'active'),
(25, 3, 'designations', 'false', 'designations', 'index', '', 1, 'active'),
(26, 3, 'team_types', 'false', 'team_types', 'index', '', 1, 'active'),
(27, 3, 'countries', 'false', 'countries', 'index', '', 1, 'active'),
(28, 3, 'platforms', 'false', 'platforms', 'index', '', 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `acl_controllers`
--

CREATE TABLE `acl_controllers` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `plugins` varchar(100) NOT NULL,
  `controllers` text NOT NULL,
  `actions` varchar(50) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_controllers`
--

INSERT INTO `acl_controllers` (`id`, `title`, `plugins`, `controllers`, `actions`, `admin`, `order`, `status`) VALUES
(1, 'main_menu', '[\"default\"]', '[\"dashboards\",\"clients\",\"domains\",\"projects\",\"employees\",\"task_managements\",\"inventories\",\"team_managements\",\"project_members\",\"documents\",\"office_certificates\",\"products\",\"service_charges\",\"our_website_references\",\"client_visits\",\"meetings\"]', '', 1, 0, 'active'),
(2, 'user_manager', '[\"default\"]', '[\"users\",\"roles\"]', '', 1, 0, 'active'),
(3, 'general_settings', '[\"default\"]', '[\"departments\",\"hostings\",\"project_categories\",\"document_types\",\"designations\",\"team_types\",\"countries\",\"platforms\"]', '', 1, 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `project_category_id` int(11) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `product_id` int(11) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `office_address` text,
  `description` text,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `client_type_id` int(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `project_category_id`, `company_name`, `product_id`, `contact_person`, `office_address`, `description`, `phone`, `email`, `website`, `client_type_id`, `country_id`, `status`, `created`, `modified`) VALUES
(1, 0, 'Bangladesh Paper Mills', 7, 'Mr. Shahin', '', 'dsfds', '01738924858', '', '', 0, 1, 1, '2017-09-11 08:32:18', '2017-09-16 08:23:32'),
(2, 0, 'City Group, Konapara (4)', 7, 'Mr. Nayan Shaha', '', '', '0171 8267211', '', '', 0, 1, 1, '2017-09-11 11:04:05', '2017-09-16 08:23:37'),
(3, 0, 'Essential Drugs , Dhaka', 7, 'Mr. Ashfaquer Rahman', '', '', '0119 0343434', '', '', 0, 1, 0, '2017-09-16 07:03:10', '2017-09-16 08:23:43'),
(4, 0, 'Essential Drugs , Bogra', 7, 'Mr. Badol', '', '', '0191 2468187', '', '', 0, 1, 0, '2017-09-16 07:04:37', '2017-09-16 08:23:52'),
(5, 0, 'Galco Steel Mill', 7, 'Md Nasir', '', '', '0191 8390441', '', '', 0, 1, 0, '2017-09-16 07:05:54', '2017-09-16 08:24:03'),
(6, 0, 'Godrej  Warehouse', 7, 'Mr Proloy', '', '', '0195 5503560', '', '', 0, 1, 0, '2017-09-16 07:07:16', '2017-09-16 08:24:12'),
(7, 0, 'Godrej Saver', 7, 'Mr Proloy', '', '', '0195 5503560', '', '', 0, 1, 0, '2017-09-16 07:08:30', '2017-09-16 08:24:23'),
(8, 0, 'Hyundai Motors', 7, 'Md Muradul Islam', '', '', '0119 0420851', '', '', 0, 1, 0, '2017-09-16 07:09:18', '2017-09-16 08:24:32'),
(9, 0, 'Hotel Lake Castel', 7, 'Md Salauddin', '', '', '0184 5261883', '', '', 0, 1, 0, '2017-09-16 07:09:58', '2017-09-16 08:24:40'),
(10, 0, 'JMS Glass', 7, 'Md Awal', '', '', '0171 5854480', '', '', 0, 1, 0, '2017-09-16 07:10:33', '2017-09-16 08:24:47'),
(11, 0, 'Lalbag Chemical ', 7, 'Abdul Aziz', '', '', '0173 5741733', '', '', 0, 1, 0, '2017-09-16 07:11:30', '2017-09-16 08:24:55'),
(12, 0, 'Radio Today', 7, 'MR Jamil', '', '', '01711 366772', '', '', 0, 1, 0, '2017-09-16 07:12:09', '2017-09-16 08:25:02'),
(13, 0, 'SS Steel', 7, 'Mr Jahid', '', '', '0173 0341331', '', '', 0, 1, 0, '2017-09-16 07:12:44', '2017-09-16 08:25:11'),
(14, 0, 'Zulegh  Pharmaciticals', 7, 'Mr. Raton', '', '', '0173 0303188', '', '', 0, 1, 0, '2017-09-16 07:14:05', '2017-09-16 08:25:38'),
(15, 0, 'LG Butterfly', 7, 'Md Jahid', '', '', '0167 8100212', '', '', 0, 1, 0, '2017-09-16 07:15:08', '2017-09-16 08:25:47'),
(16, 0, 'Super Star Group', 7, 'Md Mosle Uddin Firoz', '', '', '0171 3195838', '', '', 0, 1, 0, '2017-09-16 07:16:12', '2017-09-16 08:25:56'),
(17, 0, 'IDC, Dhaka', 8, 'Mr Hebjur Rahman', '', '', '0198 7000761', '', '', 0, 1, 0, '2017-09-16 07:17:02', '2017-09-16 08:26:38'),
(18, 0, 'B & B Food, CTG', 8, 'Mr Suvas', '', '', '0181 9941508', '', '', 0, 1, 0, '2017-09-16 07:17:59', '2017-09-16 08:21:14'),
(19, 4, 'Airmat Goodi Fan CTG', 8, 'Md Jahir', '', '', '0181 8514495', '', '', 1, 1, 0, '2017-09-16 07:19:31', '2017-09-17 05:51:33'),
(20, 0, 'New Generation Treading , CTG', 8, 'Hosneara Alom', '', '', '0167 7676671', '', '', 0, 1, 0, '2017-09-16 07:20:53', '2017-09-16 08:21:30'),
(21, 0, 'Cylon Biskut', 8, 'Md Shohan', '', '', '0199 3336633', '', '', 0, 1, 0, '2017-09-16 07:22:14', '2017-09-16 08:21:42'),
(22, 0, 'Ujala Paints', 8, 'Md Shohidul Islam', '', '', '01712947775', '', '', 0, 1, 0, '2017-09-16 07:22:57', '2017-09-16 08:21:55'),
(23, 0, 'Square Pharmaciticals', 8, 'Md Zillur Rahman', '', '', '01713067370', '', '', 0, 1, 0, '2017-09-16 07:25:07', '2017-09-16 08:22:11'),
(24, 0, 'Index Group ', 6, 'Nur-a Alam', '', '', '01844004242', 'bijoytech.nur@gmail.com', '', 0, 1, 0, '2017-09-16 08:53:23', '2017-09-16 08:53:23'),
(25, 0, 'Khulna Power Company Ltd. (KPCL)', 6, 'Moshiur Rahman', '', '', '01714046987 ', 'moshiur@khulnapower.com', '', 0, 1, 0, '2017-09-16 08:55:10', '2017-09-16 08:55:10'),
(26, 4, 'Avery Dennison ', 5, 'Shyamal Kumar Biswas', '', '', '01713142577', 'shyamal.kumar@ap.averydennison.com', '', 1, 1, 0, '2017-09-16 08:56:08', '2017-09-17 05:51:47'),
(27, 0, 'Home Ideas', 1, 'Nazmul Islam', '', '', '01844004176', 'nazmul07@index-companies.com', '', 0, 1, 0, '2017-09-16 08:56:48', '2017-09-16 08:56:48'),
(28, 0, 'Gonga Gang', 1, 'S M Shahjada', '', '', '01759000000', 'smshahjada@gmail.com', '', 0, 1, 0, '2017-09-16 08:57:33', '2017-09-16 08:57:33'),
(29, 0, 'Trust Group', 2, 'Ayesha Akter', '', '', '01844146223', '', '', 0, 1, 0, '2017-09-16 08:58:26', '2017-09-16 08:58:26'),
(30, 4, 'AFC Pharmaceuticals ', 2, 'Mr. Jahir', '', '', '01971800309', '', '', 1, 1, 0, '2017-09-16 08:59:54', '2017-09-17 05:51:26'),
(31, 0, 'Preetom Burgers', 3, 'Mr. Nihar', '', '', '01710506975', 'nihar@officextracts.com', '', 0, 1, 0, '2017-09-16 09:00:56', '2017-09-16 09:00:56'),
(32, 0, 'Paper 21 Ltd. ', 3, 'Mehedi hasan', '', '', '01763032957', 'mehedi_tkg@yahoo.com', '', 0, 1, 0, '2017-09-16 09:01:46', '2017-09-16 09:01:46'),
(33, 1, 'Viyellatex', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:09:16', '2017-09-17 08:09:16'),
(34, 1, 'Microfiber', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:09:36', '2017-09-17 08:09:36'),
(35, 1, 'JS Treasure', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:09:49', '2017-09-17 08:09:49'),
(36, 1, 'Rubize', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:09:58', '2017-09-17 08:09:58'),
(37, 1, 'Pretty Group', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:10:06', '2017-09-17 08:10:06'),
(38, 1, 'TNZ', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:10:15', '2017-09-17 08:10:15'),
(39, 1, 'NRB Buy Sell', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:10:24', '2017-09-17 08:10:24'),
(40, 1, 'Green grocer', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:10:40', '2017-09-17 08:10:40'),
(41, 1, 'Bengal Creations', 0, '', '', '', '', '', '', 1, 1, 0, '2017-09-17 08:10:47', '2017-09-17 08:10:47'),
(42, 1, 'isight (Malaysia)', 0, '', '', '', '', '', '', 1, 4, 0, '2017-09-17 08:11:34', '2017-09-17 08:11:34'),
(43, 1, 'Gture. Hurigruten', 1, '', '', '', '', '', '', 2, 1, 0, '2017-09-17 08:11:56', '2017-09-17 08:12:41'),
(44, 1, 'Preet', 0, '', '', '', '', '', '', 2, 1, 0, '2017-09-17 08:12:10', '2017-09-17 08:12:10'),
(45, 1, 'Danaweb', 0, '', '', '', '', '', '', 2, 1, 0, '2017-09-17 08:13:19', '2017-09-17 08:13:19'),
(46, 1, 'HF Assets', 0, '', '', '', '', '', '', 1, 1, 1, '2017-10-16 12:14:57', '2017-10-16 12:14:57'),
(47, 4, 'Summit Group', 0, '', '', '', '', '', '', 1, 1, 0, '2017-10-17 09:03:33', '2017-10-17 09:03:33'),
(48, 5, 'RPGCL', 0, '', '', '', '', '', '', 1, 1, 0, '2017-10-17 09:05:34', '2017-10-17 09:05:34'),
(49, 3, 'Tripzip', 0, '', '', '', '', '', '', 1, 1, 0, '2017-10-17 09:10:39', '2017-10-17 09:10:39'),
(50, 1, 'Approvide', 0, '', '', '', '', '', '', 1, 1, 0, '2017-10-17 11:44:01', '2017-10-17 11:44:01'),
(51, 1, 'Bagh Project', 0, '', '', '', '', '', '', 1, 1, 0, '2017-10-18 12:28:42', '2017-10-18 12:28:42'),
(52, 1, 'Youthbd.com', 0, '', '', '', '', '', '', 1, 1, 1, '2017-11-26 09:43:53', '2017-11-26 09:43:53');

-- --------------------------------------------------------

--
-- Table structure for table `client_types`
--

CREATE TABLE `client_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_types`
--

INSERT INTO `client_types` (`id`, `name`) VALUES
(1, 'Local'),
(2, 'International');

-- --------------------------------------------------------

--
-- Table structure for table `client_visits`
--

CREATE TABLE `client_visits` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `meeting_summary` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_visits`
--

INSERT INTO `client_visits` (`id`, `employee_id`, `client_name`, `address`, `contact`, `meeting_summary`, `status`, `date`, `created`, `modified`) VALUES
(1, 2, 'HF Assets Management', 'Tejgon', '', 'ok', 'Positive', '2017-10-08 00:00:00', '2017-10-07 05:46:38', '2017-10-09 06:21:48'),
(2, 24, 'C\'Minor Cafe ', '55 Satmasjid Road, Dhaka, Bangladesh', 'Md. Babul Hossain, +8801680551846', '', 'Semi Potential ', '2017-10-19 00:00:00', '2017-10-22 09:18:18', '2017-10-22 09:18:18'),
(3, 24, 'Square Pharmaceuticals Ltd. ', 'Square Centre 48, Mohakhali C/A, Bir Uttam AK Khandakar Rd, Dhaka 1212, Bangladesh', 'Rahinur Rahman, Database Administrator. +8801755534755', '', 'Potential', '2017-10-19 00:00:00', '2017-10-22 09:19:14', '2017-10-22 09:19:14');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `code`) VALUES
(1, 'Bangladesh', 'bd'),
(2, 'India', 'in'),
(3, 'Pakistan', 'pk'),
(4, 'Malaysia', 'my');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'Software'),
(2, 'Marketing'),
(3, 'Support'),
(4, 'Content Writer'),
(5, 'Accounts'),
(6, 'Management'),
(7, 'Intern Software'),
(8, 'Intern Marketing'),
(9, 'Networking'),
(10, 'Design'),
(11, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`) VALUES
(1, 'Managing Director'),
(2, 'Chief Technology Officer (CTO) '),
(3, 'Project Manager'),
(4, 'Team Leader'),
(5, 'Sr. Software Engineer'),
(6, 'Software Engineer'),
(7, 'Junior Software Engineer'),
(8, 'Assistant Technical Manager.'),
(9, 'Marketing Manager'),
(10, 'Accounts Manager'),
(11, 'Sales Representative'),
(12, 'Content Writer'),
(13, 'Network Engineer'),
(14, 'Software Quality Assurance'),
(15, 'Database Administrator'),
(16, 'Marketing Executive'),
(17, 'Office Assistance'),
(18, 'Sr Graphics Designer'),
(19, 'Junior Graphics Designer'),
(20, 'Intern'),
(21, 'Jr. Web Developer');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `document_name` varchar(100) DEFAULT NULL,
  `attachments` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `document_type_id` int(11) NOT NULL,
  `document_category_id` int(11) NOT NULL,
  `submission_date` date NOT NULL,
  `submit_by` varchar(255) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `client_name`, `document_name`, `attachments`, `description`, `document_type_id`, `document_category_id`, `submission_date`, `submit_by`, `status`, `created`, `modified`) VALUES
(1, 'TutorsInc! - Syed Noor Alam ', 'TutorsInc! Technical and Financial Proposal', '', 'Brand Outline\r\nThe following document will give the reader an idea about our initiative and paint a picture of the overall outline of the website we dream to build together.\r\n\r\nBrief Idea\r\nWe see ourselves as facilitators; a group of individuals who found one another on the road to connecting the dots between sincere students and dedicated teachers. We plan on doing so through a state-of-the-art online platform that allows students to avail numerous services from quality teachers around the country and someday, around the globe. We are TutorsInc.\r\n\r\nOur Services\r\nThe basic idea of this platform is to provide students a one stop solution, one curriculum at a time. Our services vary from subscription based content to free for all content. \r\nFollowing are basic descriptions about each segment of our service.\r\n\r\nClassrooms\r\nAs we mentioned, we will be starting off curriculum wise. Each curriculum will have a number of relevant subjects which will contain classrooms of specialized teachers. Before we go ahead, weâ€™d like to define what we mean by classrooms. It is a virtual space that is given to one particular teacher, where he or she will connect to students upon subscriptions. \r\nThis will open up the students to the following services. (Please note none of the services will be downloadable.)\r\n\r\nVideos: Pre-recorded videos that cover the entire syllabus of that particular teacher will be available in this section.\r\nNotes: Pre-uploaded notes of that particular teacher will be available in this section.\r\nQuizzes: Pre-uploaded quizzes that cover each chapter of the syllabus will be available in this section.\r\nDiscussion Forum: A Facebook group-like forum, where students can reach the teacher of the particular classroom at real time. The forum will be moderated by that particular teacher.\r\nLive:Â¬Using this portal, teacher of the given classroom can come live (like Facebook Live) and connect to students to discuss their problems and take assessments.\r\n\r\nOriginals\r\nAnother major part of our services lie in TutorsInc. Originals. This section contains the content created by us, given to students for free. The details of the elements within it are given below.\r\n\r\nQuestion Paper & Mark Schemes: This section contains a compilation of Question Papers and Mark Schemes, categorized yearly and chapter wise. The documents will be in pdf form, open for users to download.\r\nMock Papers: This section will have another compilation of pdfs, with mock papers. Moreover along with these, we will also provide these answers through videos, which will be downloadable.\r\n\r\nTutorsFeed\r\nThis part of the website is a simple feed like any other feed on social media. (For instance, Facebookâ€™s newsfeed) Here teachers will be able to upload statuses regarding their own classrooms, which will be visible to students in a form of feed.\r\n\r\nTutorsBlog\r\nThe last significant service of the website is a blog page, where institutes around the world can share their research and development work. We already have a few institutes from America, who are interested in doing so. The most notable being, Duke University from North Carolina, America.\r\n\r\nEnding Remarks\r\nThis was just a brief description of the website. We believe no stone should be left unturned when it comes to education and that is what we aspire to do. \r\nWe look forward to hearing from you and hopefully fix a time so that we can sit and further discuss our dream of revolutionizing the education industry, first within our country and eventually across the world.\r\n\r\n', 3, 0, '2017-09-24', 'Arif Hossen', 'active', '2017-09-25 06:55:40', '2017-10-22 08:32:43'),
(2, 'Netherlands Fit2Swarm', 'Technical and Financial Proposal', '', 'Final Tender submission date : 2-1-2018\r\n\r\nPlease see in zip file. ', 2, 0, '2017-09-23', 'Farhana A Rahman', 'active', '2017-09-25 07:02:09', '2018-01-07 05:48:30'),
(3, 'Chittagong Custom House', 'Ctg Custom House Web Application Development Requirements', '', 'Amount Demand : 485000 tk, without tender\r\nif tender then amount : 985000 tk \r\nMaintenance Charge  per month : 30000 tk', 2, 0, '2017-08-09', 'Arif Hossen', 'active', '2017-09-25 09:08:33', '2017-10-22 08:46:49'),
(4, 'Chittagong Custom House', 'Maintenance and Supports Agreement for Ctg Custom house website', '', '', 2, 0, '2017-08-10', 'Arif Hossen', 'active', '2017-09-25 09:09:14', '2017-10-22 08:46:59'),
(5, 'TutorsInc! - Syed Noor Alam  ', 'Technical and Financial Proposal Client Feedback', '', '', 3, 0, '2017-10-04', 'Arif Hossen', 'active', '2017-10-05 05:28:24', '2017-10-22 08:47:09'),
(6, 'HF Asset Management Limited ', 'Technical and Financial Proposal', '', 'HF Asset Management Limited Corporate Office: 138/1 (5th Floor) Tejgaon I/A, Dhaka-1208, Bangladesh.  Hotline:  +88 09611222000, 16273 Tel: +88 028870170 Email: nazmul734@gmail.com Web: www.hfassetmanagement.com \r\n \r\n ', 3, 0, '2017-10-02', 'Bristy', 'active', '2017-10-07 06:03:18', '2017-10-22 08:48:10'),
(7, 'HF Asset Management Limited  ', 'Technical and Financial Proposal with Agreement Letter', '', '', 4, 0, '2017-10-09', 'Arif Hossen', 'active', '2017-10-08 10:06:40', '2017-10-22 08:48:21'),
(8, 'TripZip Tour', '1st Phase Financial Proposal for Website Development for TripZip revised 19.07.2017', '', '', 6, 0, '2017-07-19', 'Sahadat Hossain', 'active', '2017-10-09 06:35:20', '2017-10-22 08:48:36'),
(9, 'TripZip Tour', '2nd Phase Financial Proposal for Website Development for TripZip revised 19.07.2017', '', '', 6, 0, '2017-07-19', 'Sahadat Hossain', 'active', '2017-10-09 06:36:09', '2017-10-22 08:48:42'),
(10, 'Sure Cash', 'Technical & Financial Proposal for Website Development for SureCash', '', '', 3, 0, '2017-08-08', 'Bristy', 'active', '2017-10-09 06:38:07', '2017-10-22 08:48:50'),
(11, 'Square Fashion', 'Technical and Financial Proposal for Office Automation System', '', '', 3, 0, '2017-05-12', 'Bristy', 'active', '2017-10-09 06:39:31', '2017-10-22 08:49:05'),
(12, 'Square Apparels', 'square-apparels Template', '', '', 5, 0, '2017-08-10', 'Bristy', 'active', '2017-10-09 06:43:19', '2017-10-22 08:49:14'),
(14, 'Sky Logistics Limited', 'WordPress Financial Proposal for SKY Logistics LTD. for Website Development 2017.06.12(1)', '', '', 3, 0, '2017-06-30', 'Bristy', 'active', '2017-10-09 06:45:36', '2017-10-22 08:49:22'),
(16, 'Teletalk Bangladesh Ltd', 'Technical And Financial Proposal of UYVMS for Teletalk Bangladesh Ltd.', '', '', 3, 0, '2017-06-13', 'Bristy', 'active', '2017-10-09 06:59:24', '2017-10-22 08:49:30'),
(17, 'Suguna Food', 'Technical And Financial Proposal of VMS for Suguna Food And Feeds', '', '', 3, 0, '2017-08-17', 'Bristy', 'active', '2017-10-09 07:44:14', '2017-10-22 08:49:37'),
(18, 'Karnafuli Project', 'Technical And Financial Proposal For Karnaphuli Gas ( KGDCL)', '', '', 3, 0, '2017-09-24', 'Bristy', 'active', '2017-10-09 07:46:25', '2017-10-22 08:49:46'),
(19, 'Eco_Tech_Project_UY-HRMS', 'Technical_&_Financial_Proposal_Leave& Attendance', '', '', 3, 0, '2017-06-10', 'Bristy', 'active', '2017-10-09 07:47:28', '2017-10-22 08:47:57'),
(23, 'Padma Oil Tender', 'Final Padma Oil RFP', NULL, '', 1, 0, '2017-08-28', 'Rahat Azim', 'active', '2017-10-22 08:43:16', '2017-10-22 08:51:24'),
(24, 'Chittagong Custom House', 'Three(3) Company Proposal Sent to custom house', NULL, '', 3, 0, '2018-01-01', 'Arif Hossen', 'active', '2017-10-24 09:00:52', '2018-01-07 09:15:12'),
(25, 'Avery Dennison Bangladesh ', 'Service Agreement of ', NULL, '', 4, 0, '2017-10-25', '', 'active', '2017-10-25 05:48:11', '2017-10-25 06:59:16'),
(26, 'ICT Innovation Govt', 'Project Proposal Accessible Notice Board For Disabilities Student', NULL, '', 3, 0, '2017-11-29', 'Farhana A Rahman', 'active', '2017-11-29 12:24:18', '2017-11-29 12:24:18'),
(27, 'ICT Innovation Govt', 'Project Proposal Digital Security System', NULL, '', 3, 0, '2017-11-29', 'Farhana A Rahman', 'active', '2017-11-29 12:24:42', '2017-11-29 12:24:42'),
(28, 'ICT Innovation Govt', 'Health Prior Proposal on Autism Welfare', NULL, '', 3, 0, '2017-11-29', 'Farhana A Rahman', 'active', '2017-11-29 12:25:24', '2017-11-29 12:25:24'),
(29, 'LICT', 'IOT Project Proposal', NULL, 'Submit to LICT ICT Tower', 3, 0, '2017-12-21', 'Arif Hossen', 'active', '2018-01-07 05:18:04', '2018-01-07 05:19:07'),
(30, 'ICT Division ', 'Innovation Work Proposed for Health Prior21 Ltd', NULL, 'Submit Project to https://www.ictd.gov.bd/\r\n\r\nInnovation Work ', 3, 0, '2018-01-04', 'Hasnat', 'active', '2018-01-07 05:26:41', '2018-01-07 05:27:34'),
(31, 'ICT Division ', 'Innovation Fund : Accessible Notice Board for Disabled Student. ', NULL, 'Innovation Fund A2i Service Fund : Ideabank : \r\n\r\nSubmit To ICT division ', 3, 0, '2018-01-04', 'Arif Hossen', 'active', '2018-01-07 05:29:11', '2018-01-07 05:29:30'),
(32, 'ICT Division ', 'Innovation Fund : Digital Security System', NULL, '', 3, 0, '2018-01-04', 'Arif Hossen', 'active', '2018-01-07 05:30:20', '2018-01-07 05:31:13'),
(33, 'Square Apperals Ltd', 'Technical & Financial Proposal for Website Development for Square Apparels Ltd', NULL, 'hanks for your reply. We are submitting the \"Technical & Financial Proposal for Website Development for Square Apparels Ltd.\" with different development technologies. Please receive the attached proposals as\r\n\r\n1. Using WordPress at: (Wordpress) Financial Proposal for Website Development for Square Apparels.pdf\r\n\r\n2. Using Laravel & AngularJS at: (Laravel) Financial Proposal for Website Development for Square Apparels.pdf\r\n\r\nIf you find any modules we missing in the feature list then reply us with your additional features or if you don\'t need any of the feature from the list then also inform us we will make the correction as per your need.\r\n', 3, 0, '2018-01-04', 'Sahadat', 'active', '2018-01-07 05:32:25', '2018-01-07 05:32:25'),
(34, 'University Teacher', 'Content Of Theme Development Project To Enable The Optimistic Intellect In IT Sector', NULL, 'Content Of Theme Development Project To Enable The Optimistic Intellect In IT Sector', 3, 0, '2017-12-28', 'Farhana A.Rahman', 'active', '2018-01-07 05:34:56', '2018-01-07 05:34:56'),
(35, 'Matador Group', 'Proposal On UY-VMS For Matador Group', NULL, 'Proposal Submit To Matador Group. ', 3, 0, '2017-12-28', 'Nourin Bristy', 'active', '2018-01-07 06:03:10', '2018-01-07 06:03:10'),
(36, 'Potonjoli', 'Financial Proposal of UYVMS for Potonjoli', NULL, '', 3, 0, '2017-12-20', 'Rahat Azim', 'active', '2018-01-07 06:05:26', '2018-01-07 06:05:41'),
(37, 'BIPPA', 'Technical and Financial Proposal for BIPPA Dynamic Website.', NULL, 'Website Development Price Quotation : Send this documents to sahil snipro...\r\n\r\n\r\nDear Mr. Sahil Omar Khan,\r\n\r\nAs per our meeting and discussion , We have made  Technical and Financial Proposal for BIPPA Dynamic Website. \r\n\r\nPlease overview the proposal . Let us your feedback if anythings needed. ', 3, 0, '2017-12-18', 'Arif Hossen', 'active', '2018-01-07 08:06:45', '2018-01-07 08:07:37'),
(38, 'Aptech Designs Limited', 'Technical & Financial Proposal for UY-HRM(R) (Human Resource Management)', NULL, 'Aptech Designs Limited  : HRM Software', 3, 0, '2017-12-12', 'Tuhin', 'active', '2018-01-07 08:13:00', '2018-01-07 08:13:17'),
(39, 'TNZ', 'Technical & financial proposal for UY-HRM(R) software development', NULL, '', 3, 0, '2017-12-03', 'Tuhin', 'active', '2018-01-07 08:14:48', '2018-01-07 08:14:48'),
(40, 'Advanced ERP (BD) Ltd.', 'Mobile app for ERP', NULL, 'Mostafizur Rahaman Sohel Bhai ERP Mobile Apps.', 3, 0, '2017-10-15', 'Arif Hossen', 'active', '2018-01-07 08:19:32', '2018-01-07 08:20:02'),
(41, 'Bravo Tours for mattress', 'Bravo Tours for mattress - Technical & Financial Proposal for E-Commerce Website Development', NULL, 'Bravo Tours for mattress - Technical & Financial Proposal for E-Commerce Website Development', 3, 0, '2017-10-07', 'Sahadat Hossain', 'active', '2018-01-07 08:22:20', '2018-01-07 08:22:20'),
(42, 'Riad Trading (Abdurrob Bhai)', 'Financial Proposal of Website and POS Factory Software', NULL, 'Financial Proposal of Website and POS Factory Software and Website\r\nRiad Trading...', 3, 0, '2017-10-04', 'Arif Hossen', 'active', '2018-01-07 08:26:50', '2018-01-07 08:26:50'),
(43, 'DIGICON Limited', 'Technical And Financial Proposal Of UYVMS for DIGICON', NULL, 'Technical And Financial Proposal Of UYVMS for DIGICON', 3, 0, '2017-07-27', 'Arif Hossen', 'active', '2018-01-07 08:31:00', '2018-01-07 08:31:29'),
(44, 'Abu Jabed ', 'Coverage Measurement Tool  Grameen Phone (GP-03797)', NULL, 'GP Project : 4.29.2018 to 8,October,2017 \r\nOutCome : 0 ', 1, 0, '2017-10-10', 'Arif Hossen', 'active', '2018-01-07 08:35:58', '2018-01-07 08:38:16'),
(45, 'Himalaya Preet Group', 'Technical & Financial Proposal For eCommerce Product Display Website Development For Himalaya', NULL, 'Technical & Financial Proposal For eCommerce Product Display Website Development For Himalaya', 1, 0, '2017-11-13', 'Sahadat', 'active', '2018-01-07 08:52:35', '2018-01-07 08:52:35'),
(46, 'LICT', 'RFP Of She Power Project . She Power: Sustainable Development for Women through ICT Project', NULL, '', 1, 0, '2017-12-26', 'Nourin Bristy', 'active', '2018-01-07 09:01:25', '2018-01-07 09:04:35'),
(47, 'Avery Dinnision ', 'Agreement Papers (Service Charge)', NULL, 'Agreement paper of Parax Avery Dinison Bangladesh with UY SYS', 4, 0, '2018-01-28', 'Get From Avery Dinission', 'active', '2018-01-28 11:08:00', '2018-01-28 11:08:00'),
(48, 'Fakir Fashion', 'Financial & Technical Proposal for Dynamic Website Development for \"Fakir Fashion\"', NULL, 'Fakir Fashion Reference by Square Fashion. ', 3, 0, '2018-01-28', 'Sahadat', 'active', '2018-01-28 11:09:13', '2018-01-28 11:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `document_categories`
--

CREATE TABLE `document_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_categories`
--

INSERT INTO `document_categories` (`id`, `name`) VALUES
(1, 'Website'),
(2, 'E-commerce'),
(3, 'POS Retails'),
(4, 'POS Ceramic'),
(5, 'HRM '),
(6, 'VMS (Vat Management)'),
(7, 'Web Application'),
(8, 'Software'),
(9, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `document_files`
--

CREATE TABLE `document_files` (
  `id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document_files`
--

INSERT INTO `document_files` (`id`, `document_id`, `file`, `modified`, `created`) VALUES
(14, 21, '21.treated_effluent_analysis_report_2016_1.jpg', '2017-10-22 06:52:13', '2017-10-22 06:52:13'),
(15, 21, '21.treated_effluent_analysis_report_2016_2.jpg', '2017-10-22 06:52:14', '2017-10-22 06:52:14'),
(16, 21, '21.treated_effluent_analysis_report_2016_3.jpg', '2017-10-22 06:52:14', '2017-10-22 06:52:14'),
(19, 1, '1.Technical and Financial Proposal for Website Development for TutorsInc.Revised.docx', '2017-10-22 07:00:04', '2017-10-22 07:00:04'),
(20, 5, '5.Client_Feedback_Technical and Financial Proposal for Website Development for TutorsInc.Revised.docx', '2017-10-22 07:01:44', '2017-10-22 07:01:44'),
(21, 1, '1.Brand Outline.docx', '2017-10-22 07:02:22', '2017-10-22 07:02:22'),
(22, 2, '2.Technical and Financial Proposal for Fit2Swarm Tools.docx', '2017-10-22 07:02:57', '2017-10-22 07:02:57'),
(24, 4, '4.Maintenance and Supports Agreement for Ctg Custom house website.docx', '2017-10-22 07:04:30', '2017-10-22 07:04:30'),
(25, 3, '3.Ctg Custom House Web Application Development Requirements.docx', '2017-10-22 07:05:33', '2017-10-22 07:05:33'),
(26, 3, '3.Maintenance and Supports Agreement for Ctg Custom house website.docx', '2017-10-22 08:09:50', '2017-10-22 08:09:50'),
(28, 6, '6.Technical & Financial Proposal for Website Development.docx', '2017-10-22 08:11:45', '2017-10-22 08:11:45'),
(29, 7, '7.Financial Proposal for Website Development for HF Assets Management 2017.10.09.docx', '2017-10-22 08:11:55', '2017-10-22 08:11:55'),
(30, 8, '8.1st Phase Financial Proposal for Website Development for TripZip revised 19.07.2017.docx', '2017-10-22 08:12:56', '2017-10-22 08:12:56'),
(31, 9, '9.2nd Phase Financial Proposal for Website Development for TripZip revised 19.07.2017.docx', '2017-10-22 08:13:05', '2017-10-22 08:13:05'),
(32, 10, '10.Technical & Financial Proposal for Website Development for SureCash.docx', '2017-10-22 08:14:32', '2017-10-22 08:14:32'),
(33, 11, '11.Technical and Financial Proposal for Office Automation System.pdf', '2017-10-22 08:15:04', '2017-10-22 08:15:04'),
(34, 12, '12.Our Project Portfolio Links.docx', '2017-10-22 08:15:49', '2017-10-22 08:15:49'),
(35, 12, '12.Square Apparels Ltd..pptx', '2017-10-22 08:15:49', '2017-10-22 08:15:49'),
(36, 12, '12.square.jpg', '2017-10-22 08:15:49', '2017-10-22 08:15:49'),
(37, 12, '12.square-apparels.jpg', '2017-10-22 08:15:49', '2017-10-22 08:15:49'),
(38, 14, '14.AngularJS-Laravel Financial Proposal for SKY Logistics LTD. for Website Development 2017.06.12.docx', '2017-10-22 08:17:10', '2017-10-22 08:17:10'),
(39, 14, '14.WordPress Financial Proposal for SKY Logistics LTD. for Website Development 2017.06.12(1).docx', '2017-10-22 08:17:10', '2017-10-22 08:17:10'),
(40, 16, '16.Technical And Financial Proposal of UYVMS for Teletalk Bangladesh Ltd..docx', '2017-10-22 08:18:05', '2017-10-22 08:18:05'),
(41, 17, '17.Technical And Financial Proposal of VMS for Suguna Food And Feeds.docx', '2017-10-22 08:18:37', '2017-10-22 08:18:37'),
(42, 18, '18.Technical And Financial Proposal For Karnaphuli Gas ( KGDCL).docx', '2017-10-22 08:20:04', '2017-10-22 08:20:04'),
(43, 19, '19.Technical_&_Financial_Proposal_Leave& Attendance with Payroll.docx', '2017-10-22 08:20:30', '2017-10-22 08:20:30'),
(44, 19, '19.Technical_&_Financial_Proposal_Leave& Attendance.docx', '2017-10-22 08:20:30', '2017-10-22 08:20:30'),
(46, 22, '22.Brochure_UY_HRMS.pdf', '2017-10-22 08:25:29', '2017-10-22 08:25:29'),
(52, 6, '6.', '2017-10-22 08:28:06', '2017-10-22 08:28:06'),
(58, 23, '23.Final Padma Oil RFP.zip', '2017-10-22 08:43:16', '2017-10-22 08:43:16'),
(59, 24, '24.JARS Systems Ltd Technical & Financial Proposal for Website Design and Development.docx', '2017-10-24 09:00:52', '2017-10-24 09:00:52'),
(60, 24, '24.Proposal for Website Design and Development by Nogor Solutions Ltd.docx', '2017-10-24 09:00:52', '2017-10-24 09:00:52'),
(61, 24, '24.Technical and Financial Proposal for Website Design and Development by Sigma Systems Ltd.docx', '2017-10-24 09:00:52', '2017-10-24 09:00:52'),
(64, 25, '25.Service Agreement of UY Systems with Avery Denison.docx', '2017-10-25 06:59:17', '2017-10-25 06:59:17'),
(65, 26, '26.Project Proposal Accessible Notice Board For Disabilities Student.pdf', '2017-11-29 12:24:18', '2017-11-29 12:24:18'),
(66, 27, '27.Project Proposal Digital Security System.pdf', '2017-11-29 12:24:42', '2017-11-29 12:24:42'),
(67, 28, '28.Health Prior Proposal on Autism Welfare.pdf', '2017-11-29 12:25:24', '2017-11-29 12:25:24'),
(68, 29, '29.Professional Training Sponsorship Proposal - Revised.docx', '2018-01-07 05:18:55', '2018-01-07 05:18:55'),
(69, 29, '29.Financial proposal for IOT Training (1).xls', '2018-01-07 05:19:07', '2018-01-07 05:19:07'),
(70, 30, '30.Forwading_Health Prior.docx', '2018-01-07 05:26:41', '2018-01-07 05:26:41'),
(71, 30, '30.Ministry of Information and Communication Technology  print.pdf', '2018-01-07 05:26:57', '2018-01-07 05:26:57'),
(72, 30, '30.Health Prior Proposal 11.docx', '2018-01-07 05:27:13', '2018-01-07 05:27:13'),
(73, 31, '31.Forwading_AccessableNoticeBoard.docx', '2018-01-07 05:29:11', '2018-01-07 05:29:11'),
(74, 31, '31.Project Proposal Accessible Notice Board For Disabilities Student.doc', '2018-01-07 05:29:30', '2018-01-07 05:29:30'),
(75, 32, '32.Forwading_Digital Security.docx', '2018-01-07 05:30:20', '2018-01-07 05:30:20'),
(76, 32, '32.Project Proposal Digital Security.doc', '2018-01-07 05:31:13', '2018-01-07 05:31:13'),
(77, 33, '33.(Laravel) Financial Proposal for Website Development for Square Apparels.pdf', '2018-01-07 05:32:25', '2018-01-07 05:32:25'),
(78, 33, '33.(Wordpress) Financial Proposal for Website Development for Square Apparels.pdf', '2018-01-07 05:32:25', '2018-01-07 05:32:25'),
(79, 34, '34.Theme Development Project To Enable The Optimistic Intellect In IT Sector.pdf', '2018-01-07 05:34:56', '2018-01-07 05:34:56'),
(80, 34, '34.Theme Development Project To Enable The Optimistic Intellect In IT Sector.docx', '2018-01-07 05:34:56', '2018-01-07 05:34:56'),
(81, 2, '2.CHC Final Tender Submission.zip', '2018-01-07 05:48:30', '2018-01-07 05:48:30'),
(82, 35, '35.Matador Group.pdf', '2018-01-07 06:03:10', '2018-01-07 06:03:10'),
(83, 35, '35.NBR SR.jpg', '2018-01-07 06:03:10', '2018-01-07 06:03:10'),
(84, 36, '36.Financial Proposal of UYVMS for Potonjoli.docx', '2018-01-07 06:05:27', '2018-01-07 06:05:27'),
(85, 37, '37.Technical & Financial Proposal for BIPPA Dynamic Website.docx', '2018-01-07 08:07:37', '2018-01-07 08:07:37'),
(86, 38, '38.Technical & Financial Proposal Aptech Designs Limited for UY-HRMS Software Development.pdf', '2018-01-07 08:13:00', '2018-01-07 08:13:00'),
(87, 39, '39.Technical & Financial Proposal TNZ for UY-HRMS Software Development.pdf', '2018-01-07 08:14:48', '2018-01-07 08:14:48'),
(88, 40, '40.Technical and Financial Proposal for ERP Android Mobile Apps.docx', '2018-01-07 08:20:02', '2018-01-07 08:20:02'),
(89, 41, '41.Technical & Financial Proposal for E-Commerce Website Development  05.10.2017.pdf', '2018-01-07 08:22:20', '2018-01-07 08:22:20'),
(90, 42, '42.Technical Proposal for Website Development for Riad Trading.docx', '2018-01-07 08:26:50', '2018-01-07 08:26:50'),
(91, 42, '42.Financial Proposal for Raid Trading for UY POS DEVELOPMENT 20171003.docx', '2018-01-07 08:26:50', '2018-01-07 08:26:50'),
(92, 43, '43.Technical And Financial Proposal Of VMS for Digicon Technology Ltd..pdf', '2018-01-07 08:31:29', '2018-01-07 08:31:29'),
(93, 44, '44.ABC & NDA.ZIP', '2018-01-07 08:35:58', '2018-01-07 08:35:58'),
(94, 44, '44.Relevant Doc.zip', '2018-01-07 08:35:58', '2018-01-07 08:35:58'),
(95, 44, '44.RFQ DOC_F.zip', '2018-01-07 08:35:58', '2018-01-07 08:35:58'),
(96, 44, '44.RFI_General & Commercial Information.xlsm', '2018-01-07 08:38:16', '2018-01-07 08:38:16'),
(97, 44, '44.image001.png', '2018-01-07 08:38:16', '2018-01-07 08:38:16'),
(98, 44, '44.Scope_Coverage Measurement Application.pdf', '2018-01-07 08:38:16', '2018-01-07 08:38:16'),
(99, 44, '44.Invitation Letter and Instructions for RFI_Coverage Measurement Application.pdf', '2018-01-07 08:38:16', '2018-01-07 08:38:16'),
(100, 45, '45.(revised excluded customer purchase) Financial Proposal for eCommerce Development for himalaya 2017.11.13.pdf', '2018-01-07 08:52:35', '2018-01-07 08:52:35'),
(101, 46, '46.RFP P1L2.docx', '2018-01-07 09:01:25', '2018-01-07 09:01:25'),
(102, 46, '46.Staffing Schedule.xls', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(103, 46, '46.Work Schedule.xlsx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(104, 46, '46.Comments or Suggestions on the Terms of Reference and on Counterpart Staff and Facilities.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(105, 46, '46.Draft_Power of Attorney.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(106, 46, '46.INDEX_Fin.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(107, 46, '46.Form 5B1.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(108, 46, '46.Draft_Eligibility Declaration Cert.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(109, 46, '46.INDEX_Tech.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(110, 46, '46.Form 5A1.docx', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(111, 46, '46.Relevant Doc.zip', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(112, 46, '46.RFQ DOC_F.zip', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(113, 46, '46.4. Form 5A 4.zip', '2018-01-07 09:03:42', '2018-01-07 09:03:42'),
(114, 24, '24.JARS_Final_Submission.zip', '2018-01-07 09:10:27', '2018-01-07 09:10:27'),
(115, 24, '24.UY Systems Ltd. CHC Final Tender Submission.zip', '2018-01-07 09:12:37', '2018-01-07 09:12:37'),
(116, 24, '24.Sigma System_Final Submission_CHC.zip', '2018-01-07 09:15:12', '2018-01-07 09:15:12'),
(117, 47, '47.Agreement Paxar Vs UYSYS.pdf', '2018-01-28 11:08:00', '2018-01-28 11:08:00'),
(118, 48, '48.Offer-1 Financial & Technical Proposal for Website Development for Fakir Fashion.pdf', '2018-01-28 11:09:13', '2018-01-28 11:09:13'),
(119, 48, '48.Offer-2 Financial & Technical Proposal for Website Development for Fakir Fashion.pdf', '2018-01-28 11:09:13', '2018-01-28 11:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `name`) VALUES
(1, 'Tender'),
(2, 'Technical Proposal'),
(3, 'Technical and Financial Proposal'),
(4, 'Agreements'),
(5, 'Presentation Slide'),
(6, 'Financial Proposal');

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(100) NOT NULL,
  `hosting_id` int(11) NOT NULL,
  `provider` varchar(50) NOT NULL,
  `renew_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `client` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `join_date` date DEFAULT NULL,
  `profile_img` varchar(100) DEFAULT NULL,
  `resignation_date` date DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `cv_attachment` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `department_id`, `name`, `designation_id`, `join_date`, `profile_img`, `resignation_date`, `phone`, `email`, `skype`, `cv_attachment`, `status`, `created`, `modified`) VALUES
(1, 6, 'Farhana A Rahman', 1, '2003-07-15', '20245739_10159094722295083_1495493942632760334_n.jpg', NULL, '01713037403', 'farhana@uysys.com', 'farhana.rahman', NULL, 'active', '2017-09-24 06:49:56', '2017-09-24 06:53:05'),
(3, 1, 'Moniruzzaman(Zaman)', 2, '2011-04-05', '11059318_10153946377672052_8273327928895426119_n.jpg', NULL, '01923760310', 'sahadat@uysys.com', 'sahadat39', NULL, 'active', '2017-09-24 06:52:35', '2017-10-01 12:01:17'),
(4, 5, 'Md. Shafiullah', 9, '2014-09-01', 'Shopon Picture.jpg', NULL, '01720212106', 'accounts@uysys.com', 'md.shafiullah2', 'Shafiullah CV.doc', 'active', '2017-09-24 06:59:07', '2017-09-28 09:04:59'),
(10, 1, 'Md. Zahangir Alam', 5, '2014-06-01', 'jahangir.png', NULL, '01914635730', '', '', 'Resume of Md. Zahangir Alam.docx', 'active', '2017-09-24 07:48:37', '2017-09-24 11:37:33'),
(11, 1, 'Rafiqul Islam', 5, NULL, NULL, NULL, '', '', '', NULL, 'active', '2017-09-24 07:48:52', '2017-09-24 07:48:52'),
(12, 1, 'Jafar Ahmed', 5, '2016-01-02', 'jafor_passport_photo.png', NULL, '01728489434', 'jafarju@yahoo.com', 'jafarju', 'JafarAhmadCV2017.doc', 'active', '2017-09-24 07:49:37', '2017-10-01 11:47:53'),
(13, 1, 'Rasel Rari', 13, NULL, 'rasel_passport_photo.jpg', NULL, '', '', '', 'Rasel-CV-update.docx', 'active', '2017-09-24 07:50:05', '2017-10-05 09:38:19'),
(15, 2, 'Mukidur Rahman Tonoy', 11, '2018-07-15', '0F4A0543.jpg', NULL, '01724462258', 'tonoymukidur@gmail.com', 'tonoyrahman1122', NULL, 'active', '2017-09-24 07:51:18', '2019-01-30 06:13:50'),
(16, 1, 'Harun or Rashid', 7, NULL, 'Untitled.png', NULL, '', '', '', 'Resume Of Harun.pdf', 'active', '2017-09-24 07:51:29', '2017-09-25 06:07:55'),
(20, 1, 'Md.Wafi Islam Omi', 7, '2017-01-01', 'A1-4602.jpg', NULL, '01674313173', '', '', 'WafiIslamOmi.pdf', 'active', '2017-09-24 07:52:32', '2017-09-25 04:39:29'),
(26, 11, 'Sumi', 17, NULL, NULL, NULL, '', '', '', NULL, 'active', '2017-09-24 09:38:28', '2017-09-24 09:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `hostings`
--

CREATE TABLE `hostings` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hostings`
--

INSERT INTO `hostings` (`id`, `name`) VALUES
(1, 'Host Gator');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE `inventories` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `mother_board` varchar(50) DEFAULT NULL,
  `processor` varchar(255) NOT NULL,
  `ram` varchar(50) DEFAULT NULL,
  `hard_disk` varchar(100) DEFAULT NULL,
  `pc_number` varchar(50) DEFAULT NULL,
  `monitor_name` varchar(255) NOT NULL,
  `total_monitor` int(11) NOT NULL,
  `mouse` varchar(10) NOT NULL,
  `keyboard` varchar(10) NOT NULL,
  `type` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `company` varchar(100) NOT NULL,
  `date` datetime DEFAULT NULL,
  `location` text,
  `phone` varchar(20) NOT NULL,
  `key_person` varchar(50) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `meeting_summary` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `company`, `date`, `location`, `phone`, `key_person`, `type`, `meeting_summary`) VALUES
(1, 'HF Asset Management Limited ', '2017-10-08 11:00:00', 'Corporate Office: 138/1 (5th Floor) Tejgaon I/A, Dhaka-1208, Bangladesh.  Hotline:  +88 09611222000, 16273 Tel: +88 028870170 ', '01711596705', 'Fayekuzzam MD HF Asset Mgt Com.Ltd ', 'Website Development', ''),
(2, 'A2i Disability Challenge Fund', '2017-10-23 10:00:00', ' a2i Innovation Lab, National Museum of Science and Technology, Agargaon', '', '', 'verbally describe your solution ', ''),
(3, 'TutorsInc ', '2017-09-27 04:00:00', 'TutorsInc. Stride International School House# 34, Rd.26, Sector 7, Uttara ', '8801711532909', '', '', ''),
(4, 'Islam Aftab Kamrul & Co', '2018-01-08 00:00:00', 'Islam Aftab Kamrul & Co., Chartered Accountants\r\nBSCIC Electronics Complex (Level 5)\r\nPlot No. 1/1, Road-3, Avenue-4,\r\nSection-7, Mirpur, Dhaka-1216', '+8801914742185', ' Shadman B. Zahir', '', 'Speaker : Rahat Azim');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_feedback_files`
--

CREATE TABLE `meeting_feedback_files` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting_feedback_files`
--

INSERT INTO `meeting_feedback_files` (`id`, `meeting_id`, `file`, `filename`, `created`) VALUES
(1, 0, '.registration_form.png', 'registration_form', '2017-11-04'),
(2, 0, '.selection_process.png', 'selection_process', '2017-11-04'),
(3, 0, '.ict_product.png', 'ict_product', '2017-11-04'),
(4, 1, '1.E-Business.png', 'E-Business', '2017-11-04'),
(5, 1, '1.ict_international_market_focus.png', 'ict_international_market_focus', '2017-11-04'),
(6, 1, '1.ict_product.png', 'ict_product', '2017-11-04'),
(7, 1, '1.ICT_Startup.png', 'ICT_Startup', '2017-11-04'),
(8, 1, '1.pioneer.png', 'pioneer', '2017-11-04'),
(13, 7, '7.E-Business.png', 'E-Business', '2017-11-04'),
(14, 7, '7.ICT_Business_person.png', 'ICT_Business_person', '2017-11-04'),
(18, 7, '7.pioneer.png', 'pioneer', '2017-11-04'),
(19, 7, '7.registration_form.png', 'registration_form', '2017-11-04'),
(20, 7, '7.selection_process.png', 'selection_process', '2017-11-04'),
(21, 4, '4.meeting.png', 'meeting', '2018-01-07');

-- --------------------------------------------------------

--
-- Table structure for table `office_certificates`
--

CREATE TABLE `office_certificates` (
  `id` int(11) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `file_name` varchar(500) NOT NULL,
  `description` text,
  `status` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `office_certificates`
--

INSERT INTO `office_certificates` (`id`, `name`, `file_name`, `description`, `status`, `created`, `modified`) VALUES
(1, 'UY  Vat Certificate', '1.VAT certificate.pdf.pdf', 'UY Vat Certificate', NULL, '2017-09-27 06:05:30', '2017-09-27 06:05:30'),
(2, 'UY Tin Certificate', '2.TIN Certificate.pdf.pdf', 'UY Tin Certificate', NULL, '2017-09-27 06:06:24', '2017-09-27 06:06:24'),
(3, 'Uy System Presentation Slide', '3.UY Systems Ltd. Presentation.pptx', 'Uy System Presentation Slide for Local Marketing...', NULL, '2017-09-27 06:09:19', '2017-09-27 06:09:19'),
(4, 'Uy System Presentation offshore Marketing', '4.UY Systems Ltd. Offshore Market.ppt', 'Uy System Presentation offshore Marketing. International', NULL, '2017-09-27 06:10:23', '2017-09-27 06:10:23'),
(5, 'ISO 9001-2015', '5.ISO 9001-2015.jpg', '', NULL, '2017-10-01 10:22:49', '2017-10-01 10:22:49'),
(6, 'ISO 27001-2013', '6.ISO 27001-2013.jpg', '', NULL, '2017-10-01 10:23:01', '2017-10-01 10:23:01'),
(7, 'Tax Certificate', '7.TAX-Certificate-16-17.jpg', '', NULL, '2017-10-01 10:23:54', '2017-10-01 10:23:54'),
(8, 'Trade License Jessore.', '8.Trade License Jessore..pdf', '', NULL, '2017-10-01 10:25:13', '2017-10-01 10:25:13'),
(9, 'Trade License Old', '9.Trade License Old.pdf', '', NULL, '2017-10-01 10:25:23', '2017-10-01 10:25:23'),
(10, 'Trade Lisence_2016_2017_2018', '10.Trade Lisence_2016_2017_2018.pdf', '', NULL, '2017-10-01 10:25:32', '2017-10-01 10:25:33'),
(11, 'Trade-Lisence-Chittagong', '11.Trade-Lisence-Chittagong.jpg', '', NULL, '2017-10-01 10:25:43', '2017-10-01 10:25:44'),
(12, 'Company Letter Pad', '12.New Letter Pad.docx', '', NULL, '2017-10-01 10:26:37', '2017-10-01 10:26:38'),
(13, 'Meomorandum And Article of Association', '13.Memorandum And Articles of Association.pdf', '', NULL, '2017-10-01 10:27:17', '2017-10-01 10:27:17'),
(14, 'Logistics', '14.Logistics.pdf', '', NULL, '2017-10-01 10:27:36', '2017-10-01 10:27:36'),
(15, 'Incorporation Certificate', '15.Incorporation Certificate.pdf', '', NULL, '2017-10-01 10:27:51', '2017-10-01 10:27:51'),
(16, 'Basis Certificate_2016', '16.Basis Certificate_2016.pdf', '', NULL, '2017-10-01 10:28:32', '2017-10-01 10:28:32'),
(17, 'BASIS Certificate 2017', '17.BASIS Certificate 2017.pdf', '', NULL, '2017-10-01 10:28:51', '2017-10-01 10:28:51'),
(18, 'Bank Solvency Certificate_2016', '18.Bank Solvency Certificate_2016.pdf', '', NULL, '2017-10-01 10:29:16', '2017-10-01 10:29:16'),
(19, 'Bank Solvency Certificate_2017', '19.Bank Solvency Certificate_2017.pdf', '', NULL, '2017-10-01 10:29:24', '2017-10-01 10:29:30'),
(20, 'Bank Solvency_2017', '20.Bank Solvency_2017.jpg', '', NULL, '2017-10-01 10:29:40', '2017-10-01 10:29:40'),
(21, 'Audit report 2012', '21.Audit report 2012.pdf', '', NULL, '2017-10-01 10:31:18', '2017-10-01 10:31:18'),
(22, 'Audit Report 2013', '22.Audit Report 2013.pdf', '', NULL, '2017-10-01 10:31:27', '2017-10-01 10:31:27'),
(23, 'Auditors Report 2014 of UY Systems Ltd', '23.Auditors Report 2014 of UY Systems Ltd.pdf', '', NULL, '2017-10-01 10:31:37', '2017-10-01 10:31:37'),
(24, 'Auditors Report 2015 of UY Systems Ltd', '24.Auditors Report 2015 of UY Systems Ltd.pdf', '', NULL, '2017-10-01 10:31:48', '2017-10-01 10:31:48'),
(25, 'Auditors Report 2016', '25.Auditors Report 2016.pdf', '', NULL, '2017-10-01 10:31:57', '2017-10-01 10:31:57'),
(26, 'CMMI Level Manual (UY SYSTEMS Ltd)', '26.CMM Manual.pdf', 'CMMI Level Manual (UY SYSTEMS Ltd)', NULL, '2017-11-25 09:21:58', '2017-11-25 09:21:58'),
(27, 'Company Profile of UY Systems Ltd', '27.Company Profile of UY Systems Ltd.pdf', 'Company Profile of UY Systems Ltd', NULL, '2018-02-11 08:31:05', '2018-02-11 08:31:05'),
(28, 'CMMI L3 Certificate', '28.CMMI Level 3 certificate.pdf', 'CMMI L3 Certificate', NULL, '2018-02-11 08:51:25', '2018-02-11 08:51:25'),
(29, 'BMD Completion Certificate', '29.BMD Completion Certificate-.pdf', 'BMD Completion Certificate, Bangladesh Metrological Department Mobile Apps. ', NULL, '2018-02-11 09:42:05', '2018-02-11 09:42:05');

-- --------------------------------------------------------

--
-- Table structure for table `our_website_references`
--

CREATE TABLE `our_website_references` (
  `id` int(11) NOT NULL,
  `company` varchar(255) NOT NULL,
  `website_url` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `complete_date` date NOT NULL,
  `modified` date NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `our_website_references`
--

INSERT INTO `our_website_references` (`id`, `company`, `website_url`, `details`, `complete_date`, `modified`, `status`) VALUES
(1, 'Pledgeharbor School', 'http://pledgeharbor.org/', 'http://pledgeharbor.org/', '0000-00-00', '2017-10-18', 'Active'),
(2, 'Viyellatex', 'http://viyellatex.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(3, 'Hurtigruten', 'http://hurtigruten.cn/', 'http://hurtigruten.cn/', '0000-00-00', '2017-10-18', 'Active'),
(4, 'Route66', 'http://uysys.net/demo/route66usa/public/', '', '0000-00-00', '2017-10-18', 'Active'),
(5, 'Lolafitz', 'http://lolafitz.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(6, 'kadoeshi', 'http://uysys.net/demo/kadoeshi', '', '0000-00-00', '2017-10-18', 'Active'),
(7, 'crossfit5triple9', 'http://crossfit5triple9.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(8, 'tnzbd', 'http://tnzbd.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(9, 'nrbbazaar', 'https://www.nrbbazaar.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(10, 'squarefashions', 'http://www.squarefashions.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(11, 'prettygroupbd', 'http://prettygroupbd.com/', '', '0000-00-00', '2017-10-18', 'Active'),
(12, 'rajahspices', 'http://rajahspices.co.uk/', '', '0000-00-00', '2017-10-18', 'Active'),
(13, 'hereandnow365', 'http://hereandnow365.co.uk/', '', '0000-00-00', '2017-10-18', 'Active'),
(14, 'trs', 'http://trs.co.uk/', '', '0000-00-00', '2017-10-18', 'Active'),
(15, 'mfgbd', 'http://www.mfgbd.net/', '', '0000-00-00', '2017-10-18', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `pay_schedules`
--

CREATE TABLE `pay_schedules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pay_schedules`
--

INSERT INTO `pay_schedules` (`id`, `name`) VALUES
(1, 'Monthly'),
(2, 'Quarterly'),
(3, 'Yearly');

-- --------------------------------------------------------

--
-- Table structure for table `platforms`
--

CREATE TABLE `platforms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platforms`
--

INSERT INTO `platforms` (`id`, `name`) VALUES
(1, 'PHP'),
(2, '.NET'),
(3, 'VB.NET');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `feature` text NOT NULL,
  `module` text NOT NULL,
  `demo_link` varchar(255) NOT NULL,
  `demo_access` text NOT NULL,
  `screenshot` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `platform_id`, `description`, `feature`, `module`, `demo_link`, `demo_access`, `screenshot`, `status`, `created`, `modified`) VALUES
(1, 'Ceramics POS', 2, 'This product is basically development for Sanitary and Tiles business like Ceramics product. It is UOM measurement converting to square feet to Box or Pieces. It is Stock maintains basically several warehouse.', 'Features goes here.', '1. Master Setup Management  \r\n2. Security Management \r\n3. Supplier Management \r\n4. Customer Management \r\n5. Accounts Management \r\n6. Item  Management \r\n7. Purchase Management \r\n8. Sales Management \r\n9. Report Management \r\n', '', '', 'DFD', 'active', '2017-09-11 09:02:38', '2017-09-16 05:19:14'),
(2, 'Retail and Super shop POS', 2, 'This product is basically development for Retail and Super shop business like grocery or any departmental store.', 'This product is basically development for Retail and Super shop business like grocery or any departmental store.', '1	Master Setup Management \r\n2	Security Management\r\n3	Supplier Management\r\n4	Customer Management\r\n5	Accounts Management\r\n6	Item  Management\r\n7	Purchase Management\r\n8	Sales Management\r\n9	Report Management\r\n', '', '', 'TEST', 'active', '2017-09-16 05:21:17', '2017-09-16 05:21:17'),
(3, 'Inventory Management  POS', 2, 'product is basically development for stock maintains.', 'product is basically development for stock maintains.', '1	Master Setup Management \r\n2	Security Management\r\n3	Supplier Management\r\n4	Customer Management\r\n5	Accounts Management\r\n6	Item  Management\r\n7	Purchase Management\r\n8	Sales Management\r\n9	Report Management\r\n', '', '', 'TEST', 'active', '2017-09-16 05:22:45', '2017-09-16 05:22:45'),
(4, 'Paper Maintainer POS', 2, 'This product is basically development for Press paper maintains.', 'This product is basically development for Press paper maintains.', '1	Master Setup Management \r\n2	Security Management\r\n3	Supplier Management\r\n4	Customer Management\r\n5	Accounts Management\r\n6	Item  Management\r\n7	Purchase Management\r\n8	Sales Management\r\n9	Report Management\r\n', '', '', 'SFD', 'active', '2017-09-16 05:24:14', '2017-09-16 05:24:14'),
(5, 'Attendance and Leave', 2, 'This product is basically development for Leave and attendances.', 'This product is basically development for Leave and attendances.', '1	Organization Structure  \r\n2	Configurations and setup \r\n3	Security Management\r\n4	Leave Management\r\n5	Attendance Management\r\n6	Travel on Duty  Management\r\n7	Employee Management\r\n8	Report Management\r\n', '', '', 'ASDF', 'active', '2017-09-16 05:25:28', '2017-09-16 05:25:28'),
(6, 'Attendance and Leave with Payroll', 2, 'This product is basically development for Leave and attendances.', 'This product is basically development for Leave and attendances.', '1	Organization Structure  \r\n2	Configurations and setup \r\n3	Security Management\r\n4	Leave Management\r\n5	Attendance Management\r\n6	Travel on Duty  Management\r\n7	Employee Management\r\n8	Payroll Management\r\n9	Report Management\r\n', '', '', 'Ads', 'active', '2017-09-16 05:27:52', '2017-09-16 05:27:52'),
(7, 'VAT Management Software(Desktop)', 2, 'In present market, there are no existences of any modern Value Added Tax (VAT) Accounting Software in accordance to the guidelines of National Board of Revenue (NBR), Bangladesh. UY Systems Ltd. has developed a modern VAT Accounting Automation Software, named as UY VAT MANAGEMENT SYSTEM (UYVMSÂ®), which covers all of your VAT requirements in compliance with the guidelines of National Board of Revenue (NBR), Bangladesh. It is specials software for production, manufacturing, export, import and other types of companies where in need to maintain VAT payments or VAT returns.\r\n\r\nWe also provide offshore software development services for small and large companies with cost effective VAT accounting solutions. Our service is quick and easy. With our software system, you can record, print or submit the VAT return online.', 'test', 'VAT REPORT 1991:\r\n', '', '', 'test', 'active', '2017-09-16 05:40:42', '2017-09-16 05:40:42'),
(8, 'VAT Management Software (Web)', 2, 'In present market, there are no existences of any modern Value Added Tax (VAT) Accounting Software in accordance to the guidelines of National Board of Revenue (NBR), Bangladesh. UY Systems Ltd. has developed a modern VAT Accounting Automation Software, named as UY VAT MANAGEMENT SYSTEM (UYVMSÂ®), which covers all of your VAT requirements in compliance with the guidelines of National Board of Revenue (NBR), Bangladesh. It is specials software for production, manufacturing, export, import and other types of companies where in need to maintain VAT payments or VAT returns.\r\n\r\nWe also provide offshore software development services for small and large companies with cost effective VAT accounting solutions. Our service is quick and easy. With our software system, you can record, print or submit the VAT return online.', 'test', 'VAT REPORT 1991\r\nMANAGEMENT REPORT', '', '', 'test', 'active', '2017-09-16 05:42:30', '2017-09-16 05:42:30'),
(9, 'Pharmacy POS (Desktop)', 2, 'Pharmacy POS ', 'Pharmacy POS (Desktop)', 'Pharmacy POS (Desktop)', '', '', 'test', 'active', '2017-09-16 05:43:54', '2017-09-16 05:43:54'),
(10, 'Restaurant Management System (RMS) ', 3, 'Our UY-eRMS software is a complete Restaurant Management System. Our UY-eRMS software helps to manage your restaurant activities with fewer personal involvements right from table reservation to that incoming takeaway order and everything in between.\r\n\r\nUY-eRMS software is ideal for restaurants, bars, coffee shops, and many more. The software is designed to improve communication between your kitchen, lobby, and bartenders to oversee all the activities from one place. UY-eRMS handles wide varieties of task including restaurants billing, inventory management systems, payment management, table services, order management and many more. Our Restaurant Management Software has been widely accepted by many small, medium and large scale restaurants.', 'TEST', 'Global Settings\r\nCustomized Discount\r\nCustomized VAT\r\nCustomized service charge\r\nShift TableTakeaway services\r\nMenu update by Admin\r\nMultiple usersCreate food \r\ncategoryCreate menu\r\nStock setting\r\nRecipe setting\r\nItem Wise VAT \r\nsettingCreate table\r\nCreate Card Type (Visa, Master, Credit etc.)\r\nCreate Employee\r\nDefine user role by admin\r\nSet admin privilege\r\nMultiple Printer settings (ex. Kitchen, bar, etc.)\r\nCreate order and Table by selecting appropriate food menu\r\nItem wise individual price setting\r\nView Full Product inventory (stock)Split bill (If need)Simple cancel order process upon user access role defined\r\nSimple cancel item process upon user access role defined\r\nCustomer database management', 'http://103.229.80.106:8089/Portal', 'U-Panel\r\nuser: Jahid\r\npass: 123456\r\nA-Panel\r\nuser: admin\r\npass: 123456', '', 'active', '2017-09-16 05:47:42', '2017-09-17 04:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `client_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `amount_title` varchar(255) NOT NULL,
  `total_amount` float NOT NULL,
  `collected_amount` float NOT NULL,
  `pending_amount` float NOT NULL,
  `advanced_percentage` double NOT NULL,
  `agreement_date` date NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `project_category_id` int(11) NOT NULL,
  `project_status` varchar(10) DEFAULT NULL,
  `supporting_agreement` tinyint(1) NOT NULL,
  `support_duration` varchar(10) DEFAULT NULL,
  `support_amount` float DEFAULT NULL,
  `project_size` varchar(255) NOT NULL,
  `discuss_start_date` date NOT NULL,
  `discuss_member` varchar(255) NOT NULL,
  `estimate_time` varchar(255) NOT NULL,
  `frontend_assign_to` varchar(255) NOT NULL,
  `frontend_estimate_time` varchar(255) NOT NULL,
  `frontend_start_date` date NOT NULL,
  `frontend_end_date` date NOT NULL,
  `frontend_target_date` date NOT NULL,
  `backend_assigned_to` varchar(255) NOT NULL,
  `backend_target_date` date NOT NULL,
  `backend_complete_date` date NOT NULL,
  `overall_complete_date` date NOT NULL,
  `requirement_extend` varchar(255) NOT NULL,
  `price_applicable` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `client_id`, `source`, `amount_title`, `total_amount`, `collected_amount`, `pending_amount`, `advanced_percentage`, `agreement_date`, `start_date`, `end_date`, `project_category_id`, `project_status`, `supporting_agreement`, `support_duration`, `support_amount`, `project_size`, `discuss_start_date`, `discuss_member`, `estimate_time`, `frontend_assign_to`, `frontend_estimate_time`, `frontend_start_date`, `frontend_end_date`, `frontend_target_date`, `backend_assigned_to`, `backend_target_date`, `backend_complete_date`, `overall_complete_date`, `requirement_extend`, `price_applicable`, `status`, `created`, `modified`) VALUES
(26, 'VAT Management Software', 23, 'UYSYS', 'czxzc', 35000, 35000, 0, 50, '2019-08-07', '2019-06-01 00:00:00', '2019-08-07 00:00:00', 0, '2', 0, NULL, NULL, '', '0000-00-00', '', '', '', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '0000-00-00', '0000-00-00', '0000-00-00', 'yes', 'yes', 'active', '2019-08-07 13:05:06', '2019-08-07 13:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `project_categories`
--

CREATE TABLE `project_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_categories`
--

INSERT INTO `project_categories` (`id`, `name`, `status`, `created`, `modified`) VALUES
(1, 'Website', 'active', '2017-09-11 07:13:11', '2017-09-17 05:49:03'),
(2, 'E-commerce Site', 'active', '2017-09-17 05:49:12', '2017-09-17 05:49:12'),
(3, 'E-commerce Portal', 'active', '2017-09-17 05:49:21', '2017-09-17 05:49:21'),
(4, 'Software', 'active', '2017-09-17 05:49:28', '2017-09-17 05:49:28'),
(5, 'VAT Software', 'active', '2017-10-16 12:12:35', '2017-10-16 12:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `project_documents`
--

CREATE TABLE `project_documents` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `document_name` varchar(100) DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_documents`
--

INSERT INTO `project_documents` (`id`, `project_id`, `document_name`, `file`, `filename`, `status`, `created`, `modified`) VALUES
(4, 1, NULL, '1.E-Business.png', 'E-Business', NULL, '2017-11-04 06:09:12', '2017-11-04 06:09:12'),
(5, 1, NULL, '1.ICT_Business_person.png', 'ICT_Business_person', NULL, '2017-11-04 06:09:12', '2017-11-04 06:09:12'),
(6, 1, NULL, '1.ict_international_market_focus.png', 'ict_international_market_focus', NULL, '2017-11-04 06:09:13', '2017-11-04 06:09:13'),
(7, 1, NULL, '1.ict_product.png', 'ict_product', NULL, '2017-11-04 06:09:13', '2017-11-04 06:09:13'),
(19, 20, NULL, '20.pioneer.png', 'pioneer', NULL, '2017-11-04 06:23:16', '2017-11-04 06:23:16'),
(20, 20, NULL, '20.registration_form.png', 'registration_form', NULL, '2017-11-04 06:23:16', '2017-11-04 06:23:16'),
(21, 25, NULL, '25.Invoice for Youth Group  Dynamic Website development.pdf', 'Invoice for Youth Group  Dynamic Website development', NULL, '2017-11-26 09:48:04', '2017-11-26 09:48:04'),
(22, 25, NULL, '25.2nd Revised Financial Proposal for Website Development for Youth Group 2017.11.13.pdf', '2nd Revised Financial Proposal for Website Development for Youth Group 2017.11.13', NULL, '2017-11-26 09:48:04', '2017-11-26 09:48:04'),
(23, 25, NULL, '25.other required information.xlsx', 'other required information', NULL, '2017-11-26 09:48:04', '2017-11-26 09:48:04'),
(24, 25, NULL, '25.industries or sister concern information.xlsx', 'industries or sister concern information', NULL, '2017-11-26 09:48:04', '2017-11-26 09:48:04'),
(25, 25, NULL, '25.WORKORDER.pdf', 'WORKORDER', NULL, '2017-11-26 09:49:09', '2017-11-26 09:49:09');

-- --------------------------------------------------------

--
-- Table structure for table `project_members`
--

CREATE TABLE `project_members` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_transactions`
--

CREATE TABLE `project_transactions` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `transaction_no` varchar(255) NOT NULL,
  `amount` double NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_transactions`
--

INSERT INTO `project_transactions` (`id`, `project_id`, `date`, `transaction_no`, `amount`, `created`) VALUES
(1, 1, '2017-10-12', '5655', 1500122, '2017-10-07 10:15:03'),
(3, 0, '2017-10-12', 'ds', 4345, '2017-10-07 10:16:55'),
(4, 0, '2017-10-13', 'rd453', 100000, '2017-10-07 11:14:29'),
(5, 0, '2017-10-13', 'rd453', 100000, '2017-10-07 11:15:14'),
(6, 0, '2017-10-12', '45', 554344, '2017-10-07 11:15:30'),
(7, 0, '2017-10-10', 'yy', 55666, '2017-10-07 11:16:17'),
(8, 1, '2017-10-03', 'asdfa', 4455, '2017-10-07 11:17:16'),
(9, 1, '2017-10-03', 'asdfa', 50000, '2017-10-07 11:18:18'),
(10, 1, '2017-10-04', 'er', 1000000, '2017-10-07 11:18:32'),
(11, 1, '2017-10-17', 'sd', 65, '2017-10-07 11:20:44'),
(12, 2, '2017-10-01', '1', 500000, '2017-10-07 11:23:28'),
(13, 2, '2017-10-03', '2', 150000, '2017-10-07 11:23:39');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `accesslist` mediumtext NOT NULL,
  `menu_access_list` text,
  `is_deletable` tinytext NOT NULL,
  `status` tinytext NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `description`, `accesslist`, `menu_access_list`, `is_deletable`, `status`, `created`) VALUES
('54217bff-7358-4187-8e9c-0ae112142117', 'Web Manager', 'Access to web and menus', '{\"users\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"clients\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"employees\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"domains\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"hostings\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"inventories\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"projects\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"project_documents\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"project_members\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"office_certificates\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"meeting_presentaions\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"task_managements\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"team_types\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"team_managements\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"documents\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"countries\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"}}', '', 'no', 'active', '2014-09-23'),
('5423b276-712c-4958-b2c4-1034cdd1d5ac', 'System Admin', 'access everything with codding power', '{\"web_pages\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"menus\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"users\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"system_settings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"site_settings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"}}', NULL, 'no_no', 'active', '2014-09-25'),
('565d6a71-4d54-4688-bcfe-4622c0b90f99', 'Super Admin', 'Main admin', '{\"users\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\",\"admin_acl\":\"admin_acl\"},\"clients\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"designations\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"employees\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"domains\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"hostings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"inventories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_categories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"projects\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_transactions\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_members\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"office_certificates\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meetings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"task_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_files\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"countries\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"platforms\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"products\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"service_charges\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"departments\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"our_website_references\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"client_visits\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meeting_feedback_files\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"dashboards\":{\"admin_index\":\"admin_index\",\"admin_dashboard\":\"admin_dashboard\"}}', '{\"1\":{\"1\":\"1\",\"2\":\"2\",\"3\":\"3\",\"4\":\"4\",\"5\":\"5\",\"6\":\"6\",\"7\":\"7\",\"8\":\"8\",\"9\":\"9\",\"10\":\"10\",\"11\":\"11\",\"12\":\"12\",\"13\":\"13\",\"14\":\"14\",\"15\":\"15\",\"16\":\"16\"},\"2\":{\"18\":\"18\",\"19\":\"19\",\"20\":\"20\"},\"3\":{\"21\":\"21\",\"22\":\"22\",\"23\":\"23\",\"24\":\"24\",\"25\":\"25\",\"26\":\"26\",\"27\":\"27\",\"28\":\"28\"}}', '', 'active', '2015-12-01'),
('59d8bc53-240c-4476-85e8-0964cdd1d5ac', 'Sales & Marketing', 'Sales & Marketing', '{\"users\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\",\"admin_acl\":\"admin_acl\"},\"clients\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"designations\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"employees\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"domains\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"hostings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"inventories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_categories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"projects\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_transactions\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_members\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"office_certificates\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meetings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"task_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_files\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"countries\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"platforms\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"products\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"service_charges\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"departments\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"our_website_references\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"client_visits\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meeting_feedback_files\":{\"admin_index\":\"0\",\"admin_add\":\"0\",\"admin_edit\":\"0\",\"admin_delete\":\"0\"},\"dashboards\":{\"admin_index\":\"admin_index\",\"admin_dashboard\":\"admin_dashboard\"}}', '{\"1\":{\"1\":\"1\",\"12\":\"12\",\"14\":\"14\",\"15\":\"15\",\"16\":\"16\"}}', '', 'active', '2017-10-07'),
('59d8bc62-bfbc-4365-ab06-0964cdd1d5ac', 'Accounts', 'Accounts', '{\"users\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\",\"admin_acl\":\"admin_acl\"},\"clients\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"designations\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"employees\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"domains\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"hostings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"inventories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_categories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"projects\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_transactions\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_members\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"office_certificates\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meetings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"task_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"countries\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"platforms\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"products\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"service_charges\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"departments\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"our_website_references\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"client_visits\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"dashboards\":{\"admin_index\":\"admin_index\",\"admin_dashboard\":\"admin_dashboard\"}}', '{\"1\":{\"1\":\"1\",\"2\":\"2\",\"3\":\"3\",\"5\":\"5\",\"7\":\"7\",\"10\":\"10\",\"11\":\"11\",\"12\":\"12\",\"13\":\"13\",\"14\":\"14\",\"15\":\"15\",\"16\":\"16\"}}', '', 'active', '2017-10-07'),
('5a0a7dde-7114-4e86-a1c2-0054cdd1d5ac', 'Developer', 'Developer Access', '{\"users\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"roles\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\",\"admin_acl\":\"admin_acl\"},\"clients\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"designations\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"employees\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"domains\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"hostings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"inventories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_categories\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"projects\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_transactions\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"project_members\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"office_certificates\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meetings\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"task_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"team_managements\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_types\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"documents\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"document_files\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"countries\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"platforms\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"products\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"service_charges\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"departments\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"our_website_references\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"client_visits\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"meeting_feedback_files\":{\"admin_index\":\"admin_index\",\"admin_add\":\"admin_add\",\"admin_edit\":\"admin_edit\",\"admin_delete\":\"admin_delete\"},\"dashboards\":{\"admin_index\":\"admin_index\",\"admin_dashboard\":\"admin_dashboard\"}}', '{\"1\":{\"14\":\"14\",\"15\":\"15\"}}', '', 'active', '2017-11-14');

-- --------------------------------------------------------

--
-- Table structure for table `service_charges`
--

CREATE TABLE `service_charges` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `pay_schedule_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `due` varchar(255) NOT NULL,
  `remarks` text NOT NULL,
  `start_date` int(11) NOT NULL,
  `end_date` int(11) NOT NULL,
  `service_status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_charges`
--

INSERT INTO `service_charges` (`id`, `client_id`, `pay_schedule_id`, `amount`, `due`, `remarks`, `start_date`, `end_date`, `service_status_id`) VALUES
(1, 24, 1, 10500, 'Sep,17', '', 0, 0, 1),
(2, 25, 1, 3500, 'May,June July Aug,Sep17', '', 0, 0, 1),
(3, 14, 1, 5000, 'May,June July Aug,Sep17', '', 0, 0, 1),
(4, 10, 1, 11500, 'Aug, Sep 17', '', 0, 0, 1),
(5, 6, 1, 10800, 'Paid', '', 0, 0, 1),
(6, 30, 1, 3000, '', 'Problem\r\n', 0, 0, 1),
(7, 17, 1, 10000, 'Paid', '', 0, 0, 1),
(8, 31, 1, 5000, 'Sep,17', '', 0, 0, 1),
(9, 51, 2, 5646, '', '', 0, 0, 1),
(10, 11, 1, 8000, 'Paid', '', 0, 0, 1),
(11, 28, 1, 3000, 'May,June July Aug,Sep17', '', 0, 0, 1),
(12, 26, 1, 5000, '', '', 0, 0, 1),
(13, 37, 2, 1500, '', '', 0, 0, 1),
(14, 26, 1, 7500, '', '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_statuses`
--

CREATE TABLE `service_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_statuses`
--

INSERT INTO `service_statuses` (`id`, `name`) VALUES
(1, 'Running'),
(2, 'Expected'),
(3, 'Close');

-- --------------------------------------------------------

--
-- Table structure for table `task_managements`
--

CREATE TABLE `task_managements` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `total_hour` float DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `team_managements`
--

CREATE TABLE `team_managements` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `team_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `team_types`
--

CREATE TABLE `team_types` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(64) NOT NULL,
  `personal_details` text NOT NULL,
  `role_id` char(36) NOT NULL,
  `status` tinytext NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `personal_details`, `role_id`, `status`, `created`) VALUES
('542391f1-037c-41a3-a270-0cadcdd1d5ac', 'admin@uysys.com', '$2a$10$2pVjlMYsTs40DTnvd3DLvuLCBp8AyoB1KKbhqrelqhwMzcMmPI4z.', '{\"first_name\":\"UYSYS \",\"last_name\":\"ADMIN\",\"date_of_birth\":\"1988-12-14\",\"blood_group\":\"A+\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"Mirpur\",\"address_line_2\":\"Dhaka\"},\"cell\":\"+01922299933\",\"fax\":\"none\",\"skype\":\"uysystems\"}', '565d6a71-4d54-4688-bcfe-4622c0b90f99', 'active', '2014-09-25 13:54:25'),
('565d6ac9-0110-487c-9130-51dac0b90f99', 'baten@uysys.com', '$2a$10$2pVjlMYsTs40DTnvd3DLvuLCBp8AyoB1KKbhqrelqhwMzcMmPI4z.', '{\"first_name\":\"Md. Abdul\",\"last_name\":\"Baten\",\"date_of_birth\":\"2015-12-01\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '54217bff-7358-4187-8e9c-0ae112142117', 'active', '2015-12-01 15:39:21'),
('59d8bca6-354c-4486-9cdb-0964cdd1d5ac', 'arifhossen', '$2a$10$LmTcjb4I79DeZZPZwMKV9urGw8hVuqiXjTCYedthniW6RvLUgRGsi', '{\"first_name\":\"Arif \",\"last_name\":\"Hossen\",\"date_of_birth\":\"\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"married\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '565d6a71-4d54-4688-bcfe-4622c0b90f99', 'active', '2017-10-07 05:38:14'),
('59d8bcc0-fb24-402e-b4bc-0964cdd1d5ac', 'tuhin', '$2a$10$iZ72IN2d3raXSEtREdqQ0uprxdeEDZ/8L2HnGZ/AHmzvaIhDxiklO', '{\"first_name\":\"Tuhin\",\"last_name\":\"Hossain\",\"date_of_birth\":\"\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '59d8bc53-240c-4476-85e8-0964cdd1d5ac', 'active', '2017-10-07 05:38:40'),
('59d8bcf8-3134-4b4a-8aad-0964cdd1d5ac', 'shopon', '$2a$10$3E8JTfUb8dQpE8wOvqEmRuQuqWJZ.Hjgprmf/Uejf8XBCXcFyHY9S', '{\"first_name\":\"shopon\",\"last_name\":\"Hossain\",\"date_of_birth\":\"\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"married\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '59d8bc62-bfbc-4365-ab06-0964cdd1d5ac', 'active', '2017-10-07 05:39:36'),
('59db2a84-c2c4-439a-8389-2bcccdd1d5ac', 'bristy', '$2a$10$TxvMz.T8FVvhByBPqtD7puZJcq1pxBENtaGdhbZAEGMzaq5dKLVo2', '{\"first_name\":\"Bristy\",\"last_name\":\"\",\"date_of_birth\":\"\",\"blood_group\":\"\",\"gender\":\"female\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '59d8bc53-240c-4476-85e8-0964cdd1d5ac', 'active', '2017-10-09 01:51:32'),
('5a0a7f21-14b0-49e1-a166-0054cdd1d5ac', 'jafor', '$2a$10$Fp3aMrVk.TBSlWZ4jquPc.BGP2vcre4JWEDEv7SaSwfBmFq64LwXe', '{\"first_name\":\"Jafar \",\"last_name\":\"Ahmed \",\"date_of_birth\":\"2001-11-14\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '5a0a7dde-7114-4e86-a1c2-0054cdd1d5ac', 'active', '2017-11-13 23:29:05'),
('5a0a8dcd-a364-466f-b888-0054cdd1d5ac', 'omi', '$2a$10$rGfIw/MuE4qh7f7NcFAJ6.BIeShbbHwu1lOgRLXuT7y3FA69RiIX2', '{\"first_name\":\"Omi\",\"last_name\":\"\",\"date_of_birth\":\"\",\"blood_group\":\"\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"\",\"address_line_2\":\"\"},\"cell\":\"\",\"fax\":\"\",\"skype\":\"\"}', '5a0a7dde-7114-4e86-a1c2-0054cdd1d5ac', 'active', '2017-11-14 00:31:41'),
('5a0a8dcd-a364-466f-b888-0054cdd1d5bc', 'sakil@uysys.com', '143bf0ac74cd4d3c5a78ce3111bbc3688abcce70', '{\"first_name\":\"UYSYS \",\"last_name\":\"ADMIN\",\"date_of_birth\":\"1988-12-14\",\"blood_group\":\"A+\",\"gender\":\"male\",\"maritial_status\":\"single\",\"current_address\":{\"address_line_1\":\"Mirpur\",\"address_line_2\":\"Dhaka\"},\"cell\":\"+01922299933\",\"fax\":\"none\",\"skype\":\"uysystems\"}', '565d6a71-4d54-4688-bcfe-4622c0b90f99', 'active', '2014-09-25 13:54:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_actions`
--
ALTER TABLE `acl_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acl_controllers`
--
ALTER TABLE `acl_controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_types`
--
ALTER TABLE `client_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_visits`
--
ALTER TABLE `client_visits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_categories`
--
ALTER TABLE `document_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_files`
--
ALTER TABLE `document_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostings`
--
ALTER TABLE `hostings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventories`
--
ALTER TABLE `inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting_feedback_files`
--
ALTER TABLE `meeting_feedback_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_certificates`
--
ALTER TABLE `office_certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_website_references`
--
ALTER TABLE `our_website_references`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pay_schedules`
--
ALTER TABLE `pay_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platforms`
--
ALTER TABLE `platforms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_categories`
--
ALTER TABLE `project_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_documents`
--
ALTER TABLE `project_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_members`
--
ALTER TABLE `project_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_transactions`
--
ALTER TABLE `project_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_charges`
--
ALTER TABLE `service_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_statuses`
--
ALTER TABLE `service_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_managements`
--
ALTER TABLE `task_managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_managements`
--
ALTER TABLE `team_managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_types`
--
ALTER TABLE `team_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_actions`
--
ALTER TABLE `acl_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `acl_controllers`
--
ALTER TABLE `acl_controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `client_types`
--
ALTER TABLE `client_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `client_visits`
--
ALTER TABLE `client_visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `document_categories`
--
ALTER TABLE `document_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `document_files`
--
ALTER TABLE `document_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `hostings`
--
ALTER TABLE `hostings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inventories`
--
ALTER TABLE `inventories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `meeting_feedback_files`
--
ALTER TABLE `meeting_feedback_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `office_certificates`
--
ALTER TABLE `office_certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `our_website_references`
--
ALTER TABLE `our_website_references`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `pay_schedules`
--
ALTER TABLE `pay_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `platforms`
--
ALTER TABLE `platforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `project_categories`
--
ALTER TABLE `project_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `project_documents`
--
ALTER TABLE `project_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `project_members`
--
ALTER TABLE `project_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_transactions`
--
ALTER TABLE `project_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `service_charges`
--
ALTER TABLE `service_charges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `service_statuses`
--
ALTER TABLE `service_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `task_managements`
--
ALTER TABLE `task_managements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `team_managements`
--
ALTER TABLE `team_managements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `team_types`
--
ALTER TABLE `team_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
