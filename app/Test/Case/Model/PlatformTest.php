<?php
App::uses('Platform', 'Model');

/**
 * Platform Test Case
 *
 */
class PlatformTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.platform',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Platform = ClassRegistry::init('Platform');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Platform);

		parent::tearDown();
	}

}
