<?php
App::uses('DeveloperProfile', 'Model');

/**
 * DeveloperProfile Test Case
 *
 */
class DeveloperProfileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.developer_profile',
		'app.developer',
		'app.developer_bank_profile'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DeveloperProfile = ClassRegistry::init('DeveloperProfile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DeveloperProfile);

		parent::tearDown();
	}

}
