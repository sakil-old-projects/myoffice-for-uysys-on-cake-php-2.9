<?php
App::uses('Baten', 'Model');

/**
 * Baten Test Case
 *
 */
class BatenTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.baten',
		'app.developer',
		'app.developer_bank_profile',
		'app.developer_profile'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Baten = ClassRegistry::init('Baten');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Baten);

		parent::tearDown();
	}

}
