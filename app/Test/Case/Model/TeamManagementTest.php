<?php
App::uses('TeamManagement', 'Model');

/**
 * TeamManagement Test Case
 *
 */
class TeamManagementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.team_management',
		'app.employee',
		'app.project_member',
		'app.project',
		'app.client',
		'app.country',
		'app.project_document',
		'app.task_management',
		'app.team_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TeamManagement = ClassRegistry::init('TeamManagement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TeamManagement);

		parent::tearDown();
	}

}
