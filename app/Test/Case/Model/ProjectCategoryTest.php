<?php
App::uses('ProjectCategory', 'Model');

/**
 * ProjectCategory Test Case
 *
 */
class ProjectCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_category',
		'app.project',
		'app.client',
		'app.country',
		'app.project_document',
		'app.project_member',
		'app.employee',
		'app.task_management',
		'app.team_management',
		'app.team_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectCategory = ClassRegistry::init('ProjectCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectCategory);

		parent::tearDown();
	}

}
