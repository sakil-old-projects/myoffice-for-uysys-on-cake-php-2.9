<?php
App::uses('Context', 'Model');

/**
 * Context Test Case
 *
 */
class ContextTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.context',
		'app.doc'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Context = ClassRegistry::init('Context');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Context);

		parent::tearDown();
	}

}
