<?php
App::uses('ProjectMember', 'Model');

/**
 * ProjectMember Test Case
 *
 */
class ProjectMemberTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_member',
		'app.project',
		'app.employee',
		'app.task_management',
		'app.team_management'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectMember = ClassRegistry::init('ProjectMember');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectMember);

		parent::tearDown();
	}

}
