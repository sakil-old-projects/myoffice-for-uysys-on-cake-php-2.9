<?php
App::uses('DeveloperBankProfile', 'Model');

/**
 * DeveloperBankProfile Test Case
 *
 */
class DeveloperBankProfileTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.developer_bank_profile',
		'app.developer',
		'app.developer_profile'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->DeveloperBankProfile = ClassRegistry::init('DeveloperBankProfile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->DeveloperBankProfile);

		parent::tearDown();
	}

}
