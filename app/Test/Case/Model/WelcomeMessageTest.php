<?php
App::uses('WelcomeMessage', 'Model');

/**
 * WelcomeMessage Test Case
 *
 */
class WelcomeMessageTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.welcome_message'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->WelcomeMessage = ClassRegistry::init('WelcomeMessage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->WelcomeMessage);

		parent::tearDown();
	}

}
