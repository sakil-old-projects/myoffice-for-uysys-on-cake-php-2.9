<?php
App::uses('TaskManagement', 'Model');

/**
 * TaskManagement Test Case
 *
 */
class TaskManagementTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.task_management',
		'app.employee',
		'app.project_member',
		'app.project',
		'app.client',
		'app.country',
		'app.project_document',
		'app.team_management'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TaskManagement = ClassRegistry::init('TaskManagement');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TaskManagement);

		parent::tearDown();
	}

}
