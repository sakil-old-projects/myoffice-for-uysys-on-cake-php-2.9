<?php
App::uses('MenuGroup', 'Model');

/**
 * MenuGroup Test Case
 *
 */
class MenuGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.menu_group',
		'app.menu'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MenuGroup = ClassRegistry::init('MenuGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MenuGroup);

		parent::tearDown();
	}

}
