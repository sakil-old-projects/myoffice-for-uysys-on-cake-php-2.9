<?php
App::uses('OfficeCertificate', 'Model');

/**
 * OfficeCertificate Test Case
 *
 */
class OfficeCertificateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.office_certificate'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OfficeCertificate = ClassRegistry::init('OfficeCertificate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OfficeCertificate);

		parent::tearDown();
	}

}
