<?php
App::uses('MeetingPresentation', 'Model');

/**
 * MeetingPresentation Test Case
 *
 */
class MeetingPresentationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.meeting_presentation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MeetingPresentation = ClassRegistry::init('MeetingPresentation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MeetingPresentation);

		parent::tearDown();
	}

}
