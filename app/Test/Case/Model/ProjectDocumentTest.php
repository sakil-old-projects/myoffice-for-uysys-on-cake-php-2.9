<?php
App::uses('ProjectDocument', 'Model');

/**
 * ProjectDocument Test Case
 *
 */
class ProjectDocumentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.project_document',
		'app.project'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProjectDocument = ClassRegistry::init('ProjectDocument');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProjectDocument);

		parent::tearDown();
	}

}
