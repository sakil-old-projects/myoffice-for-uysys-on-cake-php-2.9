<?php
App::uses('ProductFeature', 'Model');

/**
 * ProductFeature Test Case
 *
 */
class ProductFeatureTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.product_feature',
		'app.product'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProductFeature = ClassRegistry::init('ProductFeature');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProductFeature);

		parent::tearDown();
	}

}
