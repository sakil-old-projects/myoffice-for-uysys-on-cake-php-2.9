<?php
/**
 * ProjectFixture
 *
 */
class ProjectFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'client_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'total_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'collected_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'pending_amount' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'start_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'category' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'project_status' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'supporting_agreement' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'support_duration' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'support_amount' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'status' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 10, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'client_id' => 1,
			'total_amount' => 1,
			'collected_amount' => 1,
			'pending_amount' => 1,
			'start_date' => '2017-09-09 06:49:54',
			'end_date' => '2017-09-09 06:49:54',
			'category' => 'Lorem ip',
			'project_status' => 'Lorem ip',
			'supporting_agreement' => 1,
			'support_duration' => 'Lorem ip',
			'support_amount' => 1,
			'status' => 'Lorem ip',
			'created' => '2017-09-09 06:49:54',
			'modified' => '2017-09-09 06:49:54'
		),
	);

}
