[
 {
	"title": "main_menu",
	"plugins": ["default"],
	"controllers": ["dashboards","clients","domains","projects","employees","task_managements","inventories","team_managements","project_members","documents","office_certificates","products","service_charges","our_website_references","client_visits","meetings"],
	"actions": "",
	"admin": "true",
	"order": 0,
	"status": "active",
	"children": [{
		"title": "clients",
		"plugins": "false",
		"controllers": "clients",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "domains",
		"plugins": "false",
		"controllers": "domains",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "projects",
		"plugins": "false",
		"controllers": "projects",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "employees",
		"plugins": "false",
		"controllers": "employees",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "task_managements",
		"plugins": "false",
		"controllers": "task_managements",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "inventories",
		"plugins": "false",
		"controllers": "inventories",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "team_managements",
		"plugins": "false",
		"controllers": "team_managements",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "project_members",
		"plugins": "false",
		"controllers": "project_members",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "office_certificates",
		"plugins": "false",
		"controllers": "office_certificates",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "documents",
		"plugins": "false",
		"controllers": "documents",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "products",
		"plugins": "false",
		"controllers": "products",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "service_charges",
		"plugins": "false",
		"controllers": "service_charges",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "our_website_references",
		"plugins": "false",
		"controllers": "our_website_references",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "client_visits",
		"plugins": "false",
		"controllers": "client_visits",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "meetings",
		"plugins": "false",
		"controllers": "meetings",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	}
	,{
		"title": "dashboards",
		"plugins": "false",
		"controllers": "dashboards",
		"actions": "dashboard",
		"params": "",
		"admin": "true",
		"status": "active"
	}]
},
{
	"title": "user_manager",
	"plugins": ["default"],
	"controllers": ["users", "roles"],
	"actions": "",
	"admin": "true",
	"order": 0,
	"status": "active",
	"children": [{
		"title": "list_all_users",
		"plugins": "false",
		"controllers": "users",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	}, {
		"title": "new_user",
		"plugins": "false",
		"controllers": "users",
		"actions": "add",
		"params": "",
		"admin": "true",
		"status": "active"
	}, {
		"title": "roles",
		"plugins": "false",
		"controllers": "roles",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},{
		"title": "generate_acl",
		"plugins": "false",
		"controllers": "roles",
		"actions": "acl",
		"params": "",
		"admin": "true",
		"status": "active"
	}
	]
}
,
{
	"title": "general_settings",
	"plugins": ["default"],
	"controllers": ["departments","hostings","project_categories","document_types","designations","team_types","countries","platforms"],
	"actions": "",
	"admin": "true",
	"order": 0,
	"status": "active",
	"children": [
	             
     {
 		"title": "departments",
 		"plugins": "false",
 		"controllers": "departments",
 		"actions": "index",
 		"params": "",
 		"admin": "true",
 		"status": "active"
 	},
	 {
		"title": "hostings",
		"plugins": "false",
		"controllers": "hostings",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	             
	{
		"title": "project_categories",
		"plugins": "false",
		"controllers": "project_categories",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	{
		"title": "document_types",
		"plugins": "false",
		"controllers": "document_types",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	 {
		"title": "designations",
		"plugins": "false",
		"controllers": "designations",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	{
		"title": "team_types",
		"plugins": "false",
		"controllers": "team_types",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	{
		"title": "countries",
		"plugins": "false",
		"controllers": "countries",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	},
	{
		"title": "platforms",
		"plugins": "false",
		"controllers": "platforms",
		"actions": "index",
		"params": "",
		"admin": "true",
		"status": "active"
	}
	]
}


    
]