<?php
$arr=array(
	'0'=>array(
		'title'			=>'dashboard',
		'plugins'		=>array(),	
		'controllers'	=>array('dashboards'),
		'action'		=>'index',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(),
	),
	'1'=>array(
		'title'			=>'page_manager',
		'plugins'		=>array('default'),
		'controllers'	=>array('web_pages'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'list_all',
				'plugins'		=>'false',
				'controllers'	=>'web_pages',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'new_page',
				'plugins'		=>'false',
				'controllers'	=>'web_pages',
				'action'		=>'add',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)
		
	),
	'2'=>array(
		'title'			=>'menu_manager',
		'plugins'		=>array('default'),
		'controllers'	=>array('menus'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'list_all_menu',
				'plugins'		=>'false',
				'controllers'	=>'menus',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'sort_menu',
				'plugins'		=>'false',
				'controllers'	=>'menus',
				'action'		=>'sort_menu',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)
		
	),
	'3'=>array(
		'title'			=>'user_manager',
		'plugins'		=>array('default'),
		'controllers'	=>array('users','roles'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'list_all_users',
				'plugins'		=>'false',
				'controllers'	=>'users',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'new_user',
				'plugins'		=>'false',
				'controllers'	=>'users',
				'action'		=>'add',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'2'	=>array(
				'title'			=>'roles',
				'plugins'		=>'false',
				'controllers'	=>'roles',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)
		
	),
	'4'=>array(
		'title'			=>'site_settings',
		'plugins'		=>array('default'),
		'controllers'	=>array('site_settings'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'settings',
				'plugins'		=>'false',
				'controllers'	=>'site_settings',
				'action'		=>'edit',
				'params'		=>'54219a9b-f910-4d8c-9515-0ae112142117',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)
		
	),
	'5'=>array(
		'title'			=>'advertise',
		'plugins'		=>array('default'),
		'controllers'	=>array('advertises'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'list_all_advertise',
				'plugins'		=>'false',
				'controllers'	=>'advertises',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'6'=>array(
		'title'			=>'social_network',
		'plugins'		=>array('default'),
		'controllers'	=>array('social_networks'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'list_all',
				'plugins'		=>'false',
				'controllers'	=>'social_networks',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'add_new',
				'plugins'		=>'false',
				'controllers'	=>'social_networks',
				'action'		=>'add',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'2'	=>array(
				'title'			=>'sort_network',
				'plugins'		=>'false',
				'controllers'	=>'social_networks',
				'action'		=>'sort_socialnetwork',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)
		
	),
	'7'=>array(
		'title'			=>'shop_manager',
		'plugins'		=>array('ecommerce'),
		'controllers'	=>array('brands','categories','products','types','attributes','attribute_values','shipping_methods','stores','MerchantBrands','offers','featureCategory'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'merchant_products',
				'plugins'		=>'ecommerce',
				'controllers'	=>'products',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'classified_products',
				'plugins'		=>'ecommerce',
				'controllers'	=>'products',
				'action'		=>'classifiedproducts',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'2'	=>array(
				'title'			=>'brands',
				'plugins'		=>'ecommerce',
				'controllers'	=>'brands',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'3'	=>array(
				'title'			=>'categories',
				'plugins'		=>'ecommerce',
				'controllers'	=>'categories',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'4'	=>array(
				'title'			=>'merchant_brands',
				'plugins'		=>'ecommerce',
				'controllers'	=>'MerchantBrands',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'5'	=>array(
				'title'			=>'offers',
				'plugins'		=>'ecommerce',
				'controllers'	=>'offers',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)

		)	
	),
	'8'=>array(
		'title'			=>'configurations',
		'plugins'		=>array('ecommerce'),
		'controllers'	=>array('types','shipping_methods'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'type_configuration',
				'plugins'		=>'ecommerce',
				'controllers'	=>'types',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'9'=>array(
		'title'			=>'order_manager',
		'plugins'		=>array('ecommerce'),
		'controllers'	=>array('product_orders'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'orders',
				'plugins'		=>'ecommerce',
				'controllers'	=>'product_orders',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'10'=>array(
		'title'			=>'billings',
		'plugins'		=>array('ecommerce','timeout'),
		'controllers'	=>array('products','product_orders','exclusive_sells','exclusive_buys'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'stock_report',
				'plugins'		=>'ecommerce',
				'controllers'	=>'products',
				'action'		=>'billing',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'order_transaction_report',
				'plugins'		=>'ecommerce',
				'controllers'	=>'product_orders',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'2'	=>array(
				'title'			=>'exclusive_buy_payments',
				'plugins'		=>'timeout',
				'controllers'	=>'exclusive_buys',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'3'	=>array(
				'title'			=>'exclusive_sell_payments',
				'plugins'		=>'timeout',
				'controllers'	=>'exclusive_sells',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'11'=>array(
		'title'			=>'ship_manager',
		'plugins'		=>array('shipping'),
		'controllers'	=>array('countries','cities','channels','dimensionalWeights'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'country',
				'plugins'		=>'shipping',
				'controllers'	=>'countries',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'city',
				'plugins'		=>'shipping',
				'controllers'	=>'cities',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'2'	=>array(
				'title'			=>'channels',
				'plugins'		=>'shipping',
				'controllers'	=>'channels',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'3'	=>array(
				'title'			=>'dimensional_weight',
				'plugins'		=>'shipping',
				'controllers'	=>'dimensionalWeights',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'12'=>array(
		'title'			=>'free_merchant',
		'plugins'		=>array('timeout'),
		'controllers'	=>array('clients'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'all_free_merchant',
				'plugins'		=>'timeout',
				'controllers'	=>'clients',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'13'=>array(
		'title'			=>'blog_manager',
		'plugins'		=>array('blog'),
		'controllers'	=>array('blogCategories','posts'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'categories',
				'plugins'		=>'blog',
				'controllers'	=>'blogCategories',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			),
			'1'	=>array(
				'title'			=>'posts',
				'plugins'		=>'blog',
				'controllers'	=>'posts',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),
	'14'=>array(
		'title'			=>'merchant',
		'plugins'		=>array('merchant'),
		'controllers'	=>array('merchants'),
		'action'		=>'',
		'admin'			=>'true',
		'order'			=>0,
		'status'		=>'active',
		'children'		=>array(
			'0'	=>array(
				'title'			=>'all_merchant',
				'plugins'		=>'merchant',
				'controllers'	=>'merchants',
				'action'		=>'index',
				'params'		=>'',
				'admin'			=>'true',
				'status'		=>'active',
			)
		)	
	),

);

echo json_encode($arr);
?>