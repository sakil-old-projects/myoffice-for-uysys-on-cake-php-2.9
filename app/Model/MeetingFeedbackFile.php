<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property Country $Country
 * @property Project $Project
 */
class MeetingFeedbackFile extends AppModel {



	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Meeting' => array(
			'className' => 'Meeting',
			'foreignKey' => 'meeting_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		 
	);


}
