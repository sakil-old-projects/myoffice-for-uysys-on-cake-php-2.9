<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property Country $Country
 * @property Project $Project
 */
class Meeting extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'company_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'company' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MeetingFeedbackFile' => array(
			'className' => 'MeetingFeedbackFile',
			'foreignKey' => 'meeting_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	

}
