<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property Country $Country
 * @property Project $Project
 */
class ClientVisit extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'company_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'client_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
	'Employee' => array(
			'className' => 'Employee',
			'foreignKey' => 'employee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


}
