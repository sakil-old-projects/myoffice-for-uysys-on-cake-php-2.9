<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 *
 */
class DocumentFile extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'document_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		 
	);

}
