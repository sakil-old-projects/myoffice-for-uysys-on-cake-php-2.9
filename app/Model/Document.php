<?php
App::uses('AppModel', 'Model');
/**
 * Document Model
 *
 */
class Document extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	
	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'DocumentType' => array(
			'className' => 'DocumentType',
			'foreignKey' => 'document_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		 
	);
	
	
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'DocumentFile' => array(
			'className' => 'DocumentFile',
			'foreignKey' => 'document_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
