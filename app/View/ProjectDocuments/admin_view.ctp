<h1 class="page-title"><?php echo __('Project Document details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['id']); ?></dd>

	<dt><?php echo __('Project'); ?></dt>
	<dd>\<?php echo $this->Html->link($projectDocument['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projectDocument['Project']['id'])); ?><dd>

	<dt><?php echo __('Document Name'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['document_name']); ?></dd>

	<dt><?php echo __('File Name'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['file_name']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($projectDocument['ProjectDocument']['modified']); ?></dd>

</dl>
