<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Meeting'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Meeting Presentations', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Meeting',array('class'=>'form','type'=>'file')); 
	
		echo $this->Form->input('company',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('location',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('phone',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('key_person',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('type',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('meeting_summary',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		
		echo $this->Form->input('multiple_files.', array('label'=>'Select Multiple File','type' => 'file', 'multiple','required'=>false));
		
		

		echo "<br>";
		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>



<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#MeetingDate').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: "HH:mm:ss"
	});
});

</script>
<?php $this->end(); ?>
