<h1 class="page-title"><?php echo __('Meeting Presentation details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['id']); ?></dd>

	<dt><?php echo __('Company'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['company']); ?></dd>

	<dt><?php echo __('Date'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['date']); ?></dd>

	<dt><?php echo __('Time'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['time']); ?></dd>

	<dt><?php echo __('Location'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['location']); ?></dd>

	<dt><?php echo __('Phone'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['phone']); ?></dd>

	<dt><?php echo __('Key Person'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['key_person']); ?></dd>

	<dt><?php echo __('Type'); ?></dt>
	<dd><?php echo h($meetingPresentation['MeetingPresentation']['type']); ?></dd>

</dl>
