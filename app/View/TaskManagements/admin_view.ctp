<h1 class="page-title"><?php echo __('Task Management details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['id']); ?></dd>

	<dt><?php echo __('Employee'); ?></dt>
	<dd>\<?php echo $this->Html->link($taskManagement['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $taskManagement['Employee']['id'])); ?><dd>

	<dt><?php echo __('Title'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['title']); ?></dd>

	<dt><?php echo __('Description'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['description']); ?></dd>

	<dt><?php echo __('Start Time'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['start_time']); ?></dd>

	<dt><?php echo __('End Time'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['end_time']); ?></dd>

	<dt><?php echo __('Total Hour'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['total_hour']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($taskManagement['TaskManagement']['status']); ?></dd>

</dl>
