<h1 class="page-title"><?php echo __('Team Type details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($teamType['TeamType']['id']); ?></dd>

	<dt><?php echo __('Title'); ?></dt>
	<dd><?php echo h($teamType['TeamType']['title']); ?></dd>

</dl>
