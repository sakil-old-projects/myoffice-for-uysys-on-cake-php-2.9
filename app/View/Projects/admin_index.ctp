



<div class="box box-first document-search">
	<h3 class="box-title">Projects</h3>
	<div class="box-body">
		<?php echo $this->Form->create('Project',array('class'=>'form','data-role'=>'form')); ?>
		<div class="col-md-4">
			<?php 
					
			echo $this->Form->input('id',array('empty'=>array(''=>'Select Project'),'options'=>$projectLists,'div'=>array('class'=>'form-group'),'label'=>false,'required'=>false,'class'=>'form-control form-select'));
			echo $this->Form->input('client_id',array('empty'=>array(''=>'Select Client'),'div'=>array('class'=>'form-group'),'label'=>false,'required'=>false,'class'=>'form-control form-select'));
			?>
		</div>
		<div class="col-md-4">		
			<?php 
			echo $this->Form->input('project_category_id',array('empty'=>array(''=>'Select Category'),'div'=>array('class'=>'form-group'),'label'=>false,'required'=>false,'class'=>'form-control form-select'));
			echo $this->Form->input('project_status',array('empty'=>array(''=>'Select Status'),'options'=>$projectStatus,'div'=>array('class'=>'form-group'),'label'=>false,'required'=>false,'class'=>'form-control form-select'));		
			?>
		</div>
		
		<div class="col-md-4">
			<?php echo $this->Form->button('<i class="fa fa-search"></i> Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-common'));?>
		</div>
		
		<?php echo $this->Form->end(); ?>
	</div>
</div>




<div class="box">
<h3 class="box-title"><?php echo __('Projects'); ?>
 <?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add Project', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right')); ?></h3>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table" >
				<thead>
				<tr>
				
					<th><?php echo $this->Paginator->sort('client_id'); ?></th>
					<th><?php echo $this->Paginator->sort('Project Title'); ?></th>
					<th><?php echo $this->Paginator->sort('Source'); ?></th>
					<th><?php echo $this->Paginator->sort('Amount Title'); ?></th>
					<th><?php echo $this->Paginator->sort('total_amount'); ?></th>
					<th><?php echo $this->Paginator->sort('collected_amount'); ?></th>
					<th><?php echo $this->Paginator->sort('pending_amount'); ?></th>
					<th><?php echo $this->Paginator->sort('Advanced %'); ?></th>
					<th><?php echo $this->Paginator->sort('start_date'); ?></th>
					<th><?php echo $this->Paginator->sort('end_date'); ?></th>
					<th><?php echo $this->Paginator->sort('project_status'); ?></th>
				
					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($projects as $project): ?>
				<tr>
					
					<td>
						<?php echo $this->Html->link($project['Client']['company_name'], array('controller' => 'clients', 'action' => 'view', $project['Client']['id'])); ?>
					</td>
					<td><?php echo h($project['Project']['name']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['source']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['amount_title']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['total_amount']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['collected_amount']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['pending_amount']); ?>&nbsp;</td>
					<td><?php echo h($project['Project']['advanced_percentage']); ?>&nbsp;</td>
					<td><?php echo h(date('d-m-Y',strtotime($project['Project']['start_date']))); ?>&nbsp;</td>
					<td><?php echo h(date('d-m-Y',strtotime($project['Project']['end_date']))); ?>&nbsp;</td>
					<td><?php echo h($projectStatus[$project['Project']['project_status']]); ?>&nbsp;</td>
				
					<td class="text-right action">
						<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('controller'=>'project_transactions','action' => 'index', $project['Project']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
						<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $project['Project']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
						<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $project['Project']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
					
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev( __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next'), array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	