<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Project'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Project', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Project',array('class'=>'form','type'=>'file')); 
	
		
		echo $this->Form->input('client_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('name',array('label'=>'Project Title','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('source',array('class'=>'form-control','div'=>array('class'=>'form-group')));	
		echo $this->Form->input('amount_title',array('class'=>'form-control','div'=>array('class'=>'form-group')));		
		echo $this->Form->input('total_amount',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('collected_amount',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('pending_amount',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('advanced_percentage',array('label'=>'Advanced %','class'=>'form-control','div'=>array('class'=>'form-group')));
		
		echo $this->Form->input('agreement_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		
		echo $this->Form->input('start_date',array('type'=>'text','div'=>array('class'=>'form-group'),'class'=>'form-control input-datepicker-addon'));
		echo $this->Form->input('end_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group input-datepicker-addon')));
		
		echo $this->Form->input('project_status',array('options'=>$projectStatus,'class'=>'form-control','div'=>array('class'=>'form-group')));

		
		echo $this->Form->input('project_size',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('discuss_start_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('discuss_member',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('estimate_time',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('frontend_assign_to',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('frontend_estimate_time',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('frontend_start_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('frontend_end_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('frontend_target_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('backend_assigned_to',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('backend_target_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('backend_complete_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('overall_complete_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('requirement_extend',array('options'=>array('yes'=>'Yes','no'=>'No'),'class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('price_applicable',array('options'=>array('yes'=>'Yes','no'=>'No'),'class'=>'form-control','div'=>array('class'=>'form-group')));
		
		
		echo $this->Form->input('multiple_files.', array('label'=>'Select Multiple File','type' => 'file', 'multiple','required'=>false));
	
		
		
		
		echo "<br>";
		echo $this->Form->input('status',array('options'=>$status,'class'=>'form-control','div'=>array('class'=>'form-group')));

		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>

<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#ProjectAgreementDate,#ProjectStartDate,#ProjectEndDate,#ProjectDiscussStartDate,#ProjectFrontendStartDate,#ProjectFrontendEndDate,#ProjectFrontendTargetDate,#ProjectBackendTargetDate,#ProjectBackendCompleteDate,#ProjectOverallCompleteDate').datepicker({
		dateFormat: 'yy-mm-dd'
		
	});
});

</script>
<?php $this->end(); ?>
