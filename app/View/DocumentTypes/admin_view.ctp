<h1 class="page-title"><?php echo __('Document Type details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($documentType['DocumentType']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($documentType['DocumentType']['name']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($documentType['DocumentType']['status']); ?></dd>

</dl>
