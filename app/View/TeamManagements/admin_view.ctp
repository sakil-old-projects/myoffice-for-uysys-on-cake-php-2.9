<h1 class="page-title"><?php echo __('Team Management details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($teamManagement['TeamManagement']['id']); ?></dd>

	<dt><?php echo __('Employee'); ?></dt>
	<dd>\<?php echo $this->Html->link($teamManagement['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $teamManagement['Employee']['id'])); ?><dd>

	<dt><?php echo __('Team Type'); ?></dt>
	<dd>\<?php echo $this->Html->link($teamManagement['TeamType']['title'], array('controller' => 'team_types', 'action' => 'view', $teamManagement['TeamType']['id'])); ?><dd>

</dl>
