<h1 class="page-title"><?php echo __('Domain details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($domain['Domain']['id']); ?></dd>

	<dt><?php echo __('Domain Name'); ?></dt>
	<dd><?php echo h($domain['Domain']['domain_name']); ?></dd>

	<dt><?php echo __('Hosting'); ?></dt>
	<dd>\<?php echo $this->Html->link($domain['Hosting']['name'], array('controller' => 'hostings', 'action' => 'view', $domain['Hosting']['id'])); ?><dd>

	<dt><?php echo __('Provider'); ?></dt>
	<dd><?php echo h($domain['Domain']['provider']); ?></dd>

	<dt><?php echo __('Renew Date'); ?></dt>
	<dd><?php echo h($domain['Domain']['renew_date']); ?></dd>

	<dt><?php echo __('Expire Date'); ?></dt>
	<dd><?php echo h($domain['Domain']['expire_date']); ?></dd>

	<dt><?php echo __('Phone'); ?></dt>
	<dd><?php echo h($domain['Domain']['phone']); ?></dd>

	<dt><?php echo __('Client'); ?></dt>
	<dd><?php echo h($domain['Domain']['client']); ?></dd>

	<dt><?php echo __('Address'); ?></dt>
	<dd><?php echo h($domain['Domain']['address']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($domain['Domain']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($domain['Domain']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($domain['Domain']['modified']); ?></dd>

</dl>
