<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Domain'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Domains', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Domain',array('class'=>'form')); 
	
		echo $this->Form->input('domain_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('hosting_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('provider',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('renew_date',array('type'=>'text','div'=>array('class'=>'form-group'),'class'=>'form-control datepicker'));
		echo $this->Form->input('expire_date',array('type'=>'text','div'=>array('class'=>'form-group'),'class'=>'form-control datepicker'));
		
		echo $this->Form->input('phone',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('client',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('address',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('status',array('options'=>$status,'class'=>'form-control','div'=>array('class'=>'form-group')));

		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>
