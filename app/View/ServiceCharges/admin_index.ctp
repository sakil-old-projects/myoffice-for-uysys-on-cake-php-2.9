<div class="box box-first document-search">
	<h3 class="box-title">Search Service Charge</h3>
	<div class="box-body">
		<?php echo $this->Form->create('ServiceCharge',array('class'=>'form','data-role'=>'form')); 	?>
		<div class="col-md-4">
			<?php 
			echo $this->Form->input('keywords',array('class'=>'form-control','div'=>array('class'=>'form-group')));		
			
			?>
		</div>
		
		
		<div class="col-md-4">
			<?php 			
			echo $this->Form->button('<i class="fa fa-search"></i> Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-common'));
			?>
		</div>
		
		<?php echo $this->Form->end(); ?>	</div>
</div>


<div class="box">
<h3 class="box-title"><?php echo __('Service Charges Lists'); ?>
 <?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add New Service Charges', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right')); ?></h3>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table" >
				<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('client'); ?></th>
					<th><?php echo $this->Paginator->sort('pay_schedule_id'); ?></th>
					<th><?php echo $this->Paginator->sort('amount'); ?></th>
					<th><?php echo $this->Paginator->sort('due'); ?></th>
					<th><?php echo $this->Paginator->sort('remarks'); ?></th>							
					<th><?php echo $this->Paginator->sort('status'); ?></th>					
					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			
			<tbody>
			<?php foreach ($serviceCharges as $serviceCharge): ?>
				<tr>
					<td><?php echo h($serviceCharge['ServiceCharge']['id']); ?>&nbsp;</td>
					
					<td>
						<?php echo $this->Html->link($serviceCharge['Client']['company_name'], array('controller' => 'clients', 'action' => 'view', $serviceCharge['Client']['id'])); ?>
					</td>
					<td><?php echo h($serviceCharge['PaySchedule']['name']); ?>&nbsp;</td>
					
					<td><?php echo h($serviceCharge['ServiceCharge']['amount']); ?>&nbsp;</td>
					<td><?php echo h($serviceCharge['ServiceCharge']['due']); ?>&nbsp;</td>
					<td><?php echo h($serviceCharge['ServiceCharge']['remarks']); ?>&nbsp;</td>
					<td><?php echo h($serviceCharge['ServiceStatus']['name']); ?>&nbsp;</td>
					
			
					<td class="text-right action">
							<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $serviceCharge['ServiceCharge']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
							<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $serviceCharge['ServiceCharge']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
					</td>
					
				</tr>
			<?php endforeach; ?>
			</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev('< ' . __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next') . ' >', array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	