<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Product'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Products', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('ServiceCharge',array('class'=>'form')); 
	
		echo $this->Form->input('client_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('pay_schedule_id',array('options'=>$PaySchedule,'class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('amount',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('due',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('remarks',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('status',array('options'=>$service_statues,'class'=>'form-control','div'=>array('class'=>'form-group')));

		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>
