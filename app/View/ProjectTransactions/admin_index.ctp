<div class="row bar bar-primary bar-top">
	<div class="col-md-3">
		<h1 class="bar-title"><?php echo __('Projects Transaction for : '.$proData['Project']['name']); ?></h1>
	</div>
	<div class="col-md-9 text-right">
		<?php 
		echo $this->Form->input('client_id',array('empty'=>array(''=>'Select Client'),'div'=>false,'label'=>false,'required'=>false,'class'=>'search-box'));
		echo $this->Form->input('project_category_id',array('empty'=>array(''=>'Select Category'),'div'=>false,'label'=>false,'required'=>false,'class'=>'search-box'));
		echo $this->Form->button('Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-default btn-sm'));
		echo $this->Form->end(); 
		
		?>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-plus-sign\'></i> Add New Transaction', array('action' => 'add',$proData['Project']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-plus-sign\'></i> Project Lists', array('controller'=>'projects','action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>
	
	<div class="col-md-12">
	<b>
	Project Name : <?php echo $proData['Project']['name']; ?><br>
	Client Name : <?php echo $proData['Client']['company_name']; ?>
	</b>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped" >
				<thead>
				<tr class="info">
				
					<th><?php echo $this->Paginator->sort('date'); ?></th>
					<th><?php echo $this->Paginator->sort('transaction_no'); ?></th>
					<th><?php echo $this->Paginator->sort('amount'); ?></th>
					
				
					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php

				$total_amount = 0;
				
			
				
				if(isset($proData['ProjectTransaction']))
				{
					foreach ($proData['ProjectTransaction'] as $project): 
					
					
					?>
					<tr>
						<td><?php echo h($project['date']); ?>&nbsp;</td>
						<td><?php echo h($project['transaction_no']); ?>&nbsp;</td>
					
						<td><?php echo h($project['amount']);
	
								$total_amount+= $project['amount'];
						
						?>&nbsp;</td>
						<td class="text-right action">
							<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-edit\'></i> Edit', array('action' => 'edit', $project['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
							<?php echo $this->Form->postLink('<i class=\'glyphicon glyphicon-remove-circle\'></i> Delete', array('action' => 'delete', $project['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
						</td>
					</tr>
					
					<?php endforeach;

				}
				?>
				
				<tr>
					<td> </td>
					<td><b>Total Amount</b></td>
					<td><b><?php echo $total_amount;?></b></td>
					<td></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev('< ' . __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next') . ' >', array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	