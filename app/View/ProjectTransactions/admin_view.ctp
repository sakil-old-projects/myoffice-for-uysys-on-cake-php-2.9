<h1 class="page-title"><?php echo __('Project details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($project['Project']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($project['Project']['name']); ?></dd>

	<dt><?php echo __('Client'); ?></dt>
	<dd>\<?php echo $this->Html->link($project['Client']['name'], array('controller' => 'clients', 'action' => 'view', $project['Client']['id'])); ?><dd>

	<dt><?php echo __('Total Amount'); ?></dt>
	<dd><?php echo h($project['Project']['total_amount']); ?></dd>

	<dt><?php echo __('Collected Amount'); ?></dt>
	<dd><?php echo h($project['Project']['collected_amount']); ?></dd>

	<dt><?php echo __('Pending Amount'); ?></dt>
	<dd><?php echo h($project['Project']['pending_amount']); ?></dd>

	<dt><?php echo __('Start Date'); ?></dt>
	<dd><?php echo h($project['Project']['start_date']); ?></dd>

	<dt><?php echo __('End Date'); ?></dt>
	<dd><?php echo h($project['Project']['end_date']); ?></dd>

	<dt><?php echo __('Category'); ?></dt>
	<dd><?php echo h($project['Project']['category']); ?></dd>

	<dt><?php echo __('Project Status'); ?></dt>
	<dd><?php echo h($project['Project']['project_status']); ?></dd>

	<dt><?php echo __('Supporting Agreement'); ?></dt>
	<dd><?php echo h($project['Project']['supporting_agreement']); ?></dd>

	<dt><?php echo __('Support Duration'); ?></dt>
	<dd><?php echo h($project['Project']['support_duration']); ?></dd>

	<dt><?php echo __('Support Amount'); ?></dt>
	<dd><?php echo h($project['Project']['support_amount']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($project['Project']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($project['Project']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($project['Project']['modified']); ?></dd>

</dl>
