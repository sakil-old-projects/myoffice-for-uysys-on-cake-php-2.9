<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Edit Project Trans'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Projects Transaction', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('ProjectTransaction',array('class'=>'form')); 
	
		echo $this->Form->input('id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('date',array('type'=>'text','div'=>array('class'=>'form-group'),'class'=>'form-control input-datepicker-addon'));
		echo $this->Form->input('project_id',array('type'=>'hidden','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('transaction_no',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('amount',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));
		
	echo $this->Form->end(); 
?>	</div>
</div>

<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#ProjectStartDate,#ProjectEndDate').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: "HH:mm:ss"
	});
});

</script>
<?php $this->end(); ?>
