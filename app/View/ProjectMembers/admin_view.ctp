<h1 class="page-title"><?php echo __('Project Member details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($projectMember['ProjectMember']['id']); ?></dd>

	<dt><?php echo __('Project'); ?></dt>
	<dd>\<?php echo $this->Html->link($projectMember['Project']['name'], array('controller' => 'projects', 'action' => 'view', $projectMember['Project']['id'])); ?><dd>

	<dt><?php echo __('Employee'); ?></dt>
	<dd>\<?php echo $this->Html->link($projectMember['Employee']['name'], array('controller' => 'employees', 'action' => 'view', $projectMember['Employee']['id'])); ?><dd>

</dl>
