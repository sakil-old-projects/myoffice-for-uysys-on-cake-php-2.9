<h1 class="page-title"><?php echo __('Document details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($document['Document']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($document['Document']['name']); ?></dd>

	<dt><?php echo __('Attachments'); ?></dt>
	<dd><?php echo h($document['Document']['attachments']); ?></dd>

	<dt><?php echo __('Type'); ?></dt>
	<dd><?php echo h($document['Document']['type']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($document['Document']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($document['Document']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($document['Document']['modified']); ?></dd>

</dl>
