
<div class="box box-first document-search">
	<h3 class="box-title">Search Documents</h3>
	<div class="box-body">
		<?php echo $this->Form->create('Document',array('class'=>'form','data-role'=>'form')); 	?>
		<div class="col-md-4">
			<?php 
			echo $this->Form->input('client_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));	
			echo $this->Form->input('date_from',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));		
			
			?>
		</div>
		<div class="col-md-4">
			<?php 

			echo $this->Form->input('document_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));				
			echo $this->Form->input('date_to',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));	
			
			?>
		</div>
		
		<div class="col-md-4">
			<?php 

			echo $this->Form->input('document_type_id',array('empty'=>array(''=>'Select Type'),'class'=>'form-control form-select','div'=>array('class'=>'form-group')));	
			
			echo $this->Form->button('<i class="fa fa-search"></i> Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-common'));
			?>
		</div>
		
		<?php echo $this->Form->end(); ?>	</div>
</div>

<div class="box">
<h3 class="box-title"><?php echo __('Documents'); ?>
 <?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add Documents', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right')); ?></h3>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table" >
				<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('client_name'); ?></th>
					<th><?php echo $this->Paginator->sort('document_name'); ?></th>
					<th><?php echo $this->Paginator->sort('attachments'); ?></th>
					<th><?php echo $this->Paginator->sort('document_type_id'); ?></th>
					<th><?php echo $this->Paginator->sort('submission_date'); ?></th>
					<th><?php echo $this->Paginator->sort('submit_by'); ?></th>

					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($documents as $document): ?>
				<tr>
					<td><?php echo h($document['Document']['id']); ?>&nbsp;</td>
					<td><?php echo h($document['Document']['client_name']); ?>&nbsp;</td>
					<td><?php echo h($document['Document']['document_name']); ?>&nbsp;</td>
					<td>
					<?php 
					
						if(isset($document['DocumentFile']) && sizeof($document['DocumentFile'])>0 )
						{
							$documentsFiles = $document['DocumentFile'][0]['file'];
							
							echo $this->Html->link($documentsFiles,"/img/site/documents/{$documentsFiles}",array('target'=>'blank')); 
						}				
					
					?>
					&nbsp;</td>
					<td><?php echo h($document['DocumentType']['name']); ?>&nbsp;</td>
					<td><?php echo h(date('Y-m-d',strtotime($document['Document']['submission_date']))); ?>&nbsp;</td>
					<td><?php echo h($document['Document']['submit_by']); ?>&nbsp;</td>

					<td class="text-right action">
						<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $document['Document']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
						<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $document['Document']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev( __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next'), array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	



<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#DocumentDateFrom,#DocumentDateTo').datepicker({
		dateFormat: 'yy-mm-dd',
		
	});
});

</script>
<?php $this->end(); ?>

