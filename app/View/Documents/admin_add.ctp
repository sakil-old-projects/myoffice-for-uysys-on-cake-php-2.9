<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Document'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Documents', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Document',array('class'=>'form','type'=>'file')); 
	
		echo $this->Form->input('client_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));	
		echo $this->Form->input('document_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('document_type_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('multiple_files.', array('label'=>'Select Multiple File','type' => 'file', 'multiple'));		
		echo $this->Form->input('description',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('submission_date',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('submit_by',array('type'=>'text','class'=>'form-control','div'=>array('class'=>'form-group')));
				
		echo $this->Form->input('status',array('options'=>$status,'class'=>'form-control','div'=>array('class'=>'form-group')));

		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>



<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#DocumentSubmissionDate').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: "HH:mm:ss"
	});
});

</script>
<?php $this->end(); ?>
