<h1 class="page-title"><?php echo __('Inventory details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['id']); ?></dd>

	<dt><?php echo __('Title'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['title']); ?></dd>

	<dt><?php echo __('Description'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['description']); ?></dd>

	<dt><?php echo __('Mother Board'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['mother_board']); ?></dd>

	<dt><?php echo __('Ram'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['ram']); ?></dd>

	<dt><?php echo __('Hard Disk'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['hard_disk']); ?></dd>

	<dt><?php echo __('Pc Number'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['pc_number']); ?></dd>

	<dt><?php echo __('Type'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['type']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($inventory['Inventory']['status']); ?></dd>

</dl>
