<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Edit Inventory'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Inventories', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Inventory',array('class'=>'form')); 
	
		echo $this->Form->input('id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('employee_id',array('empty'=>array(''=>'Select Type'),'options'=>$employees,'class'=>'form-control','div'=>array('class'=>'form-group')));	
		echo $this->Form->input('pc_number',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('description',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('mother_board',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('processor',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('ram',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('hard_disk',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('monitor_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('total_monitor',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('mouse',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('keyboard',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('type',array('options'=>$types,'class'=>'form-control','div'=>array('class'=>'form-group')));
	
		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>
