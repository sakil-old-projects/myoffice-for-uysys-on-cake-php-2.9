
<div class="box box-first">
<h3 class="box-title"><?php echo __('Clients'); ?></h3>
	<div class="box-body">
	<div class="box-search">
		<div class="row">
<div class="col-md-9">
		<?php echo $this->Form->create('Client',array('class'=>'form-inline','data-role'=>'form')); ?>
		<?php echo $this->Form->input('keywords',array('type'=>'text','div'=>false,'label'=>false,'class'=>'form-control', 'placeholder'=>'Search key words'));?>
		<?php echo $this->Form->input('product_id',array('empty'=>array(''=>'Select Products'),'options'=>$productLists,'div'=>false,'label'=>false,'required'=>false,'class'=>'form-control form-select')); ?>
		<?php echo $this->Form->button('<i class="fa fa-search" aria-hidden="true"></i> Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-sm btn-search'));?>
		
		<?php echo $this->Form->end(); ?>
	</div>

	<div class="col-md-3 text-right">
		<?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add Clients', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common')); ?>
	</div>	
</div>
	</div>
	
		<div class="table-responsive">
			<table class="table">
			<thead>
			<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>							
							<th><?php echo $this->Paginator->sort('company_name'); ?></th>
							<th><?php echo $this->Paginator->sort('contact_person'); ?></th>
							<th><?php echo $this->Paginator->sort('phone'); ?></th>
							<th><?php echo $this->Paginator->sort('website'); ?></th>
							<th><?php echo $this->Paginator->sort('Product'); ?></th>
							<th><?php echo $this->Paginator->sort('Address'); ?></th>
							<th><?php echo $this->Paginator->sort('country_id'); ?></th>
						
							<th class="text-right action-th"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			
			<tbody>
			<?php 
			
			
			foreach ($clients as $client): ?>
	<tr>
		<td><?php echo h($client['Client']['id']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['company_name']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['contact_person']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['phone']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['website']); ?>&nbsp;</td>
		<td><?php echo h($client['Product']['name']); ?>&nbsp;</td>
		<td><?php echo h($client['Client']['office_address']); ?>&nbsp;</td>
		<td><?php echo h($client['Country']['name']); ?>&nbsp;</td>
		

		<td class="text-right action">
			<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $client['Client']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
			<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $client['Client']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
		</td>
	</tr>
<?php endforeach; ?>
			</tbody>
			</table>
		</div>
	</div>
</div>

<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev( __('prev'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next'), array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	