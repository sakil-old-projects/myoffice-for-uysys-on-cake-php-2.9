<h1 class="page-title"><?php echo __('Client details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($client['Client']['id']); ?></dd>

	<dt><?php echo __('CompanyName'); ?></dt>
	<dd><?php echo h($client['Client']['company_name']); ?></dd>
	
	<dt><?php echo __('ContactPerson'); ?></dt>
	<dd><?php echo h($client['Client']['contact_person']); ?></dd>
	
	<dt><?php echo __('Office Address'); ?></dt>
	<dd><?php echo h($client['Client']['office_address']); ?></dd>

	<dt><?php echo __('Description'); ?></dt>
	<dd><?php echo h($client['Client']['description']); ?></dd>

	<dt><?php echo __('Phone'); ?></dt>
	<dd><?php echo h($client['Client']['phone']); ?></dd>

	<dt><?php echo __('Email'); ?></dt>
	<dd><?php echo h($client['Client']['email']); ?></dd>

	<dt><?php echo __('Website'); ?></dt>
	<dd><?php echo h($client['Client']['website']); ?></dd>

	<dt><?php echo __('Client Type'); ?></dt>
	<dd><?php echo h($client['Client']['client_type']); ?></dd>

	<dt><?php echo __('Country'); ?></dt>
	<dd>\<?php echo $this->Html->link($client['Country']['name'], array('controller' => 'countries', 'action' => 'view', $client['Country']['id'])); ?><dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($client['Client']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($client['Client']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($client['Client']['modified']); ?></dd>

</dl>
