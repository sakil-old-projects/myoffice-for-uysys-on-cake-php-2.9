<div class="row">
	<div class="col-sm-12 col-md-8">
		<div class="box box-first">
<h3 class="box-title"><?php echo __('Admin Edit Client'); ?> <?php echo $this->Html->link('<i class=\'fa fa-list\'></i> List Clients', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right ')); ?></h3>

	<div class="box-body">
	<?php 
	echo $this->Form->create('Client',array('class'=>'form')); 
	
		
	   echo $this->Form->input('id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
        echo $this->Form->input('project_category_id',array('options'=>$projectCategory,'class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('product_id',array('empty'=>array(''=>'Select Products'),'class'=>'form-control','div'=>array('class'=>'form-group')));	
		echo $this->Form->input('company_name',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('contact_person',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('phone',array('class'=>'form-control','div'=>array('class'=>'form-group')));	
		echo $this->Form->input('office_address',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('description',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('email',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('website',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('client_type_id',array('options'=>$client_types,'class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('country_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		
		echo $this->Form->input('status',array('div'=>array('class'=>'checkbox')));
     
		echo $this->Form->button('<i class=\'fa fa-refresh\'></i> Reset',array('type'=>'reset', 'class'=>'btn btn-common','label'=>false,'div'=>false));
		echo $this->Form->button('<i class=\'fa fa-check\'></i> Submit',array('type'=>'submit','class'=>'btn btn-common','label'=>false,'div'=>false));


	echo $this->Form->end(); 
?>	</div>
</div>
	</div>
</div>
