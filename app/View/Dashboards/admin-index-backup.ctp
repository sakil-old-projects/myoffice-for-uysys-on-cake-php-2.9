
<div class="row">
	<div class="collection">
		
	<div class="col-xs-12 col-sm-12 col-md-4">
		
			<div class="media"> 
		<div class="media-left media-middle"><i class="fa fa-money"></i></div>
		 <div class="media-body media-middle"> 
		 <h4 class="media-heading">Expected Amount</h4> 
		 <p>BDT <?php echo number_format($projectSum[0]['total_value']); ?></p> 
		 </div> 
		 </div>
		 </div>
		 <div class="col-xs-12 col-sm-12 col-md-4">
		 	<div class="media"> 
		<div class="media-left media-middle"><i class="fa fa-check"></i></div>
		 <div class="media-body media-middle"> 
		 <h4 class="media-heading">Total Collection</h4> 
			 <p>BDT <?php echo number_format($projectSum[0]['total_collection']); ?></p> 
		 </div> 
		 </div>
		 </div>
		 
		 <div class="col-xs-12 col-sm-12 col-md-4">
		 	<div class="media"> 
		<div class="media-left media-middle"><i class="fa fa-calculator"></i></div>
		 <div class="media-body media-middle"> 
		 <h4 class="media-heading">Pending Amount</h4> 
			 <p>BDT <?php echo number_format($projectSum[0]['total_pending']); ?></p> 
		 </div> 
		 </div>
		 </div>
		 
	</div>
</div>
				



					
<!--  New Row Start -->					
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="box">
             <h3 class="box-title">Projects</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                  
                    <th>Client Name</th>
                    <th>Project Title</th>
                    <th>Title of Amount</th>
                    <th>Total Amount</th>
                    <th>Collected</th>
                    <th>Due</th>                  
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($projects)):
                  
                  		foreach($projects as $key=>$project):
                  		
                  			echo "<tr>";
                  			
                  				echo "<td>".h($project['Client']['company_name'])."</td>";
                  				echo "<td>".h($project['Project']['name'])."</td>";
                  				echo "<td>".h($project['Project']['amount_title'])."</td>";
                  				echo "<td>".h($project['Project']['total_amount'])."</td>";
                  				echo "<td>".h($project['Project']['collected_amount'])."</td>";
                  				echo "<td>".h($project['Project']['pending_amount'])."</td>";
                  				
                  				
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="projects/index" class="btn btn-view"><i class="fa fa-calendar"></i> View all Projects</a>
            </div>
          </div>

	 </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <div class="box">
             <h3 class="box-title">Service Charges</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Client</th>
                    <th>Pay Schedule</th>
                    <th>Amount</th>
                    <th>Due</th>
                    <th>Remarks</th>
                    <th>Status</th>                  
                  </tr>
                  </thead>
                  <tbody>
                 
					   <?php if(isset($service_charges)):
                  
                  		foreach($service_charges as $key=>$service_charge):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($service_charge['Client']['company_name'])."</td>";
                  				echo "<td>".h($service_charge['PaySchedule']['name'])."</td>";
                  				echo "<td>".h($service_charge['ServiceCharge']['amount'])."</td>";
                  				echo "<td>".h($service_charge['ServiceCharge']['due'])."</td>";
                  				echo "<td>".h($service_charge['ServiceCharge']['remarks'])."</td>";
                  				echo "<td>".h($service_charge['ServiceStatus']['name'])."</td>";
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;
	                  
	                  ?>

                
                  </tbody>
                </table>
              </div>
              <a href="service_charges/index" class="btn btn-view"><i class="fa fa-calendar"></i> View All</a>
            </div>
          </div>

		</div>
					
	</div>
	
<!-- End Row -->	

<!--  Start New Row -->
<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8">
						<div class="box">
             <h3 class="box-title">Meeting Schedule</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Meeting Date</th>
                    <th>Time</th>
                    <th>Location</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($meetingLists)):
                  
                  		foreach($meetingLists as $key=>$meeting):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($meeting['Meeting']['company'])."</td>";
                  				echo "<td>".date('Y-m-d',strtotime($meeting['Meeting']['date']))."</td>";
                  				echo "<td>".date('h:m:s',strtotime($meeting['Meeting']['date']))."</td>";
                  				echo "<td>".h($meeting['Meeting']['location'])."</td>";
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="meetings/index" class="btn btn-view"><i class="fa fa-calendar"></i> View full schedule</a>
            </div>
          </div>

					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="box">
             <h3 class="box-title">Current Employees</h3>
            <div class="box-body">
                <?php foreach($employeeLists as $key=>$employee):?>
              <div class="media"> 
              
					<div class="media-left media-middle">
					
						<?php 
							if($employee['Employee']['profile_img']==""){
								echo $this->Html->image("site/employees_profile/profile_deafult.png",array('width'=>'40px','class'=>'img-circle','alt'=>"Profile"));
																
							}
							else 
							{									
								echo $this->Html->image("site/employees_profile/".$employee['Employee']['profile_img'],array('width'=>'40px','class'=>'img-circle','alt'=>"Profile"));
								
							}		
						?>
					
					</div>
					
					 <div class="media-body media-middle"> 
					 	<h4 class="employee-name"><?php echo h($employee['Employee']['name']); ?></h4> 
					 	<p><?php echo h($employee['Designation']['name']); ?></p> 
					 </div> 
			 </div>
			 
			 <?php endforeach;?>
						 
						
              <a href="employees/index" class="btn btn-view"><i class="fa fa-user-circle"></i> Show all</a>
            </div>
          </div>

		</div>
	</div>
<!-- End Row -->			
				
<!--  New Row Start -->					
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="box">
             <h3 class="box-title">Documents Files(Proposal,Tender)</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Client</th>
                    <th>Document Name</th>
                    <th>File</th>
                    <th>Type</th>
                    <th>Submission Date</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($documents)):
                  
                  		foreach($documents as $key=>$document):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($document['Document']['client_name'])."</td>";
                  				echo "<td>".h($document['Document']['document_name'])."</td>";
                  				echo "<td>";
                  					if(isset($document['DocumentFile']) && sizeof($document['DocumentFile'])>0 )
									{
										$documentsFiles = $document['DocumentFile'][0]['file'];
							
										echo $this->Html->link($documentsFiles,"/img/site/documents/{$documentsFiles}",array('target'=>'blank')); 
									}	
                  				
                  				echo "</td>";
                  				//echo "<td>".$this->Html->link($document['Document']['attachments'],"/img/site/documents/{$document['Document']['attachments']}",array('target'=>'blank'))."</td>";
                  				echo "<td>".h($document['DocumentType']['name'])."</td>";
                  				echo "<td>".h(date('Y-m-d',strtotime($document['Document']['submission_date'])))."</td>";
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="documents/index" class="btn btn-view"><i class="fa fa-calendar"></i> View all Documents</a>
            </div>
          </div>

	 </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <div class="box">
             <h3 class="box-title">Office Certificate</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>File</th>
                    <th>Description</th>                 
                  </tr>
                  </thead>
                  <tbody>
                 
					   <?php if(isset($officeCertificates)):
                  
                  		foreach($officeCertificates as $key=>$certificate):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($certificate['OfficeCertificate']['name'])."</td>";
                  				echo "<td>".$this->Html->link($certificate['OfficeCertificate']['file_name'],"/img/site/office_certificates/{$certificate['OfficeCertificate']['file_name']}",array('target'=>'blank'))."</td>";
                  				echo "<td>".h($certificate['OfficeCertificate']['description'])."</td>";
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;
	                  
	                  ?>

                
                  </tbody>
                </table>
              </div>
              <a href="office_certificates/index" class="btn btn-view"><i class="fa fa-calendar"></i> View All Certificate</a>
            </div>
          </div>

		</div>
					
	</div>
	
<!-- End Row -->	
	
	

	
	<!--  New Row Start -->		
	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
						<div class="box">
             <h3 class="box-title">Our Clients</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Company Name</th>
                    <th>Contact Person</th>
                    <th>Phone</th>                  
                    <th>Product</th>
                    <th>Address</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($clients)):
                  
                  		foreach($clients as $key=>$client):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($client['Client']['company_name'])."</td>";
                  				echo "<td>".h($client['Client']['contact_person'])."</td>";
                  				echo "<td>".h($client['Client']['phone'])."</td>";                  				
                  				echo "<td>".h($client['Product']['name'])."</td>";
                  				echo "<td>".h($client['Client']['office_address'])."</td>";                  	
                  				
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="clients/index" class="btn btn-view"><i class="fa fa-calendar"></i> View all Clients</a>
            </div>
          </div>

	 </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <div class="box">
             <h3 class="box-title">Client Visits</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Visit By</th>
                    <th>Client</th>
                    <th>Address</th>
                    <th>Contact</th>
                    <th>Status</th>  
                    <th>Date</th>                 
                  </tr>
                  </thead>
                  <tbody>
                 
					   <?php if(isset($clientVisits)):
                  
                  		foreach($clientVisits as $key=>$clientVisit):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($clientVisit['Employee']['name'])."</td>";
                  				echo "<td>".h($clientVisit['ClientVisit']['client_name'])."</td>";
                  				echo "<td>".h($clientVisit['ClientVisit']['address'])."</td>";
                  				echo "<td>".h($clientVisit['ClientVisit']['contact'])."</td>";
                  				echo "<td>".h($clientVisit['ClientVisit']['status'])."</td>";
                  				echo "<td>".date('Y-m-d',strtotime($clientVisit['ClientVisit']['date']))."</td>";		
                  				
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;
	                  
	                  ?>

                
                  </tbody>
                </table>
              </div>
              <a href="client_visits/index" class="btn btn-view"><i class="fa fa-calendar"></i> View All</a>
            </div>
          </div>

		</div>
					
	</div>
<!--  End Row  -->		
	
<!--  New Row Start -->		
	<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6">
			<div class="box">
             <h3 class="box-title">Website References</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Company Name</th>
                    <th>Website URL</th>
                    <th>Complete Date</th>
                    <th>Status</th>
                  
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($our_website_references)):
                  
                  		foreach($our_website_references as $key=>$website_reference):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($website_reference['OurWebsiteReference']['company'])."</td>";
                  				echo "<td>"."<a href=".$website_reference['OurWebsiteReference']['website_url']."target='_blank'>".$website_reference['OurWebsiteReference']['website_url']."</a>"."</td>";
                  			    echo "<td>".h($website_reference['OurWebsiteReference']['complete_date'])."</td>";
                  			    echo "<td>".h($website_reference['OurWebsiteReference']['status'])."</td>";              				                  				
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="our_website_references/index" class="btn btn-view"><i class="fa fa-calendar"></i> View all References</a>
            </div>
          </div>

	 </div>
		
		<div class="col-xs-12 col-sm-12 col-md-6">
		 <div class="box">
             <h3 class="box-title">Our Products</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Product</th>
                    <th>Patform</th>
                    <th>Description</th>
                    <th>Demo Link</th>
                              
                  </tr>
                  </thead>
                  <tbody>
                 
					   <?php if(isset($products)):
                  
                  		foreach($products as $key=>$product):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($product['Product']['name'])."</td>";
                  				echo "<td>".h($product['Platform']['name'])."</td>";
                  				echo "<td>".h($product['Product']['description'])."</td>";
                  				echo "<td>".h($product['Product']['demo_link'])."</td>";                  				
                  				
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;
	                  
	                  ?>

                
                  </tbody>
                </table>
              </div>
              <a href="products/index" class="btn btn-view"><i class="fa fa-calendar"></i> View All Products</a>
            </div>
          </div>

		</div>
					
	</div>
<!--  End Row  -->			

	