<div class="row">

  <div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-aqua-active">

        <h3 class="widget-user-username">Collection or Payment</h3>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../dist/img/taka-128.png" alt="User Avatar">
        <!-- <i class="fa fa-user"></i> -->
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">130,200,000</h5>
              <span class="description-text">Total</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">13,000,000</h5>
              <span class="description-text">This weak</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">35,000,000</h5>
              <span class="description-text">This month</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">50,000,000</h5>
              <span class="description-text">This Year</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">Pending</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">200,000,000</h5>
              <span class="description-text">Expected</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">

  <div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-primary">

        <h3 class="widget-user-username">Service Charge</h3>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../dist/img/taka2-128.png" alt="User Avatar">
        <!-- <i class="fa fa-user"></i> -->
      </div>
      <div class="box-footer">
        <div class="row">
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">130,200,000</h5>
              <span class="description-text">Total</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">13,000,000</h5>
              <span class="description-text">This weak</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">35,000,000</h5>
              <span class="description-text">This month</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">50,000,000</h5>
              <span class="description-text">This Year</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">Pending</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">200,000,000</h5>
              <span class="description-text">Expected</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">

  <div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-aqua-active">

        <h3 class="widget-user-username">Clients</h3>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../dist/img/clients.png" alt="User Avatar">
        <!-- <i class="fa fa-user"></i> -->
      </div>
      <div class="box-footer">
        <div class="row">
            
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">500</h5>
              <span class="description-text">Total</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">12</h5>
              <span class="description-text">This month</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">150</h5>
              <span class="description-text">This year</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">130</h5>
              <span class="description-text">Live</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2 border-right">
            <div class="description-block">
              <h5 class="description-header">13</h5>
              <span class="description-text">Ongoing</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          <div class="col-sm-2">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">Upcoming</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">

  <div class="col-md-12">
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header bg-aqua-active">

        <h3 class="widget-user-username">Work Order</h3>
      </div>
      <div class="widget-user-image">
        <img class="img-circle" src="../dist/img/work-order.png" alt="User Avatar">
        <!-- <i class="fa fa-user"></i> -->
      </div>
      <div class="box-footer">
        <div class="row">
            
          
          <div class="col-sm-3">
            <div class="description-block">
              <h5 class="description-header">500</h5>
              <span class="description-text">Total</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-3">
            <div class="description-block">
              <h5 class="description-header">12</h5>
              <span class="description-text">This Weak</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-3">
            <div class="description-block">
              <h5 class="description-header">12</h5>
              <span class="description-text">This month</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->
          
          <div class="col-sm-3">
            <div class="description-block">
              <h5 class="description-header">12</h5>
              <span class="description-text">This Year</span>
            </div>
            <!-- /.description-block -->
          </div>
          <!-- /.col -->

        </div>
        <!-- /.row -->
      </div>
    </div>
    <!-- /.widget-user -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <div class="collection">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="media">
                <div class="media-left media-middle">
                <i class="fa fa-money"></i>
                <span class="text-uppercase">Collection</span>
                </div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Expected Amount</h4>
                    <p>BDT <?php echo number_format($projectSum[0]['total_value']); ?></p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="media">
                <div class="media-left media-middle"><i class="fa fa-check"></i></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Total Collection</h4>
                    <p>BDT <?php echo number_format($projectSum[0]['total_collection']); ?></p>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="media">
                <div class="media-left media-middle"><i class="fa fa-calculator"></i></div>
                <div class="media-body media-middle">
                    <h4 class="media-heading">Pending Amount</h4>
                    <p>BDT <?php echo number_format($projectSum[0]['total_pending']); ?></p>
                </div>
            </div>
        </div>

    </div>
</div>