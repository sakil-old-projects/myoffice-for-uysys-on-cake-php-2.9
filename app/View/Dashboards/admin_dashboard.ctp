
<!--  Start New Row -->
<div class="row">
					<div class="col-xs-12 col-sm-8 col-md-8">
						<div class="box">
             <h3 class="box-title">Meeting Schedule</h3>
            <div class="box-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Meeting Date</th>
                    <th>Time</th>
                    <th>Location</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php if(isset($meetingLists)):
                  
                  		foreach($meetingLists as $key=>$meeting):
                  		
                  			echo "<tr>";
                  				echo "<td>".h($meeting['Meeting']['company'])."</td>";
                  				echo "<td>".date('Y-m-d',strtotime($meeting['Meeting']['date']))."</td>";
                  				echo "<td>".date('h:m:s',strtotime($meeting['Meeting']['date']))."</td>";
                  				echo "<td>".h($meeting['Meeting']['location'])."</td>";
                  			echo "</tr>";
                  		
	                    endforeach;
	                  endif;?>
	                  
                  
                  </tbody>
                </table>
              </div>
              <a href="meetings/index" class="btn btn-view"><i class="fa fa-calendar"></i> View full schedule</a>
            </div>
          </div>

					</div>
			
	</div>
<!-- End Row -->	
	