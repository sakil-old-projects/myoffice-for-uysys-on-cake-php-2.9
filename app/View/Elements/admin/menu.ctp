
<?php 
	function check_menu_active($current_location,$options){
		$condition = false;
		if((in_array($current_location['controller'],$options['controllers'],true)  && in_array($current_location['plugin'],$options['plugins'],'true'))== true){
			$condition = true;
		}
		if($condition == true){
			echo 'in';
		}
	}
	
	if($this->request['plugin'] == ''){
		$plugin = 'default';
	}else{
		$plugin = $this->request['plugin'];
	}
	
	$current_location = array('plugin'=>$plugin,'controller'=>$this->request['controller']);
	
	App::import('Model', array('AclController'));
	$this->AclController = new AclController();
	$aclControllers= $this->AclController->find('all');
	$menuAccessList=json_decode($auth_user['Role']['menu_access_list'],true);
	$parents=array();
	$childs=array();
	if(count($menuAccessList)>0):
		$parents=array_keys($menuAccessList);
		foreach ($menuAccessList as $key=>$ele):
			if(count($ele)>0):
				foreach ($ele as $ke=>$element):
					$childs[$element] = $element;
				endforeach;
			endif;
		endforeach;
	endif;
	//check_menu_active(array('plugin'=>'default','controller'=>'menus'),array('plugins'=>array('default'),'controllers'=>array('menus')));
	
?>

<div class="col-sm-3 col-md-2 sidebar">
	
		<?php echo $this->Html->link('<i class="fa fa-dashboard"></i> Dashboard',['controller'=>'dashboards','action'=>'index','admin'=>true,'plugin'=>false],['escape'=>false,'class'=>'dashboard-link']); ?>
	<div class="panel-group" id="accordion-menu">
		<?php 
		foreach ($aclControllers as $element):		
			if(in_array($element['AclController']['id'],$parents)):
			$title=$element['AclController']['title'];

		?>	
		<!-- Web Manager -->
			<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">
					<?php echo $this->Html->link(Inflector::humanize($title).'<i class="fa fa-angle-down pull-right"></i>',"#$title",['escape'=>false,'data-toggle'=>"collapse" ,'data-parent'=>"#accordion-menu"]);?>
				</h4>
			</div>
			<div id="<?php echo $title;?>" class="panel-collapse collapse <?php check_menu_active($current_location,array('plugins'=>json_decode($element['AclController']['plugins']),'controllers'=>json_decode($element['AclController']['controllers'])));?>">
				<div class="panel-body panel-body-custom">
					<ul class="left-menu">
					<?php 					
					foreach ($element['AclAction'] as $elem):
						if(in_array($elem['id'],$childs)):
						if($elem['plugins']!='false'){$plugin=$elem['plugins'];}else{$plugin='';}						
						?>
						<li>
							<?php echo $this->Html->link(Inflector::humanize($elem['title']),['controller'=>$elem['controllers'],'action'=>$elem['actions'],$elem['params'],'admin'=>true,'plugin'=>$plugin],['escape'=>false]);?>
						</li>
					<?php 
					 endif;
					endforeach;
					?>
					</ul>
				</div>
			</div>
		</div>

			
<?php 
	endif;
endforeach;
?>	
</div>
</div>
