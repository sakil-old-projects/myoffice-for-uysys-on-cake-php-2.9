<nav class="navbar navbar-default navbar-fixed-top navbar-site">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?php echo $this->Html->image('/admin/logo.svg',array('class'=>'img-responsive admin-logo'))?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php if(!empty($auth_status)):?>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php 
								
								$img_file = WWW_ROOT."img".DS."site".DS."avatars".DS.$auth_user['id'].".png";
								if(file_exists($img_file)):
									echo $this->Html->image("site/avatars/{$auth_user['id']}.png",array('class'=>'img-responsive pull-left avatar'));
								endif;
							?>	
							<?php 
								$user_details = json_decode($auth_user['personal_details'],true);
								echo "  ". $user_details['first_name']." ".$user_details['last_name'];
							?>
							 <i class="fa fa-user-circle"></i></a>

          <ul class="dropdown-menu" >
						<li>
							<?php echo $this->Html->link('Porfile',array('controller'=>'users','action'=>'edit',$auth_user['id'],'admin'=>true,'plugin'=>false))?>
						</li>
							
						<li>
							<?php echo $this->Html->link('Change Password',array('controller'=>'users','action'=>'change_password','admin'=>true,'plugin'=>false))?>
						</li>
						<li class="divider"></li>
						
						<li>
							<?php 
								if($auth_user['role_id'] == "54b0fd85-d824-4467-8cbe-18d0cdd1d5ac"):
									echo $this->Html->link('Sign Out',array('controller'=>'merchant_apis','action'=>'signout','admin'=>true,'plugin'=>'merchant'));
								else:
									echo $this->Html->link('Sign Out',array('controller'=>'users','action'=>'signout','admin'=>true,'plugin'=>false));
								endif;
								
								
								?>
						</li>
					</ul>
        </li>
      </ul>
      <?php endif;?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

