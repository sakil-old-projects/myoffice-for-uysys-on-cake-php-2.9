<div class="box box-first document-search">
	<h3 class="box-title">Search Employee</h3>
	<div class="box-body">
		<?php echo $this->Form->create('Employee',array('class'=>'form','data-role'=>'form')); 	?>
		<div class="col-md-4">
			<?php 
			echo $this->Form->input('keywords',array('class'=>'form-control','div'=>array('class'=>'form-group')));		
			
			?>
		</div>
		
		
		<div class="col-md-4">
			<?php 			
			echo $this->Form->button('<i class="fa fa-search"></i> Search',array('type'=>'submit','div'=>false,'label'=>false, 'class' =>'btn btn-common'));
			?>
		</div>
		
		<?php echo $this->Form->end(); ?>	</div>
</div>


<div class="box">
<h3 class="box-title"><?php echo __('Employee Lists'); ?>
 <?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add New Employee', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right')); ?></h3>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table" >
				<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>						
					<th><?php echo $this->Paginator->sort('designation'); ?></th>
					<th><?php echo $this->Paginator->sort('join_date'); ?></th>
					<th><?php echo $this->Paginator->sort('profile'); ?></th>
					<th><?php echo $this->Paginator->sort('phone'); ?></th>				
					<th><?php echo $this->Paginator->sort('cv'); ?></th>
					<th><?php echo $this->Paginator->sort('status'); ?></th>
					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
			</tr>
			</thead>
			
			<tbody>
			<?php foreach ($employees as $employee): ?>
				<tr>
					<td><?php echo h($employee['Employee']['id']); ?>&nbsp;</td>
					<td><?php echo h($employee['Employee']['name']); ?>&nbsp;</td>		
					<td><?php echo h($employee['Designation']['name']); ?>&nbsp;</td>
					<td><?php echo h($employee['Employee']['join_date']); ?>&nbsp;</td>
					<td>
					
					<?php 
					if($employee['Employee']['profile_img']==""){
						echo $this->Html->image("site/employees_profile/profile_deafult.png",array('width'=>'120px'));
														
					}
					else 
					{
							
						echo $this->Html->image("site/employees_profile/".$employee['Employee']['profile_img'],array('width'=>'130px','height'=>'130px','class'=>'img-circle'));
						
					}
					
					?>
					
					&nbsp;</td>
					<td><?php echo h($employee['Employee']['phone']); ?>&nbsp;</td>
			
					<td><?php echo $this->Html->link($employee['Employee']['cv_attachment'],"/img/site/employees_cv/{$employee['Employee']['cv_attachment']}",array('target'=>'blank')); ?>&nbsp;</td>
						
					<td><?php echo h($employee['Employee']['status']); ?>&nbsp;</td>
					
					<td class="text-right action">
									<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $employee['Employee']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
									<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $employee['Employee']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
					</td>
					
				</tr>
			<?php endforeach; ?>
			</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev( __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next'), array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	
