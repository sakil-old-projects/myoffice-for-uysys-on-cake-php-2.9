<h1 class="page-title"><?php echo __('Employee details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($employee['Employee']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($employee['Employee']['name']); ?></dd>

	<dt><?php echo __('Designation'); ?></dt>
	<dd><?php echo h($employee['Employee']['designation']); ?></dd>
	
	<dt><?php echo __('Department'); ?></dt>
	<dd><?php echo h($employee['Department']['name']); ?></dd>

	<dt><?php echo __('Join Date'); ?></dt>
	<dd><?php echo h($employee['Employee']['join_date']); ?></dd>

	<dt><?php echo __('Pic'); ?></dt>
	<dd><?php echo h($employee['Employee']['pic']); ?></dd>

	<dt><?php echo __('Resignation Date'); ?></dt>
	<dd><?php echo h($employee['Employee']['resignation_date']); ?></dd>

	<dt><?php echo __('Phone'); ?></dt>
	<dd><?php echo h($employee['Employee']['phone']); ?></dd>

	<dt><?php echo __('Email'); ?></dt>
	<dd><?php echo h($employee['Employee']['email']); ?></dd>

	<dt><?php echo __('Skype'); ?></dt>
	<dd><?php echo h($employee['Employee']['skype']); ?></dd>

	<dt><?php echo __('Cv'); ?></dt>
	<dd><?php echo h($employee['Employee']['cv']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($employee['Employee']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($employee['Employee']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($employee['Employee']['modified']); ?></dd>

</dl>
