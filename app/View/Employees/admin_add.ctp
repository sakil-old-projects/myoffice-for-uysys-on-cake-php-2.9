<div class="row bar bar-primary bar-top">
	<div class="col-md-12">
		<h1 class="bar-title"><?php echo __('Admin Add Employee'); ?></h1>
	</div>
</div>

<div class="row bar bar-secondary">
	<div class="col-md-12">
		<?php echo $this->Html->link('<i class=\'glyphicon glyphicon-list\'></i> List Employees', array('action' => 'index','admin'=>true),array('escape'=>false,'class'=>'btn btn-success')); ?>
	</div>	
</div>

<div class="row bar bar-third">
	<div class="col-md-12">
	<?php 
	echo $this->Form->create('Employee',array('class'=>'form','type'=>'file')); 
	
		echo $this->Form->input('name',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('department_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('designation_id',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('join_date',array('id'=>'EmployeeJoinDate','type'=>'text','class'=>'form-control input-datepicker-addon','div'=>array('class'=>'form-group')));
		echo $this->Form->input('profile',array('type'=>'file','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('resignation_date',array('id'=>'EmployeeResignationDate','type'=>'text','class'=>'form-control input-datepicker-addon','div'=>array('class'=>'form-group')));
		echo $this->Form->input('phone',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('email',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('skype',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('cv',array('type'=>'file','class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('salary',array('class'=>'form-control','div'=>array('class'=>'form-group')));
		echo $this->Form->input('status',array('options'=>$status,'class'=>'form-control','div'=>array('class'=>'form-group')));

		echo $this->Form->button('Reset',array('type'=>'reset', 'class'=>'btn btn-warning','label'=>false,'div'=>false));
		echo $this->Form->button('Submit',array('type'=>'submit','class'=>'btn btn-success btn-left-margin','label'=>false,'div'=>false));

	echo $this->Form->end(); 
?>	</div>
</div>


<?php  $this->start('script'); ?>
<?php 
 echo $this->Html->css('/jquery-ui/jquery-ui-timepicker-addon');
 echo $this->Html->script('/jquery-ui/jquery-ui-timepicker-addon');
?>
<script>
//http://trentrichardson.com/examples/timepicker/
$(document).ready(function(e) {
	$('#EmployeeJoinDate,#EmployeeResignationDate').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: "HH:mm:ss"
	});
});

</script>
<?php $this->end(); ?>
