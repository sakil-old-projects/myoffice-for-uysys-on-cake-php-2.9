
<div class="box box-first document-search">
	<h3 class="box-title">Search Certificates</h3>
	<div class="box-body">
		<?php echo $this->Form->create('OfficeCertificate',array('class'=>'form','data-role'=>'form')); 	?>
		<div class="col-md-4">
			<?php 
			echo $this->Form->input('keywords',array('class'=>'form-control','div'=>array('class'=>'form-group')));			
			
			?>
		</div>
	
		
		<?php echo $this->Form->end(); ?>	</div>
</div>




<div class="box">
<h3 class="box-title"><?php echo __('Office Certificates'); ?>
 <?php echo $this->Html->link('<i class=\'fa fa-plus\'></i> Add New Certicate', array('action' => 'add','admin'=>true),array('escape'=>false,'class'=>'btn btn-common pull-right')); ?></h3>
	<div class="box-body">
		<div class="table-responsive">
			<table class="table" >
				<thead>
				<tr>
					<th><?php echo $this->Paginator->sort('id'); ?></th>
					<th><?php echo $this->Paginator->sort('name'); ?></th>
					<th><?php echo $this->Paginator->sort('file_name'); ?></th>
					<th><?php echo $this->Paginator->sort('description'); ?></th>	
					<th><?php echo $this->Paginator->sort('created'); ?></th>
					<th><?php echo $this->Paginator->sort('modified'); ?></th>
					<th class="text-right action-th"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			
			<tbody>
			<?php foreach ($officeCertificates as $officeCertificate): ?>
				<tr>
					<td><?php echo h($officeCertificate['OfficeCertificate']['id']); ?>&nbsp;</td>
					<td><?php echo h($officeCertificate['OfficeCertificate']['name']); ?>&nbsp;</td>
					<td><?php echo $this->Html->link($officeCertificate['OfficeCertificate']['file_name'],"/img/site/office_certificates/{$officeCertificate['OfficeCertificate']['file_name']}",array('target'=>'blank')); ?>&nbsp;</td>
							
					<td><?php echo h($officeCertificate['OfficeCertificate']['description']); ?>&nbsp;</td>
					<td><?php echo h($officeCertificate['OfficeCertificate']['created']); ?>&nbsp;</td>
					<td><?php echo h($officeCertificate['OfficeCertificate']['modified']); ?>&nbsp;</td>
				
					
					<td class="text-right action">
						<?php echo $this->Html->link('<i class=\'fa fa-pencil-square-o\'></i>', array('action' => 'edit', $officeCertificate['OfficeCertificate']['id'],'admin'=>true),array('escape'=>false,'class'=>'btn btn-warning')); ?>
						<?php echo $this->Form->postLink('<i class=\'fa fa-times\'></i>', array('action' => 'delete', $officeCertificate['OfficeCertificate']['id'],'admin'=>true), array('escape'=>false,'class'=>'btn btn-danger'), __('Are you sure you want to delete?')); ?>
					</td>
				</tr>
			<?php endforeach; ?>
			
			</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pagination-block">
			<p>
			<?php
			echo $this->Paginator->counter(array(
			'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
			?>			</p>
			<div class="pagination">
			<?php
		echo $this->Paginator->prev('< ' . __('previous'),array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
		echo $this->Paginator->numbers(array('separator' => '','tag'=>'li','currentTag'=>'a', 'currentClass'=>'current disabled'));
		echo $this->Paginator->next(__('next') . ' >', array('tag'=>'li','disabledTag'=>'a'), null, array('class' => 'prev disabled','tag'=>'li','disabledTag'=>'a'));
	?>
			</div>
		</div>	
	</div>
</div>	