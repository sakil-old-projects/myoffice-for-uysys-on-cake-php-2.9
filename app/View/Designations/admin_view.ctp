<h1 class="page-title"><?php echo __('Designation details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($designation['Designation']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($designation['Designation']['name']); ?></dd>

</dl>
