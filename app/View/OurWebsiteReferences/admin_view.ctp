<h1 class="page-title"><?php echo __('Office Certificate details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['name']); ?></dd>

	<dt><?php echo __('File Name'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['file_name']); ?></dd>

	<dt><?php echo __('Description'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['description']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($officeCertificate['OfficeCertificate']['modified']); ?></dd>

</dl>
