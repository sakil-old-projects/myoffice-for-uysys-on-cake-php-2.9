<h1 class="page-title"><?php echo __('Project Category details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($projectCategory['ProjectCategory']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($projectCategory['ProjectCategory']['name']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($projectCategory['ProjectCategory']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($projectCategory['ProjectCategory']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($projectCategory['ProjectCategory']['modified']); ?></dd>

</dl>
