<h1 class="page-title"><?php echo __('Platform details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($platform['Platform']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($platform['Platform']['name']); ?></dd>

</dl>
