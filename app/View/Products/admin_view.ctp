<h1 class="page-title"><?php echo __('Product details'); ?></h1>
<dl>
	<dt><?php echo __('Id'); ?></dt>
	<dd><?php echo h($product['Product']['id']); ?></dd>

	<dt><?php echo __('Name'); ?></dt>
	<dd><?php echo h($product['Product']['name']); ?></dd>

	<dt><?php echo __('Platform'); ?></dt>
	<dd>\<?php echo $this->Html->link($product['Platform']['name'], array('controller' => 'platforms', 'action' => 'view', $product['Platform']['id'])); ?><dd>

	<dt><?php echo __('Description'); ?></dt>
	<dd><?php echo h($product['Product']['description']); ?></dd>

	<dt><?php echo __('Feature'); ?></dt>
	<dd><?php echo h($product['Product']['feature']); ?></dd>

	<dt><?php echo __(' Module'); ?></dt>
	<dd><?php echo h($product['Product'][' module']); ?></dd>

	<dt><?php echo __('Screenshot'); ?></dt>
	<dd><?php echo h($product['Product']['screenshot']); ?></dd>

	<dt><?php echo __('Status'); ?></dt>
	<dd><?php echo h($product['Product']['status']); ?></dd>

	<dt><?php echo __('Created'); ?></dt>
	<dd><?php echo h($product['Product']['created']); ?></dd>

	<dt><?php echo __('Modified'); ?></dt>
	<dd><?php echo h($product['Product']['modified']); ?></dd>

</dl>
