<?php
App::uses('AppController', 'Controller');
/**
 * TeamTypes Controller
 *
 * @property TeamType $TeamType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TeamTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamType->recursive = 0;
		$this->set('teamTypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TeamType->exists($id)) {
			throw new NotFoundException(__('Invalid team type'));
		}
		$options = array('conditions' => array('TeamType.' . $this->TeamType->primaryKey => $id));
		$this->set('teamType', $this->TeamType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamType->create();
			if ($this->TeamType->save($this->request->data)) {
				$this->Session->setFlash('The team type has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The team type could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TeamType->exists($id)) {
			throw new NotFoundException(__('Invalid team type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TeamType->save($this->request->data)) {
				$this->Session->setFlash('The team type has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The team type could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('TeamType.' . $this->TeamType->primaryKey => $id));
			$this->request->data = $this->TeamType->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TeamType->id = $id;
		if (!$this->TeamType->exists()) {
			throw new NotFoundException(__('Invalid team type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TeamType->delete()) {
			$this->Session->setFlash('The team type has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The team type could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
