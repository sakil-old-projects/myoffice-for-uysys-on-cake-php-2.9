<?php
App::uses('AppController', 'Controller');
/**
 * Meetings Controller
 *
 * @property Meeting $Meeting
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class MeetingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	
	public $uses = array('Meeting','MeetingFeedbackFile');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Meeting->recursive = 0;
		
		
		if (isset($this->request->data['Meeting'])) {
			
			$data = $this->request->data['Meeting'];
			$cond = array();
			if(!empty($data['product_id'])){
				$cond['Meeting.product_id'] = $data['product_id'];
			}
			
			$this->paginate = array('conditions'=>$cond,'order'=>'Meeting.company_name ASC');	
		
		}
		
	
		$this->paginate = array('order'=>'Meeting.date DESC','limit'=>100);	
				
		$this->set('Meetings', $this->Paginator->paginate());
		
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Meeting->exists($id)) {
			throw new NotFoundException(__('Invalid Meeting'));
		}
		$options = array('conditions' => array('Meeting.' . $this->Meeting->primaryKey => $id));
		$this->set('Meeting', $this->Meeting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
									
			$data = $this->request->data;	
			$multiple_files = $data['Meeting']['multiple_files'];
			unset($data['Meeting']['multiple_files']);
			
			
			$this->Meeting->create();
			if ($this->Meeting->save($this->request->data)) {
				
				$id = $this->Meeting->getInsertId();			
				
				
				//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{
					
					foreach($multiple_files as $fileData)
					{						
						
						$data['MeetingFeedbackFile']['meeting_id'] =  $id;
						
						$data['MeetingFeedbackFile']['file'] = $fileData['name'];
					
						$data['MeetingFeedbackFile']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['MeetingFeedbackFile']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['MeetingFeedbackFile']['file'] = $id.".".$data['MeetingFeedbackFile']['file'];				
					
						$this->Uploader->upload($fileData, $id.".".$data['MeetingFeedbackFile']['filename'], $data['MeetingFeedbackFile']['ext'], 'meeting_feedback_files',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->MeetingFeedbackFile->create();
						if ($this->MeetingFeedbackFile->save($data)) {
							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				
				$this->Session->setFlash('The Meeting has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The Meeting could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	
		
	
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Meeting->exists($id)) {
			throw new NotFoundException(__('Invalid Meeting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
							
			$data = $this->request->data;		
			$multiple_files = $data['Meeting']['multiple_files'];
			unset($data['Meeting']['multiple_files']);
			
			
			if ($this->Meeting->save($this->request->data)) {
				
				$id = $data['Meeting']['id'];
								
				//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{
					
					foreach($multiple_files as $fileData)
					{						
						
						$data['MeetingFeedbackFile']['meeting_id'] =  $id;
						
						$data['MeetingFeedbackFile']['file'] = $fileData['name'];
					
						$data['MeetingFeedbackFile']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['MeetingFeedbackFile']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['MeetingFeedbackFile']['file'] = $id.".".$data['MeetingFeedbackFile']['file'];				
					
						$this->Uploader->upload($fileData, $id.".".$data['MeetingFeedbackFile']['filename'], $data['MeetingFeedbackFile']['ext'], 'meeting_feedback_files',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->MeetingFeedbackFile->create();
						if ($this->MeetingFeedbackFile->save($data)) {
							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				
				
				$this->Session->setFlash('The Meeting has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The Meeting could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Meeting.' . $this->Meeting->primaryKey => $id));
			$this->request->data = $this->Meeting->find('first', $options);
		}
		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Meeting->id = $id;
		if (!$this->Meeting->exists()) {
			throw new NotFoundException(__('Invalid Meeting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Meeting->delete()) {
			$this->Session->setFlash('The Meeting has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The Meeting could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
