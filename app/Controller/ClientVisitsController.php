<?php
App::uses('AppController', 'Controller');
/**
 * ClientVisits Controller
 *
 * @property ClientVisit $ClientVisit
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ClientVisitsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ClientVisit->recursive = 0;
		
		
		if (isset($this->request->data['ClientVisit'])) {
			
			$data = $this->request->data['ClientVisit'];
			$cond = array();
			if(!empty($data['product_id'])){
				$cond['ClientVisit.product_id'] = $data['product_id'];
			}
			
			$this->paginate = array('conditions'=>$cond,'order'=>'ClientVisit.company_name ASC');	
		
		}
		
	
		
				
		$this->set('clientVisits', $this->Paginator->paginate());
		
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ClientVisit->exists($id)) {
			throw new NotFoundException(__('Invalid ClientVisit'));
		}
		$options = array('conditions' => array('ClientVisit.' . $this->ClientVisit->primaryKey => $id));
		$this->set('ClientVisit', $this->ClientVisit->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ClientVisit->create();
			if ($this->ClientVisit->save($this->request->data)) {
				$this->Session->setFlash('The ClientVisit has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The ClientVisit could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	
		
		$employees = $this->ClientVisit->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ClientVisit->exists($id)) {
			throw new NotFoundException(__('Invalid ClientVisit'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ClientVisit->save($this->request->data)) {
				$this->Session->setFlash('The ClientVisit has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The ClientVisit could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ClientVisit.' . $this->ClientVisit->primaryKey => $id));
			$this->request->data = $this->ClientVisit->find('first', $options);
		}
		$employees = $this->ClientVisit->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ClientVisit->id = $id;
		if (!$this->ClientVisit->exists()) {
			throw new NotFoundException(__('Invalid ClientVisit'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ClientVisit->delete()) {
			$this->Session->setFlash('The ClientVisit has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The ClientVisit could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
