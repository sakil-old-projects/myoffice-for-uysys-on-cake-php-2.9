<?php
App::uses('AppController', 'Controller');
/**
 * TaskManagements Controller
 *
 * @property TaskManagement $TaskManagement
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TaskManagementsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TaskManagement->recursive = 0;
		$this->set('taskManagements', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TaskManagement->exists($id)) {
			throw new NotFoundException(__('Invalid task management'));
		}
		$options = array('conditions' => array('TaskManagement.' . $this->TaskManagement->primaryKey => $id));
		$this->set('taskManagement', $this->TaskManagement->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TaskManagement->create();
			if ($this->TaskManagement->save($this->request->data)) {
				$this->Session->setFlash('The task management has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The task management could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$employees = $this->TaskManagement->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TaskManagement->exists($id)) {
			throw new NotFoundException(__('Invalid task management'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TaskManagement->save($this->request->data)) {
				$this->Session->setFlash('The task management has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The task management could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('TaskManagement.' . $this->TaskManagement->primaryKey => $id));
			$this->request->data = $this->TaskManagement->find('first', $options);
		}
		$employees = $this->TaskManagement->Employee->find('list');
		$this->set(compact('employees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TaskManagement->id = $id;
		if (!$this->TaskManagement->exists()) {
			throw new NotFoundException(__('Invalid task management'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TaskManagement->delete()) {
			$this->Session->setFlash('The task management has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The task management could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
