<?php
App::uses('AppController', 'Controller');

class DashboardsController extends AppController
{

    public $uses = array('User', 'Employee', 'Meeting', 'Project', 'ServiceCharge', 'ClientVisit', 'Document', 'OurWebsiteReference', 'Client', 'ClientVisit', 'Product', 'OfficeCertificate');

    public function admin_index()
    {
        //users
        $users = $this->User->find('count');

        $data['projectSum'] = $this->Project->find('first', array(
            'fields' => array('sum(Project.total_amount) as total_value', 'sum(Project.collected_amount) as  total_collection', 'sum(Project.pending_amount) as total_pending'),
        )
        );
        // echo "<pre>";
        // print_r($projectSum);
        // die();

        $employeeLists = $this->Employee->find('all', array('limit' => 5));
        $meetingLists = $this->Meeting->find('all', array('limit' => 5, 'order' => 'date DESC'));

        $projects = $this->Project->find('all', array('limit' => 6));
        $service_charges = $this->ServiceCharge->find('all', array('limit' => 8));

        $documents = $this->Document->find('all', array('limit' => 3));
        $officeCertificates = $this->OfficeCertificate->find('all', array('limit' => 5));

        $clients = $this->Client->find('all', array('limit' => 5, 'order' => 'Client.id ASC'));
        $clientVisits = $this->ClientVisit->find('all', array('limit' => 5));

        $products = $this->Product->find('all', array('limit' => 4));

        $our_website_references = $this->OurWebsiteReference->find('all', array('limit' => 8));

        // $this->set(compact('projectSum','employeeLists','meetingLists','projects','service_charges','documents','officeCertificates','clients','clientVisits','our_website_references','products'));
        $this->set($data);

    }

    public function admin_dashboard()
    {

        $projectSum = $this->Project->find('first', array(
            'fields' => array('sum(Project.total_amount) as total_value', 'sum(Project.collected_amount) as  total_collection', 'sum(Project.pending_amount) as total_pending'),
        )
        );

        $employeeLists = $this->Employee->find('all', array('limit' => 5));
        $meetingLists = $this->Meeting->find('all', array('limit' => 5, 'order' => 'date DESC'));

        $projects = $this->Project->find('all', array('limit' => 6));
        $service_charges = $this->ServiceCharge->find('all', array('limit' => 8));

        $documents = $this->Document->find('all', array('limit' => 3));
        $officeCertificates = $this->OfficeCertificate->find('all', array('limit' => 5));

        $clients = $this->Client->find('all', array('limit' => 5, 'order' => 'Client.id ASC'));
        $clientVisits = $this->ClientVisit->find('all', array('limit' => 5));

        $products = $this->Product->find('all', array('limit' => 4));

        $our_website_references = $this->OurWebsiteReference->find('all', array('limit' => 8));

        $this->set(compact('projectSum', 'employeeLists', 'meetingLists', 'projects', 'service_charges', 'documents', 'officeCertificates', 'clients', 'clientVisits', 'our_website_references', 'products'));

    }

}
