<?php
App::uses('AppController', 'Controller');
/**
 * ProjectMembers Controller
 *
 * @property ProjectMember $ProjectMember
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjectMembersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProjectMember->recursive = 0;
		$this->set('projectMembers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProjectMember->exists($id)) {
			throw new NotFoundException(__('Invalid project member'));
		}
		$options = array('conditions' => array('ProjectMember.' . $this->ProjectMember->primaryKey => $id));
		$this->set('projectMember', $this->ProjectMember->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProjectMember->create();
			if ($this->ProjectMember->save($this->request->data)) {
				$this->Session->setFlash('The project member has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project member could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$projects = $this->ProjectMember->Project->find('list');
		$employees = $this->ProjectMember->Employee->find('list');
		$this->set(compact('projects', 'employees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProjectMember->exists($id)) {
			throw new NotFoundException(__('Invalid project member'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectMember->save($this->request->data)) {
				$this->Session->setFlash('The project member has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project member could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ProjectMember.' . $this->ProjectMember->primaryKey => $id));
			$this->request->data = $this->ProjectMember->find('first', $options);
		}
		$projects = $this->ProjectMember->Project->find('list');
		$employees = $this->ProjectMember->Employee->find('list');
		$this->set(compact('projects', 'employees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProjectMember->id = $id;
		if (!$this->ProjectMember->exists()) {
			throw new NotFoundException(__('Invalid project member'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectMember->delete()) {
			$this->Session->setFlash('The project member has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The project member could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
