<?php
App::uses('AppController', 'Controller');
/**
 * Roles Controller
 *
 * @property Role $Role
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RolesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	
	public $uses = array(
		'Role',
		'AclAction',
		'AclController'
	);
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('acl');
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Role->recursive = 0;
		$this->paginate = array('conditions'=>array('status'=>'active'));
		 
		if ($this->request->is('post')) {
		
			$this->paginate = array(
				'conditions'=>array(
					'OR'=>array(
						"Role.title LIKE '%".trim($this->request->data['Role']['keywords'])."%'"
					),
					'status'=>'active'
				)
			);
		}
		$this->set('roles', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id));
		$this->set('role', $this->Role->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$permissions = json_encode($data['permission']);
			$data['Role']['accesslist'] = $permissions;//json_encode($data['permission']);
			unset($data['permission']);
			$this->Role->create();
			if ($this->Role->save($data)) {
				$this->Session->setFlash('The role has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The role could not be saved. Please, try again.','default',array('class'=>'alert alert-warning'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function admin_edit($id = null) {
		if (!$this->Role->exists($id)) {
			throw new NotFoundException(__('Invalid role'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
			$data = $this->request->data;	
			$permissions = json_encode($data['permission']);
			$data['Role']['accesslist'] = $permissions;
			unset($data['permission']);

			if($id!='54b0fd85-d824-4467-8cbe-18d0cdd1d5ac'):
				$menulists=json_encode($data['menu']);	
				$data['Role']['menu_access_list'] = $menulists;//json_encode($data['permission']);
				unset($data['menu']);		
			endif;
			


			if ($this->Role->save($data)) {
				$this->Session->setFlash('The role has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The role could not be saved. Please, try again.','default',array('class'=>'alert alert-warning'));
			}
		} else {
			$options = array('conditions' => array('Role.' . $this->Role->primaryKey => $id),'recursive'=> -1);
			$data = $this->Role->find('first', $options);
			$this->set('userID', $id);

			if($id != '54b0fd85-d824-4467-8cbe-18d0cdd1d5ac'):
				$menulists=$this->AclController->find('all',
					array(
					'fields'=>array('AclController.id','AclController.title'),
					'contain' => array('AclAction.id','AclAction.title')
					)				
				);						
				$this->set('menulists', $menulists);				
			endif;

			$data['Role']['accesslist'] = json_decode($data['Role']['accesslist'],true);
			$this->request->data = $data;
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Role->id = $id;
		if (!$this->Role->exists()) {
			throw new NotFoundException(__('Invalid role'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Role->delete()) {
			$this->Session->setFlash('The role has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The role could not be deleted. Please, try again.','default',array('class'=>'alert alert-warning'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	public function admin_acl(){
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$dir = new Folder('acl');
		$files = $dir->find('.*\.js');
		$file = new File($dir->pwd() . DS . $files[0]);
		$contents = $file->read();
		$file->close();
		$aclArr=json_decode($contents,true);
		//if($this->AclController->deleteAll(array('1 = 1')));
		foreach ($aclArr as $key => $value) {
			$data['AclController']['title']   =$value['title'];
			$data['AclController']['plugins']  =json_encode($value['plugins']);
			$data['AclController']['controllers'] =json_encode($value['controllers']);
			$data['AclController']['actions']  =$value['actions'];
			$data['AclController']['admin']   =(bool)$value['admin'];
			$data['AclController']['order']   =$value['order'];
			$data['AclController']['status']  =$value['status'];
	
			//$data2=array();
			//pr($data);exit;
			$id=$this->AclController->field('id',array('title'=>$value['title']));
			if($id){
				$this->AclController->id=$id;
			}else{
				$this->AclController->create();
			}
			if($this->AclController->save($data['AclController'])){
				if(count($value['children'])>0){
					foreach ($value['children'] as $k => $val) {
						$data2['AclAction']['title']   =$val['title'];
						$data2['AclAction']['acl_controller_id']=$this->AclController->id;
						$data2['AclAction']['plugins']   =$val['plugins'];
						$data2['AclAction']['controllers']  =$val['controllers'];
						$data2['AclAction']['actions']   =$val['actions'];
						$data2['AclAction']['params']   =$val['params'];
						$data2['AclAction']['admin']   =$val['admin'];
						$data2['AclAction']['status']   =$val['status'];
	
						$idd=$this->AclAction->field('id',array('title'=>$val['title'],'acl_controller_id'=>$this->AclController->id));
						if($idd){
							$this->AclAction->id=$idd;
						}else{
							$this->AclAction->create();
						}
	
						$this->AclAction->save($data2['AclAction']);
					}
				}
				$this->Session->setFlash('Acl generated successfully.','default',array('class'=>'alert alert-success'));
			}else{
				$this->Session->setFlash('Acl could not be generated. Please, try again.','default',array('class'=>'alert alert-warning'));
			}
	
		}
	}
}
