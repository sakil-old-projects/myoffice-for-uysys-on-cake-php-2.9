<?php
App::uses('AppController', 'Controller');
/**
 * Employees Controller
 *
 * @property Employee $Employee
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class EmployeesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Employee->recursive = 0;
		$this->set('employees', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
		$this->set('employee', $this->Employee->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
			$data = $this->request->data;
			
			
			$tmp_file = $data['Employee']['profile']; 
			if(isset($data['Employee']['profile']) && $data['Employee']['profile']['error'] == 0){
				$data['Employee']['profile_img'] = $data['Employee']['profile']['name'];
				//unset($data['Employee']['profile']);
							
			}
			else 
			{
				unset($data['Employee']['profile']);
			}
			
			
			$tmp_file_cv = $data['Employee']['cv']; 
			if(isset($data['Employee']['cv']) && $data['Employee']['cv']['error'] == 0){
				$data['Employee']['cv_attachment'] = $data['Employee']['cv']['name'];
				
				$data['Employee']['cv_filename'] = pathinfo($data['Employee']['cv_attachment'], PATHINFO_FILENAME);
			
				$data['Employee']['cv_ext'] = $this->Uploader->getFileExtension($data['Employee']['cv']);
			
				
				
				
				unset($data['Employee']['cv']);
			}
			else 
			{
				unset($data['Employee']['cv']);
			}
		
			
		
			
			
			$this->Employee->create();
			if ($this->Employee->save($data)) {
								
				//file profile picture
				$id = $this->Employee->getInsertId();
				if(!empty($tmp_file) && $tmp_file['error'] == 0){
					
					
					$filename = explode('.',$data['Employee']['profile_img']);
				
					$this->Uploader->upload($tmp_file, $filename[0], $data['Employee']['profile'], 'employees_profile',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				}
				
				//file cv
				
				if(!empty($tmp_file_cv) && $tmp_file_cv['error'] == 0){
					
					$this->Uploader->upload($tmp_file_cv, $data['Employee']['cv_filename'], $data['Employee']['cv_ext'], 'employees_cv',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
		
				}
							
				
				
				
				$this->Session->setFlash('The employee has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The employee could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$designations = $this->Employee->Designation->find('list');
		$departments = $this->Employee->Department->find('list');
		$this->set(compact('designations','departments'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Employee->exists($id)) {
			throw new NotFoundException(__('Invalid employee'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
			
			$data = $this->request->data;
		
			
			$tmp_file = $data['Employee']['profile']; 
			if(isset($data['Employee']['profile']) && $data['Employee']['profile']['error'] == 0){
				$data['Employee']['profile_img'] = $data['Employee']['profile']['name'];
				//unset($data['Employee']['profile']);
				
								
			}
			else 
			{
				unset($data['Employee']['profile']);
			}
			
			
			$tmp_file_cv = $data['Employee']['cv']; 
			if(isset($data['Employee']['cv']) && $data['Employee']['cv']['error'] == 0){
				$data['Employee']['cv_attachment'] = $data['Employee']['cv']['name'];
				
				$data['Employee']['cv_filename'] = pathinfo($data['Employee']['cv_attachment'], PATHINFO_FILENAME);
			
				$data['Employee']['cv_ext'] = $this->Uploader->getFileExtension($data['Employee']['cv']);
			
				
				//unset($data['Employee']['cv']);
			}
			else 
			{
				unset($data['Employee']['cv']);
			}
		
			
			if ($this->Employee->save($data)) {
				
				//file profile picture
				$id = $this->Employee->getInsertId();
				if(!empty($tmp_file) && $tmp_file['error'] == 0){
					
					
					$filename = explode('.',$data['Employee']['profile_img']);
				
					$this->Uploader->upload($tmp_file, $filename[0], $data['Employee']['profile'], 'employees_profile',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				}
				
				//file cv
				
				if(!empty($tmp_file_cv) && $tmp_file_cv['error'] == 0){
					
					$this->Uploader->upload($tmp_file_cv, $data['Employee']['cv_filename'], $data['Employee']['cv_ext'], 'employees_cv',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
		
				}
							
				
							
				
				
				
				$this->Session->setFlash('The employee has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The employee could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Employee.' . $this->Employee->primaryKey => $id));
			$this->request->data = $this->Employee->find('first', $options);
		}
		$designations = $this->Employee->Designation->find('list');
		$departments = $this->Employee->Department->find('list');
		$this->set(compact('designations','departments'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Employee->id = $id;
		if (!$this->Employee->exists()) {
			throw new NotFoundException(__('Invalid employee'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Employee->delete()) {
			$this->Session->setFlash('The employee has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The employee could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
