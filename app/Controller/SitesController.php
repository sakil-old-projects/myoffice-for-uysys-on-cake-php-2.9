<?php

App::uses('AppController', 'Controller');


class SitesController extends AppController {

	
	
	public $components = array('RequestHandler','EmailSender','Uploader');

	public $uses = array(	
		'Content',
		'WelcomeMessage',
		 
	);

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow();
		$this->layout = 'ajax';		
	}
	
	//welcome message
	public function welcome_message(){
		$this->autoRender = false;
		$response = array();
		if($this->request->is('post')){
			$data = $this->WelcomeMessage->find('first');
			if (!empty($data)) {
				$response['status'] = 'success';
				$response['data'] = $data;
			}else{
				$response['status'] = 'success';
			}
		}else{
			$response['status'] = 'error';
		}
		
	 
	
		//process the view
		/* $this->set(
			array(
				'_serialize',
				'data' => array('returnData'=>$response),
				'_jsonp' => true
			)
		);
		$this->render('data_layout'); */
		$responseData = array(
				'_serialize',
				'output' => $response,
				'_jsonp' => true
			);
		
		echo json_encode($responseData);
	}
	//content
	public function gp_content(){
		$this->autoRender = false;
		$response = array();
		if($this->request->is('post')){
			$data = $this->Content->find('first');
			if (!empty($data)) {
				$response['status'] = 'success';
				$response['data'] = $data;
			}else{
				$response['status'] = 'error';
			}
		}else{
			$response['status'] = 'error';
		}
	
	/* 	//process the view
		$this->set(
			array(
				'_serialize',
				'data' => array('returnData'=>$response),
				'_jsonp' => true
			)
		);
		$this->render('data_layout'); */
		$responseData = array(
				'_serialize',
				'output' => $response,
				'_jsonp' => true
			);
		
		echo json_encode($responseData);
	}
	 
	public function beforeRender(){
		parent::beforeRender();
		$this->response->header('Access-Control-Allow-Origin', '*');
	}
}