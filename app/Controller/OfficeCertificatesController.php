<?php
App::uses('AppController', 'Controller');
/**
 * OfficeCertificates Controller
 *
 * @property OfficeCertificate $OfficeCertificate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class OfficeCertificatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->OfficeCertificate->recursive = 0;
		$this->set('officeCertificates', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->OfficeCertificate->exists($id)) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		$options = array('conditions' => array('OfficeCertificate.' . $this->OfficeCertificate->primaryKey => $id));
		$this->set('officeCertificate', $this->OfficeCertificate->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
			$data = $this->request->data;			
			
			$tmp_file = $data['OfficeCertificate']['file']; 
			if(isset($data['OfficeCertificate']['file']) && $data['OfficeCertificate']['file']['error'] == 0){
				
				$data['OfficeCertificate']['file_name'] = $this->request->data['OfficeCertificate']['file']['name'];
				
				$data['OfficeCertificate']['onlyFilename'] = pathinfo($data['OfficeCertificate']['file_name'], PATHINFO_FILENAME);
			
				$data['OfficeCertificate']['ext'] = $this->Uploader->getFileExtension($data['OfficeCertificate']['file']);
				
				unset($data['OfficeCertificate']['file']);
			}
			
		
			
			
			$this->OfficeCertificate->create();
			if ($this->OfficeCertificate->save($this->request->data)) {
				
				
				$id = $this->OfficeCertificate->getInsertId();
				if(!empty($tmp_file) && $tmp_file['error'] == 0){
					
					$data['OfficeCertificate']['id'] = $id;
					$data['OfficeCertificate']['file_name'] = $id.".".$data['OfficeCertificate']['file_name'];
					if($this->OfficeCertificate->save($data))
					{
						
					}
										
					$this->Uploader->upload($tmp_file, $id.".".$data['OfficeCertificate']['onlyFilename'], $data['OfficeCertificate']['ext'], 'office_certificates',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				}
							
				
				$this->Session->setFlash('The office certificate has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The office certificate could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->OfficeCertificate->exists($id)) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		if ($this->request->is(array('post', 'put'))) {
		
			$data = $this->request->data;
			$tmp_file = $data['OfficeCertificate']['file'];
			unset($data['OfficeCertificate']['file']);
			if(isset($tmp_file) && $tmp_file['error'] == 0){
				if($oldData['OfficeCertificate']['file_name']){
					$attached_file = WWW_ROOT."img".DS."site".DS."office_certificates".DS.$oldData['OfficeCertificate']['file_name'];
					if(file_exists($attached_file)){
						$this->Uploader->deleteFile($attached_file);
					}
				}
				
				
					$data['OfficeCertificate']['file_name'] = $this->request->data['OfficeCertificate']['file']['name'];
					
					$data['OfficeCertificate']['only_file_name'] = pathinfo($data['OfficeCertificate']['file_name'], PATHINFO_FILENAME);
			
					$data['OfficeCertificate']['ext'] = $this->Uploader->getFileExtension($this->request->data['OfficeCertificate']['file']);
										
					$data['OfficeCertificate']['file_name'] = $id.".".$data['OfficeCertificate']['file_name'];
							
					
					$this->Uploader->upload($tmp_file, $data['OfficeCertificate']['id'].".".$data['OfficeCertificate']['only_file_name'], $data['OfficeCertificate']['ext'], 'office_certificates',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				
				
				
				//$this->Uploader->upload($tmp_file, $id, $data['Document']['attachments'], 'documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
			}
		
			
			if ($this->OfficeCertificate->save($data)) {
				$this->Session->setFlash('The office certificate has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The office certificate could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('OfficeCertificate.' . $this->OfficeCertificate->primaryKey => $id));
			$this->request->data = $this->OfficeCertificate->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->OfficeCertificate->id = $id;
		if (!$this->OfficeCertificate->exists()) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->OfficeCertificate->delete()) {
			$this->Session->setFlash('The office certificate has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The office certificate could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
