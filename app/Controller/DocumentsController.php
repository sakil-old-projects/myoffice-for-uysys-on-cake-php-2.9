<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class DocumentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	
	public $uses = array('Document','DocumentFile');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Document->recursive = 1;
		
	if (isset($this->request->data['Document'])) {
			
			$data = $this->request->data['Document'];
			$cond = array();
			if(!empty($data['client_name'])){
				$cond['Document.client_name LIKE'] = trim($data['client_name'])."%";
			}
			
			if(!empty($data['document_name'])){
				$cond['Document.document_name LIKE'] = "%".trim($data['document_name'])."%";
			}
			
			
			if(!empty($data['document_type_id'])){
				$cond['Document.document_type_id'] = trim($data['document_type_id']);
			}
			
			if(!empty($data['date_from']) && !empty($data['date_to'])){
				$cond['Document.submission_date BETWEEN ? and ?'] = array($data['date_from'], $data['date_to']);
			}

			

			$this->paginate = array('conditions'=>$cond);	
			//$this->paginate = array('order'=>'Document.id DESC');

					
			$this->set('documents', $this->Paginator->paginate());

			
		
		}
		else
		{	

			$this->paginate = array('order'=>'Document.submission_date DESC','limit'=>100);	
		
			$this->set('documents', $this->Paginator->paginate());
			

		}


			$documentTypes = $this->Document->DocumentType->find('list');
			$this->set(compact('documentTypes'));
		
	
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$this->set('document', $this->Document->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
			$data = $this->request->data;	
			$multiple_files = $data['Document']['multiple_files'];
			unset($data['Document']['multiple_files']);
			
			
			$this->Document->create();
			if ($this->Document->save($data)) {
				//file upload
				$id = $this->Document->getInsertId();
								
				//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{
					
					foreach($multiple_files as $fileData)
					{						
						
						$data['DocumentFile']['document_id'] =  $id;
						
						$data['DocumentFile']['file'] = $fileData['name'];
					
						$data['DocumentFile']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['DocumentFile']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['DocumentFile']['file'] = $id.".".$data['DocumentFile']['file'];				
					
						$this->Uploader->upload($fileData, $id.".".$data['DocumentFile']['filename'], $data['DocumentFile']['ext'], 'documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->DocumentFile->create();
						if ($this->DocumentFile->save($data)) {
							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				
				$this->Session->setFlash('The document has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The document could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$documentTypes = $this->Document->DocumentType->find('list');
		$this->set(compact('documentTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Document->exists($id)) {
			throw new NotFoundException(__('Invalid document'));
		}
		$options = array('conditions' => array('Document.' . $this->Document->primaryKey => $id));
		$oldData = $this->Document->find('first', $options);
		
		if ($this->request->is(array('post', 'put'))) {
			
			
			$data = $this->request->data;			
						
			$multiple_files = $data['Document']['multiple_files'];
			unset($data['Document']['multiple_files']);
			
					
			if ($this->Document->save($data)) {
					
				//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{
									
					foreach($multiple_files as $fileData)
					{						
						
						$data['DocumentFile']['document_id'] =  $data['Document']['id'];
						
						$data['DocumentFile']['file'] = $fileData['name'];
					
						$data['DocumentFile']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['DocumentFile']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['DocumentFile']['file'] = $data['DocumentFile']['document_id'].".".$data['DocumentFile']['file'];				
					
						$this->Uploader->upload($fileData, $data['Document']['id'].".".$data['DocumentFile']['filename'], $data['DocumentFile']['ext'], 'documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->DocumentFile->create();
						if ($this->DocumentFile->save($data)) {
							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				$this->Session->setFlash('The document has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The document could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$this->request->data = $oldData;			
		}
		$documentTypes = $this->Document->DocumentType->find('list');
		$this->set(compact('documentTypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Document->id = $id;
		if (!$this->Document->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Document->delete()) {
			$this->Session->setFlash('The document has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The document could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	

}
