<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ServiceChargesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ServiceCharge->recursive = 0;
		
		if (isset($this->request->data['Product'])) {
			
			$data = $this->request->data['Product'];
			$cond = array();
			if(!empty($data['id'])){
				$cond['Product.id'] = $data['id'];
			}
			
			$this->paginate = array('conditions'=>$cond);	
		
		}		
		 
		$clients = $this->ServiceCharge->Client->find('list');
		$serviceCharges = $this->Paginator->paginate();		
		
		
		//debug($serviceCharges);
		$this->set(compact('clients','serviceCharges'));
		
		
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
		$this->set('product', $this->Product->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ServiceCharge->create();
			if ($this->ServiceCharge->save($this->request->data)) {
				$this->Session->setFlash('The ServiceCharge has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The ServiceCharge could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$clients = $this->ServiceCharge->Client->find('list');
		$PaySchedule = $this->ServiceCharge->PaySchedule->find('list');
		$service_statues = $this->ServiceCharge->ServiceStatus->find('list');
		$this->set(compact('clients','PaySchedule','service_statues'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ServiceCharge->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ServiceCharge->save($this->request->data)) {
				$this->Session->setFlash('The ServiceCharge has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The ServiceCharge could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ServiceCharge.' . $this->ServiceCharge->primaryKey => $id));
			$this->request->data = $this->ServiceCharge->find('first', $options);
		}
		
		$clients = $this->ServiceCharge->Client->find('list');
		$PaySchedule = $this->ServiceCharge->PaySchedule->find('list');
		$service_statues = $this->ServiceCharge->ServiceStatus->find('list');
		$this->set(compact('clients','PaySchedule','service_statues'));
		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ServiceCharge->id = $id;
		if (!$this->ServiceCharge->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ServiceCharge->delete()) {
			$this->Session->setFlash('The ServiceCharge has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The ServiceCharge could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
