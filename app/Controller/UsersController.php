<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	
	public function beforeFilter(){
		parent::beforeFilter();
		//$this->Auth->allow('logout');
		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		if ($this->request->is('post')) {
		
			$this->paginate = array(
					'conditions'=>array(
							'OR'=>array(
									"User.username LIKE '%".$this->request->data['User']['keywords']."%'" 
							)
					)
			);
		}
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$data = $this->request->data;
			$photo = $data['User']['personal_details']['photo'];
			unset($data['User']['personal_details']['photo']);
			$data['User']['personal_details'] = json_encode($data['User']['personal_details']);
			$this->User->create();
			if ($this->User->save($data)) {
				$img_id = $this->User->getInsertID();
				$this->Uploader->upload($photo, $img_id, 'png', 'avatars',$fileOrImage = null, $height = '40', $width = '', $oldfile = null);
				
				$this->Session->setFlash('The user has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The user could not be saved. Please, try again.','default',array('class'=>'alert alert-warning'));
			}
		}
		$roles = $this->User->Role->find('list',array('conditions'=>array('status'=>'active')));
		$this->set(compact('roles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$data = $this->request->data;
			
			$photo = $data['User']['personal_details']['photo'];
			
			
			unset($data['User']['personal_details']['photo']);
			$data['User']['personal_details'] = json_encode($data['User']['personal_details']);
			
			if ($this->User->save($data)) {
				
				$this->Uploader->upload($photo, $id, 'png', 'avatars',$fileOrImage = null, $height = '40', $width = '', $oldfile = null);
				
				$this->Session->setFlash('The user has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The user could not be saved. Please, try again.','default',array('class'=>'alert alert-warning'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$data = $this->User->find('first', $options);
			$data['User']['personal_details'] = json_decode($data['User']['personal_details'],true);
			$this->request->data = $data;
		}
		$roles = $this->User->Role->find('list',array('conditions'=>array('status'=>'active')));
		$this->set(compact('roles'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$img_file = WWW_ROOT."img".DS."site".DS."avatars".DS.$id.".png";
			if(file_exists($img_file)){
				$this->Uploader->deleteFile($img_file);
			}
			$this->Session->setFlash('The user has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The user could not be deleted. Please, try again.','default',array('class'=>'alert alert-warning'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
/*
 * admin_login method
 */	
	
	public function admin_login(){
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				
								
				$role_id = $this->Auth->user('role_id');
				
				if($role_id != '565d6a71-4d54-4688-bcfe-4622c0b90f99')
				{
					$this->redirect(array('controller'=>'dashboards','action'=>'dashboard','admin'=>true));
					
				}
				else 
				{
					
					$this->redirect(array('controller'=>'dashboards','action'=>'index','admin'=>true));
				}
									
							
				// return $this->redirect($this->Auth->redirectUrl());
				
			} else {
				$this->Session->setFlash(__('Username or password is incorrect'),'default',array('class'=>'alert alert-warning'));
			}
		}
		if($this->Auth->login()){
			//$this->redirect(array('controller'=>'dashboards','action'=>'index','admin'=>true));
			$this->Session->destroy();
			$this->Session->delete('User');
			//debug( $this->Auth->user('role_id'));
			//die();
			
				/*				
				$role_id = $this->Auth->user('role_id');
				
				if($role_id != '565d6a71-4d54-4688-bcfe-4622c0b90f99')
				{
					$this->redirect(array('controller'=>'dashboards','action'=>'dashboard','admin'=>true));
					
				}
				else 
				{
					
					$this->redirect(array('controller'=>'dashboards','action'=>'index','admin'=>true));
				}
				
				*/
				
			
			
			
		}
		
		// debug( $this->Auth->user('role_id'));
	}

	/*
	 * isAuthorized method
	 * 
	 */
	
	
	
	public function admin_signout() {
		
		
		$this->Session->destroy();
		return $this->redirect($this->Auth->logout());
		$this->Session->setFlash(__('Username Logout Successfully'),'default',array('class'=>'alert alert-warning'));
	}
	
	public function admin_logout() {		
		
		$this->Session->destroy();
		$this->Session->delete('User');
		return $this->redirect($this->Auth->logout());
		$this->Session->setFlash(__('Username Logout Successfully'),'default',array('class'=>'alert alert-warning'));
		
	}
	
	
	
	public function admin_change_password(){
		if($this->request->is('post')){
			$data = $this->request->data;
			$this->User->id = $this->Auth->user('id');
			$data['User']['password'] = $data['User']['new_password'];
			if($this->User->save($data)){
				$this->Session->setFlash('Password has been changed','default',array('class'=>'alert alert-success'));
			}else{
				$this->Session->setFlash('Password can not be changed','default',array('class'=>'alert alert-warning'));
			}
			
		}
	}
	
}
