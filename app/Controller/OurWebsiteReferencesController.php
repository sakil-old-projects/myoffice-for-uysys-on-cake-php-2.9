<?php
App::uses('AppController', 'Controller');
/**
 * OurWebsiteReferences Controller
 *
 * @property OurWebsiteReference $OurWebsiteReference
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class OurWebsiteReferencesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->OurWebsiteReference->recursive = 0;
		$this->set('OurWebsiteReferences', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->OurWebsiteReference->exists($id)) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		$options = array('conditions' => array('OurWebsiteReference.' . $this->OurWebsiteReference->primaryKey => $id));
		$this->set('OurWebsiteReference', $this->OurWebsiteReference->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			
			$data = $this->request->data;			
			
			$tmp_file = $data['OurWebsiteReference']['file']; 
			if(isset($data['OurWebsiteReference']['file']) && $data['OurWebsiteReference']['file']['error'] == 0){
				
				$data['OurWebsiteReference']['file_name'] = $this->request->data['OurWebsiteReference']['file']['name'];
				
				$data['OurWebsiteReference']['onlyFilename'] = pathinfo($data['OurWebsiteReference']['file_name'], PATHINFO_FILENAME);
			
				$data['OurWebsiteReference']['ext'] = $this->Uploader->getFileExtension($data['OurWebsiteReference']['file']);
				
				unset($data['OurWebsiteReference']['file']);
			}
			
		
			
			
			$this->OurWebsiteReference->create();
			if ($this->OurWebsiteReference->save($this->request->data)) {
				
				
				$id = $this->OurWebsiteReference->getInsertId();
				if(!empty($tmp_file) && $tmp_file['error'] == 0){
					
					$data['OurWebsiteReference']['id'] = $id;
					$data['OurWebsiteReference']['file_name'] = $id.".".$data['OurWebsiteReference']['file_name'];
					if($this->OurWebsiteReference->save($data))
					{
						
					}
										
					$this->Uploader->upload($tmp_file, $id.".".$data['OurWebsiteReference']['onlyFilename'], $data['OurWebsiteReference']['ext'], 'office_certificates',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				}
							
				
				$this->Session->setFlash('The office certificate has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The office certificate could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->OurWebsiteReference->exists($id)) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		if ($this->request->is(array('post', 'put'))) {
		
			$data = $this->request->data;
			$tmp_file = $data['OurWebsiteReference']['file'];
			unset($data['OurWebsiteReference']['file']);
			if(isset($tmp_file) && $tmp_file['error'] == 0){
				if($oldData['OurWebsiteReference']['file_name']){
					$attached_file = WWW_ROOT."img".DS."site".DS."office_certificates".DS.$oldData['OurWebsiteReference']['file_name'];
					if(file_exists($attached_file)){
						$this->Uploader->deleteFile($attached_file);
					}
				}
				
				
					$data['OurWebsiteReference']['file_name'] = $this->request->data['OurWebsiteReference']['file']['name'];
					
					$data['OurWebsiteReference']['only_file_name'] = pathinfo($data['OurWebsiteReference']['file_name'], PATHINFO_FILENAME);
			
					$data['OurWebsiteReference']['ext'] = $this->Uploader->getFileExtension($this->request->data['OurWebsiteReference']['file']);
										
					$data['OurWebsiteReference']['file_name'] = $id.".".$data['OurWebsiteReference']['file_name'];
							
					
					$this->Uploader->upload($tmp_file, $data['OurWebsiteReference']['id'].".".$data['OurWebsiteReference']['only_file_name'], $data['OurWebsiteReference']['ext'], 'office_certificates',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
				
				
				
				//$this->Uploader->upload($tmp_file, $id, $data['Document']['attachments'], 'documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
			}
		
			
			if ($this->OurWebsiteReference->save($data)) {
				$this->Session->setFlash('The office certificate has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The office certificate could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('OurWebsiteReference.' . $this->OurWebsiteReference->primaryKey => $id));
			$this->request->data = $this->OurWebsiteReference->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->OurWebsiteReference->id = $id;
		if (!$this->OurWebsiteReference->exists()) {
			throw new NotFoundException(__('Invalid office certificate'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->OurWebsiteReference->delete()) {
			$this->Session->setFlash('The office certificate has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The office certificate could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
