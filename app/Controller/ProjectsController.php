<?php
App::uses('AppController', 'Controller');
/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	
	public $uses = array('Project','ProjectDocument');
	
	public $projectStatus = array('1'=>'In Progress','2'=>'Complete','3'=>'Pending');
	
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->set('projectStatus',$this->projectStatus);
	}
	
	
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Project->recursive = 0;
		
		if ($this->request->is('post')) {
			$data = $this->request->data['Project'];
			$cond = array();
			if(!empty($data['client_id'])){
				$cond['client_id'] = $data['client_id'];
			}
			if(!empty($data['project_category_id'])){
				$cond['project_category_id'] = $data['project_category_id'];
			}
			if(!empty($data['id'])){
				$cond['Project.id'] = $data['id'];
			}
			if(!empty($data['project_status'])){
				$cond['project_status'] = $data['project_status'];
			}
			$this->paginate = array('conditions'=>$cond,'order'=>array('Project.agreement_date DESC'));
		} 
		$this->paginate = array(
			     
			     'limit' => 100,
				 'order' => array(
					'Project.id' => 'desc'
					)
		     );		

		$projects = $this->paginate('Project');
				
		$projectLists = $this->Project->find('list');
		$clients = $this->Project->Client->find('list');
		$projectCategories = $this->Project->ProjectCategory->find('list');
		$this->set(compact('projects','projectLists','clients','projectCategories'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
		$this->set('project', $this->Project->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Project->create();
			
			$data = $this->request->data;		
			$multiple_files = $data['Project']['multiple_files'];
			unset($data['Project']['multiple_files']);
			
			
			if ($this->Project->save($this->request->data)) {
				
				
				$id = $this->Project->getInsertId();
				
				
						//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{									
					foreach($multiple_files as $fileData)
					{					
						
						$data['ProjectDocument']['project_id'] =  $id;
						
						$data['ProjectDocument']['file'] = $fileData['name'];
					
						$data['ProjectDocument']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['ProjectDocument']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['ProjectDocument']['file'] = $data['ProjectDocument']['project_id'].".".$data['ProjectDocument']['file'];				
					
						$this->Uploader->upload($fileData, $id.".".$data['ProjectDocument']['filename'], $data['ProjectDocument']['ext'], 'projects_documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->ProjectDocument->create();
						if ($this->ProjectDocument->save($data)) {							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				
				$this->Session->setFlash('The project has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$clients = $this->Project->Client->find('list');
		$projectCategories = $this->Project->ProjectCategory->find('list');
		$this->set(compact('clients','projectCategories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is(array('post', 'put'))) {
			
			
		    $data = $this->request->data;
		
			$multiple_files = $data['Project']['multiple_files'];
			unset($data['Project']['multiple_files']);
			
						
			if ($this->Project->save($this->request->data)) {			
				
				
				//Upload Multiple FIle
				if(sizeof($multiple_files)>0 && !empty($multiple_files[0]['name']))
				{									
					foreach($multiple_files as $fileData)
					{					
						
						$data['ProjectDocument']['project_id'] =  $data['Project']['id'];
						
						$data['ProjectDocument']['file'] = $fileData['name'];
					
						$data['ProjectDocument']['filename'] = pathinfo( $fileData['name'], PATHINFO_FILENAME);
			
						$data['ProjectDocument']['ext'] = $this->Uploader->getFileExtension($fileData);
										
						$data['ProjectDocument']['file'] = $data['ProjectDocument']['project_id'].".".$data['ProjectDocument']['file'];				
					
						$this->Uploader->upload($fileData, $data['Project']['id'].".".$data['ProjectDocument']['filename'], $data['ProjectDocument']['ext'], 'projects_documents',$fileOrImage = 'file', $height = '', $width = '', $oldfile = null );
														
						$this->ProjectDocument->create();
						if ($this->ProjectDocument->save($data)) {							
							
						}	
						
					}
					
					
				}
				//End Upload Multiple File
				
				
				
				$this->Session->setFlash('The project has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
		$clients = $this->Project->Client->find('list');
		$projectCategories = $this->Project->ProjectCategory->find('list');
		$this->set(compact('clients','projectCategories'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Project->delete()) {
			$this->Session->setFlash('The project has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The project could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
