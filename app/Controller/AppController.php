<?php
App::uses('Controller', 'Controller');

class AppController extends Controller
{
    //all components
    public $components = array(
        'Uploader',
        'Session',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'users',
                'action' => 'login',
                'admin' => true,
                'plugin' => false
            ),
            'loginRedirect' => array(
                'controller' => 'dashboards',
                'action' => 'index',
                'admin' => true,
                'plugin' => false
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login',
                'admin' => true,
                'plugin' => false
            ),
            'authenticate' => array(
                'Form' => array(
                    'passwordHasher' => 'Blowfish'
                )
            ),
            'authorize' => array('Controller'),
            //'authError' => 'You don\'t have permission to access those area.',
        ),
        'RequestHandler'
    );


    

    public $acl_array = [
        //cms
        'cms' => [
             
            [
                'controller' => 'users',
                'actions' => ['admin_index', 'admin_add', 'admin_edit', 'admin_delete','admin_signout']
            ],
            [
                'controller' => 'roles',
                'actions' => ['admin_index', 'admin_add', 'admin_edit', 'admin_delete','admin_acl']
            ],
            [
                'controller' => 'clients',
                'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'designations',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'employees',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'domains',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'hostings',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'inventories',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'project_categories',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'projects',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'project_transactions',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'project_documents',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'project_members',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'office_certificates',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'meetings',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'task_managements',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'team_types',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'team_managements',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'document_types',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'documents',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],            
            [
	            'controller' => 'document_files',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'countries',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'platforms',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'products',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'service_charges',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ]
            ,
            [
	            'controller' => 'departments',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ]
            ,
            [
	            'controller' => 'our_website_references',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
             [
	            'controller' => 'client_visits',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],            
             [
	            'controller' => 'meeting_feedback_files',
	            'actions' => ['admin_index','admin_add','admin_edit','admin_delete']
            ],
            [
	            'controller' => 'dashboards',
	            'actions' => ['admin_index','admin_dashboard']
            ]
        ] 
    ];
 

    public $status = [
        'active' => 'Active',
        'inactive' => 'Inactive',
        'draft' => 'Draft'
    ];
    
    

   

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->set('acl_array', $this->acl_array);
        $this->set('status', $this->status);
        $this->set('site_status', $this->site_status);

        $this->set('auth_status', $this->Auth->loggedIn());
        $this->set('auth_user', $this->Auth->user());
        //$this->Auth->alow(array('getDomainCredentials'));


    }

    public function isAuthorized($user = null)
    {

        $permission_array = json_decode($user['Role']['accesslist'], true);

        $permission_array['dashboards']['admin_index'] = 'admin_index';
        //users permission
        $permission_array['users']['admin_change_password'] = 'admin_change_password';
        $permission_array['users']['admin_login'] = 'admin_login';
        $permission_array['users']['admin_signout'] = 'admin_signout';

        // for merchant
        //$permission_array['merchants']['admin_login'] = 'admin_login';

        $permission_array['media']['admin_ajax_image_manager'] = 'admin_ajax_image_manager';
        $permission_array['media']['admin_ajax_image_delete'] = 'admin_ajax_image_delete';
        $permission_array['media']['admin_ajax_uploader'] = 'admin_ajax_uploader';


        if (isset($permission_array[$this->params['controller']][$this->params['action']])) {
            if ($permission_array[$this->params['controller']][$this->params['action']] == $this->params['action']) {
                return true;
            }
        }
        //$this->Session->setFlash('default','auth', array('class'=>'alert alert-warning'));
        $this->Auth->flash['params']['class'] = 'alert alert-danger';
        return false;
    }



}