<?php
App::uses('AppController', 'Controller');
/**
 * ProjectTransactions Controller
 *
 * @property ProjectTransaction $ProjectTransaction
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjectTransactionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	
	
	
	public $components = array('Paginator', 'Session', 'Flash');
	
	public $ProjectTransactionStatus = array('1'=>'In Progress','2'=>'Complete','3'=>'Pending');
	
	
	public function beforeFilter(){
		parent::beforeFilter();
		$this->set('ProjectTransactionStatus',$this->ProjectTransactionStatus);
	}
	
	
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		
		$this->loadModel('Project');
		
		$this->Project->recursive = 2;
		
		$project_id = $this->params['pass'][0];

		$conditions = array('Project.id'=>$project_id);
		
		$this->paginate = array('conditions'=>$conditions);
		
		$projectData = $this->Paginator->paginate('Project');
		$proData = $projectData[0];
	
		$this->set(compact('proData'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProjectTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid ProjectTransaction'));
		}
		$options = array('conditions' => array('ProjectTransaction.' . $this->ProjectTransaction->primaryKey => $id));
		$this->set('ProjectTransaction', $this->ProjectTransaction->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		
		
		$project_id = $this->params['pass'][0];
		
		if ($this->request->is('post')) {
			$this->ProjectTransaction->create();
			if ($this->ProjectTransaction->save($this->request->data)) {
				$this->Session->setFlash('The ProjectTransaction has been saved.','default',array('class'=>'alert alert-success'));
				
				return $this->redirect(array('action' => 'index',$this->request->data['ProjectTransaction']['project_id']));
			} else {
				$this->Session->setFlash('The ProjectTransaction could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		
		
		$this->set('project_id',$project_id);
	
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		
		
		if (!$this->ProjectTransaction->exists($id)) {
			throw new NotFoundException(__('Invalid ProjectTransaction'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectTransaction->save($this->request->data)) {
				$this->Session->setFlash('The ProjectTransaction has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index',$this->request->data['ProjectTransaction']['project_id']));
			} else {
				$this->Session->setFlash('The ProjectTransaction could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ProjectTransaction.' . $this->ProjectTransaction->primaryKey => $id));
			$this->request->data = $this->ProjectTransaction->find('first', $options);
		}
		
		

	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProjectTransaction->id = $id;
		if (!$this->ProjectTransaction->exists()) {
			throw new NotFoundException(__('Invalid ProjectTransaction'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectTransaction->delete()) {
			$this->Session->setFlash('The ProjectTransaction has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The ProjectTransaction could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
