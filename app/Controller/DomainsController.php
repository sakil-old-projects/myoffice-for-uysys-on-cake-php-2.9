<?php
App::uses('AppController', 'Controller');
/**
 * Domains Controller
 *
 * @property Domain $Domain
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class DomainsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Domain->recursive = 0;
		$this->set('domains', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Domain->exists($id)) {
			throw new NotFoundException(__('Invalid domain'));
		}
		$options = array('conditions' => array('Domain.' . $this->Domain->primaryKey => $id));
		$this->set('domain', $this->Domain->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Domain->create();
			if ($this->Domain->save($this->request->data)) {
				$this->Session->setFlash('The domain has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The domain could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$hostings = $this->Domain->Hosting->find('list');
		$this->set(compact('hostings'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Domain->exists($id)) {
			throw new NotFoundException(__('Invalid domain'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Domain->save($this->request->data)) {
				$this->Session->setFlash('The domain has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The domain could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Domain.' . $this->Domain->primaryKey => $id));
			$this->request->data = $this->Domain->find('first', $options);
		}
		$hostings = $this->Domain->Hosting->find('list');
		$this->set(compact('hostings'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Domain->id = $id;
		if (!$this->Domain->exists()) {
			throw new NotFoundException(__('Invalid domain'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Domain->delete()) {
			$this->Session->setFlash('The domain has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The domain could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
