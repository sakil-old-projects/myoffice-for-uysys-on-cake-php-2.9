<?php
App::uses('AppController', 'Controller');
/**
 * ProjectDocuments Controller
 *
 * @property ProjectDocument $ProjectDocument
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class MeetingFeedbackFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null,$meeting_id=null) {
		$this->MeetingFeedbackFile->id = $id;
		if (!$this->MeetingFeedbackFile->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		//$this->request->allowMethod('post', 'delete');
		
		if ($this->MeetingFeedbackFile->delete()) {
			$this->Session->setFlash('The MeetingFeedbackFile has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The MeetingFeedbackFile could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('controller'=>'meetings','action' => 'edit',$meeting_id));
	}
	
}
