<?php
App::uses('AppController', 'Controller');
/**
 * Clients Controller
 *
 * @property Client $Client
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ClientsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Client->recursive = 0;
		
		
		if (isset($this->request->data['Client'])) {
			
			$data = $this->request->data['Client'];
			$cond = array();
			if(!empty($data['product_id'])){
				$cond['Client.product_id'] = $data['product_id'];
			}
			
			$this->paginate = array('conditions'=>$cond,'order'=>'Client.company_name ASC');	
		
		}
		
	
		$this->paginate = array('order'=>'Client.company_name ASC');	
				
		$this->set('clients', $this->Paginator->paginate());
		
		$products = $this->Client->Product->find('list');
		$this->set('productLists', $products);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Client->exists($id)) {
			throw new NotFoundException(__('Invalid client'));
		}
		$options = array('conditions' => array('Client.' . $this->Client->primaryKey => $id));
		$this->set('client', $this->Client->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Client->create();
			if ($this->Client->save($this->request->data)) {
				$this->Session->setFlash('The client has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The client could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$projectCategory = $this->Client->ProjectCategory->find('list');
		$countries = $this->Client->Country->find('list');
		$products = $this->Client->Product->find('list');
		$client_types = $this->Client->ClientType->find('list');
		$this->set(compact('projectCategory','countries',"products",'client_types'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Client->exists($id)) {
			throw new NotFoundException(__('Invalid client'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Client->save($this->request->data)) {
				$this->Session->setFlash('The client has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The client could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Client.' . $this->Client->primaryKey => $id));
			$this->request->data = $this->Client->find('first', $options);
		}
		$projectCategory = $this->Client->ProjectCategory->find('list');
		$countries = $this->Client->Country->find('list');
		$products = $this->Client->Product->find('list');
		$client_types = $this->Client->ClientType->find('list');
		$this->set(compact('projectCategory','countries',"products",'client_types'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Client->id = $id;
		if (!$this->Client->exists()) {
			throw new NotFoundException(__('Invalid client'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Client->delete()) {
			$this->Session->setFlash('The client has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The client could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
