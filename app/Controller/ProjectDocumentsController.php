<?php
App::uses('AppController', 'Controller');
/**
 * ProjectDocuments Controller
 *
 * @property ProjectDocument $ProjectDocument
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjectDocumentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProjectDocument->recursive = 0;
		$this->set('projectDocuments', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProjectDocument->exists($id)) {
			throw new NotFoundException(__('Invalid project document'));
		}
		$options = array('conditions' => array('ProjectDocument.' . $this->ProjectDocument->primaryKey => $id));
		$this->set('projectDocument', $this->ProjectDocument->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProjectDocument->create();
			if ($this->ProjectDocument->save($this->request->data)) {
				$this->Session->setFlash('The project document has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project document could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$projects = $this->ProjectDocument->Project->find('list');
		$this->set(compact('projects'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProjectDocument->exists($id)) {
			throw new NotFoundException(__('Invalid project document'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectDocument->save($this->request->data)) {
				$this->Session->setFlash('The project document has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project document could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ProjectDocument.' . $this->ProjectDocument->primaryKey => $id));
			$this->request->data = $this->ProjectDocument->find('first', $options);
		}
		$projects = $this->ProjectDocument->Project->find('list');
		$this->set(compact('projects'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null,$project_id=null) {
		$this->ProjectDocument->id = $id;
		if (!$this->ProjectDocument->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->ProjectDocument->delete()) {
			$this->Session->setFlash('The ProjectDocument has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The ProjectDocument could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('controller'=>'projects','action' => 'edit',$project_id));
	}
	
}
