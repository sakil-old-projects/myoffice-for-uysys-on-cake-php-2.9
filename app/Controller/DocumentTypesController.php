<?php
App::uses('AppController', 'Controller');
/**
 * DocumentTypes Controller
 *
 * @property DocumentType $DocumentType
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class DocumentTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->DocumentType->recursive = 0;
		$this->set('documentTypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->DocumentType->exists($id)) {
			throw new NotFoundException(__('Invalid document type'));
		}
		$options = array('conditions' => array('DocumentType.' . $this->DocumentType->primaryKey => $id));
		$this->set('documentType', $this->DocumentType->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->DocumentType->create();
			if ($this->DocumentType->save($this->request->data)) {
				$this->Session->setFlash('The document type has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The document type could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->DocumentType->exists($id)) {
			throw new NotFoundException(__('Invalid document type'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->DocumentType->save($this->request->data)) {
				$this->Session->setFlash('The document type has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The document type could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('DocumentType.' . $this->DocumentType->primaryKey => $id));
			$this->request->data = $this->DocumentType->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->DocumentType->id = $id;
		if (!$this->DocumentType->exists()) {
			throw new NotFoundException(__('Invalid document type'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->DocumentType->delete()) {
			$this->Session->setFlash('The document type has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The document type could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
