<?php
App::uses('AppController', 'Controller');
/**
 * ProjectCategories Controller
 *
 * @property ProjectCategory $ProjectCategory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class ProjectCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->ProjectCategory->recursive = 0;
		$this->set('projectCategories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->ProjectCategory->exists($id)) {
			throw new NotFoundException(__('Invalid project category'));
		}
		$options = array('conditions' => array('ProjectCategory.' . $this->ProjectCategory->primaryKey => $id));
		$this->set('projectCategory', $this->ProjectCategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->ProjectCategory->create();
			if ($this->ProjectCategory->save($this->request->data)) {
				$this->Session->setFlash('The project category has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project category could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->ProjectCategory->exists($id)) {
			throw new NotFoundException(__('Invalid project category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectCategory->save($this->request->data)) {
				$this->Session->setFlash('The project category has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The project category could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('ProjectCategory.' . $this->ProjectCategory->primaryKey => $id));
			$this->request->data = $this->ProjectCategory->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProjectCategory->id = $id;
		if (!$this->ProjectCategory->exists()) {
			throw new NotFoundException(__('Invalid project category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ProjectCategory->delete()) {
			$this->Session->setFlash('The project category has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The project category could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
