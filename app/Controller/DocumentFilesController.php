<?php
App::uses('AppController', 'Controller');
/**
 * Documents Controller
 *
 * @property Document $Document
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class DocumentFilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');
	
	public $uses = array('Document','DocumentFile');

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null,$document_id=null) {
		$this->DocumentFile->id = $id;
		if (!$this->DocumentFile->exists()) {
			throw new NotFoundException(__('Invalid document'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->DocumentFile->delete()) {
			$this->Session->setFlash('The document has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The document could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('controller'=>'documents','action' => 'edit',$document_id));
	}
	

}
