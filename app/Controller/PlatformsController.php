<?php
App::uses('AppController', 'Controller');
/**
 * Platforms Controller
 *
 * @property Platform $Platform
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class PlatformsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Platform->recursive = 0;
		$this->set('platforms', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Platform->exists($id)) {
			throw new NotFoundException(__('Invalid platform'));
		}
		$options = array('conditions' => array('Platform.' . $this->Platform->primaryKey => $id));
		$this->set('platform', $this->Platform->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Platform->create();
			if ($this->Platform->save($this->request->data)) {
				$this->Session->setFlash('The platform has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The platform could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Platform->exists($id)) {
			throw new NotFoundException(__('Invalid platform'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Platform->save($this->request->data)) {
				$this->Session->setFlash('The platform has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The platform could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Platform.' . $this->Platform->primaryKey => $id));
			$this->request->data = $this->Platform->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Platform->id = $id;
		if (!$this->Platform->exists()) {
			throw new NotFoundException(__('Invalid platform'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Platform->delete()) {
			$this->Session->setFlash('The platform has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The platform could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
