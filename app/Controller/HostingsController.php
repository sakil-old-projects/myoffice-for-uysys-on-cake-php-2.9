<?php
App::uses('AppController', 'Controller');
/**
 * Hostings Controller
 *
 * @property Hosting $Hosting
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class HostingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Hosting->recursive = 0;
		$this->set('hostings', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Hosting->exists($id)) {
			throw new NotFoundException(__('Invalid hosting'));
		}
		$options = array('conditions' => array('Hosting.' . $this->Hosting->primaryKey => $id));
		$this->set('hosting', $this->Hosting->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Hosting->create();
			if ($this->Hosting->save($this->request->data)) {
				$this->Session->setFlash('The hosting has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The hosting could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Hosting->exists($id)) {
			throw new NotFoundException(__('Invalid hosting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Hosting->save($this->request->data)) {
				$this->Session->setFlash('The hosting has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The hosting could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('Hosting.' . $this->Hosting->primaryKey => $id));
			$this->request->data = $this->Hosting->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Hosting->id = $id;
		if (!$this->Hosting->exists()) {
			throw new NotFoundException(__('Invalid hosting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Hosting->delete()) {
			$this->Session->setFlash('The hosting has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The hosting could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
