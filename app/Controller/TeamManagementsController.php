<?php
App::uses('AppController', 'Controller');
/**
 * TeamManagements Controller
 *
 * @property TeamManagement $TeamManagement
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class TeamManagementsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamManagement->recursive = 0;
		$this->set('teamManagements', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TeamManagement->exists($id)) {
			throw new NotFoundException(__('Invalid team management'));
		}
		$options = array('conditions' => array('TeamManagement.' . $this->TeamManagement->primaryKey => $id));
		$this->set('teamManagement', $this->TeamManagement->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamManagement->create();
			if ($this->TeamManagement->save($this->request->data)) {
				$this->Session->setFlash('The team management has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The team management could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		}
		$employees = $this->TeamManagement->Employee->find('list');
		$teamTypes = $this->TeamManagement->TeamType->find('list');
		$this->set(compact('employees', 'teamTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TeamManagement->exists($id)) {
			throw new NotFoundException(__('Invalid team management'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TeamManagement->save($this->request->data)) {
				$this->Session->setFlash('The team management has been saved.','default',array('class'=>'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The team management could not be saved. Please, try again.','default',array('class'=>'alert alert-warnging'));
			}
		} else {
			$options = array('conditions' => array('TeamManagement.' . $this->TeamManagement->primaryKey => $id));
			$this->request->data = $this->TeamManagement->find('first', $options);
		}
		$employees = $this->TeamManagement->Employee->find('list');
		$teamTypes = $this->TeamManagement->TeamType->find('list');
		$this->set(compact('employees', 'teamTypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TeamManagement->id = $id;
		if (!$this->TeamManagement->exists()) {
			throw new NotFoundException(__('Invalid team management'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TeamManagement->delete()) {
			$this->Session->setFlash('The team management has been deleted.','default',array('class'=>'alert alert-success'));
		} else {
			$this->Session->setFlash('The team management could not be deleted. Please, try again.','default',array('class'=>'alert alert-warnging'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
